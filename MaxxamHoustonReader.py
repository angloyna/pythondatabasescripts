from openpyxl import Workbook
from openpyxl import load_workbook
from maxxhoustonhelperfunctions import *
import MySQLdb

def maxxamReader(excelfilepath, analysistype, phase, containertype, analysislab):
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()
    
    wb = load_workbook(filename = excelfilepath)
    sheet_ranges = wb.worksheets[0]
    resultsStartCol = 29
    curcol = resultsStartCol
    
    conn.execute('SELECT id from sampledb.analysis_types where name = %s', analysistype)
    analysistypeid = conn.fetchall()[0][0]
  
    curcell = sheet_ranges.cell(row = 1, column = curcol).value
    
    repSpecMap = {}
    nameToresultID = {}
    dbnames = {}
    dbUnitMap = {}
    
    lab = analysislab
    method = 'Unknown' 
    process_status = "Processed"
    val_bool = "Y"
    containername = containertype
        
    while curcell != None:
        resultunitpair = curcell.split('[')
        if curcell.find("[") >= 0:
            spec = resultunitpair[0].strip()
            units = resultunitpair[1].split(']')[0].strip().lower()
            if units == 'btu/cf':
                units = 'btu/scf'
            if units == 'wt %':
                units = 'wt. %'
            if units == 'lv %':
                units = 'l.v. %'
        else:
            spec = resultunitpair[0].strip()
            units = 'None'
        repSpecMap[curcol] = (spec, units)
        curcol += 1
        curcell = sheet_ranges.cell(row = 1, column = curcol).value
        
    if phase == 'Gas' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c30plus from result_names;")
    if phase == 'Gas' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c30plus from result_names;")       
        
        
        
    rows = conn.fetchall()
    
    for row in rows:
        dbnames[row[1]] = row[0]
         
    
    for item in repSpecMap:
        curspecies = repSpecMap[item][0]
        if dbnames.has_key(curspecies):
            nameToresultID[curspecies] = dbnames[curspecies]
        else:
            print "can't find", curspecies
     
    conn.execute("SELECT id, spl_name FROM units")
    
    units = conn.fetchall()
    
    for unit in units:
        dbUnitMap[unit[1].lower()] = unit[0]
    
    samplerow = 2
    cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
    
    while cursamplecell != None:
        samplerowstr = str(samplerow)
        sitename = sheet_ranges['B' + samplerowstr].value
        starttime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['P' + samplerowstr].value,'%H:%M:%S').time())
        
        if sheet_ranges['R' + samplerowstr].value == None:
            endtime = starttime + datetime.timedelta(minutes=1)
        else:
            endtime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['R' + samplerowstr].value,'%H:%M:%S').time())
            
        print starttime, endtime
        
        analysisnumber = sheet_ranges['T' + samplerowstr].value[1:]
        pressure = sheet_ranges['U' + samplerowstr].value
        temp = sheet_ranges['X' + samplerowstr].value
        barcode = sheet_ranges['G' + samplerowstr].value
        
        print analysisnumber, pressure, temp, barcode
        
        #conn.execute("SELECT id FROM sites WHERE name = %s", (sitename,))
        #siteid = conn.fetchall()[0][0]
        
        #conn.execute("SELECT id FROM analysis_points WHERE analysis_number = %s AND site_id = %s",(analysisnumber, siteid))
        #analysisid = conn.fetchall()[0][0]
        
        #conn.execute("SELECT id FROM labs WHERE name = %s", (lab,))
        #labid = conn.fetchall()[0][0]
        
        #conn.execute("SELECT id FROM container_type where name = %s", (containername))
        #containerid = conn.fetchall()[0][0]
        
        #conn.execute("SELECT id from companies where name = 'JP3 Measurement'")
        #samplerid = conn.fetchall()[0][0]
        
        #conn.execute("INSERT INTO samples(barcode, analysis_id, starttime, endtime, process_status, phase, temp, pres, sampler_id, lab_id, method, valid_bool, analysis_type_id, container_type_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s)", (barcode, analysisid, starttime, endtime, process_status, phase, temp, pressure, samplerid, labid, method, val_bool, analysistypeid, containerid))
        
        #conn.execute("SELECT sample_id FROM samples WHERE starttime = %s AND analysis_id = %s AND lab_id = %s", (starttime, analysisid, labid))
        #sampleid = conn.fetchall()[0][0]
        
        curcol = resultsStartCol
        curcell = sheet_ranges.cell(row = samplerow, column = curcol).value
        while sheet_ranges.cell(row = 1, column = curcol).value != None:
                resultname = repSpecMap[curcol][0]
                unitname = repSpecMap[curcol][1].lower()
                value = curcell
                if unitname == 'volume fraction':
                    value = float(value) * 100
                    unitname = 'l.v. %'
                if unitname == 'mass fraction':
                    value = float(value) * 100
                    unitname = 'wt. %'
                if unitname == 'mol fraction':
                    value = float(value) * 100
                    unitname = 'mol %'
                print value
                if nameToresultID.has_key(resultname):
                    resultid = nameToresultID[resultname]
                    unitid = dbUnitMap[unitname]
                    #if value != None and value != 0:
                        #conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) VALUES (%s, %s, %s, %s)", (sampleid, resultid, unitid, value))
                        #print resultname, unitname, value
                else:
                    "Can't find: ", resultname, unitname
                curcol += 1
                curcell = sheet_ranges.cell(row = samplerow, column = curcol).value
                
        samplerow += 1
        cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
        
    db.commit()
    db.close()
                