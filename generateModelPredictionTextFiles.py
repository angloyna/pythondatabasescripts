import MySQLdb


def writeSpectralInformationToFile(timeFile, spectraFile, samples):

    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    for sample in samples:
        conn.execute("SELECT time, temp, pres, spectrum from sampledb.spectra where sample_id = %s", sample)
        results = conn.fetchall()
        for result in results:
            timeFile.write(str(result[0]) + "\t" + str(result[1]) + "\t" + str(result[2]) + "\n")
            spectrum = result[3].split(",")
            for i in range (0,len(spectrum)):
                if i < (len(spectrum)-1):
                    spectraFile.write(str(spectrum[i]) + "\t")
                else:
                    spectraFile.write(str(spectrum[i]) + "\n")    

def generateSampleIDsqlString(samples):
    
    sampleIds = ''
    for k in range(0, len(samples)):
        if k < (len(samples)-1):
            sampleIds += str(samples[k]) + ","
        else:
            sampleIds += str(samples[k])    
    
    return sampleIds

def main():

    directoryPath = r"C:\Users\agloyna\Desktop\validationReport"
    samples = [7608,7621,7622,7623,7624]

    
    tempAndPresFile = open(directoryPath + '\T&P.txt', 'w')
    SpectraFile = open(directoryPath + '\spectra.txt', 'w')
       
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    
    conn = db.cursor()
    
    sampleIds = generateSampleIDsqlString(samples)

    conn.execute("SELECT count(id) from sampledb.spectra where sample_id in (" + sampleIds + ")")
    numberSpectra = conn.fetchone()[0]
    
    db.close()
    
    tempAndPresFile.write(str(numberSpectra) + "\n")
    tempAndPresFile.write("Datestamp\tTemperature\tPressure\n")

    writeSpectralInformationToFile(tempAndPresFile, SpectraFile, samples)
    
    tempAndPresFile.close()
    SpectraFile.close()
    
main()