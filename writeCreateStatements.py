import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()



tablenames = ["analysis_points", "analysis_types", "channel_description_log", "channel_phase_log", "companies", "container_type", "edd_files", "header_names", "labs", "methods", "pdf_files", "regions", "result_names", "results", "results_modeled", "sample_tweets", "samples", "site_unit_log", "sites", "speciation_types", "spectra", "units", "users"]


f = open('createStatements.txt', 'w')

for i in range(len(tablenames)):
    print tablenames[i]
    conn.execute("SHOW CREATE TABLE " + tablenames[i])

    rows = conn.fetchall()

    for row in rows:
        f.write(row[0] + '\n')
        f.write(row[1])
        f.write('\n\n\n')
    
    
db.close()
f.close()