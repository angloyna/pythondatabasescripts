import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
conn = db.cursor()


conn.execute("SELECT id, name, vp_psia_gpa2177, molar_mass, reldens_gpa2145, grossheating_gpa2145 from sampledb.result_names where (id in (select distinct id from sampledb.result_names where vp_psia_gpa2177 is not null)) or (id in (select distinct id from sampledb.result_names where molar_mass is not null)) or (id in (select distinct id from sampledb.result_names where reldens_gpa2145 is not null)) or (id in (select distinct id from sampledb.result_names where grossheating_gpa2145))")

results = conn.fetchall()

for result in results:
    conn.execute("INSERT INTO sampledb.gpa_2145_coefficients(result_id, vapor_pressure_psia, molar_mass_gmol, relative_density, gross_heating_value) VALUES (%s, %s, %s, %s, %s)", (result[0], result[2], result[3], result[4], result[5]))
    
db.commit()    
db.close()