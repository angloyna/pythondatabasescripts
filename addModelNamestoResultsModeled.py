import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor()  


models = ['Dry BTU', 'c1', 'c2','c3','ic4','nc4','ic5','nc5','c6', 'c7','c8','c9','c10','co2', 'n2', 'Relative Density']

sitename = 'Martindale Separators'
analysisnumber = 8

conn.execute('select id from sampledb.sites where name = %s', (sitename))

siteid = conn.fetchone()[0]

conn.execute('select id from sampledb.analysis_points where site_id = %s and analysis_number = %s', (siteid, analysisnumber))

analysisid = conn.fetchone()[0]

for model in models:
    conn.execute('select id from sampledb.result_names where name = %s', (model))
    
    resultid = conn.fetchone()[0]
    if model == 'VPCR4':
        unitname = 'psi'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
    if model == 'Dry BTU':
        unitname = 'BTU/cf'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
    if model == 'Relative Density':
        unitname = 'None'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
    if model not in ('API Gravity', 'VPCR4', 'Dry BTU', 'Relative Density'):
        unitname = 'mol %'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
    if model == 'API Gravity':
        unitname = 'None'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
        
    conn.execute('insert into sampledb.results_modeled(analysis_id, result_id, unit_id) values (%s, %s, %s)', (analysisid, resultid, unitid))
    
db.commit()
db.close()