import MySQLdb
import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter
import numpy
import datetime
import os
import sys


spectraPerSample = {}
resultNameMap = {}

db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()

unit_name = 'mol %'
excelfilename = 'C:/Users/agloyna/Desktop/2015_10_21_LyssyDuplication_smallersampleidValidation.xlsx'
dateStampCol = 1
tempCol = 2
presCol = 3
resultCol = 4

yellowFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')

wb = load_workbook(filename = excelfilename)
sh = wb.worksheets[0]

summarySheet = wb.create_sheet()

curcol = dateStampCol
curRow = 2

curcell = sh.cell(row = curRow, column = curcol).value
#run through each row in Model Predictor Output and do a count of spectra per sample

while curcell != None:
    #find the sample_id associated
    conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s order by sample_id", (curcell, sh.cell(row = curRow, column = curcol + 1).value, sh.cell(row = curRow, column = curcol + 2).value))
    sample_id = conn.fetchall()
    if len(sample_id) > 1:
        print "This sample is probably a duplicate........", curcell, sh.cell(row = curRow, column = curcol + 1).value, sh.cell(row = curRow, column = curcol + 2).value
        sample_id = sample_id[0][0]
    else:
        sample_id = sample_id[0][0]
        
    if not spectraPerSample.has_key(sample_id):
        spectraPerSample[sample_id] = 1
    if spectraPerSample.has_key(sample_id):
        numSamples = spectraPerSample[sample_id]
        spectraPerSample[sample_id] = numSamples + 1
        
    curRow += 1
    curcell = sh.cell(row = curRow, column = curcol).value
      
#just picking a random sample_id (the last one)
validationReportsampleID = sample_id
    
#run through each column title and map the title to the column number
curcol = resultCol
curRow = 1

curcell = sh.cell(row = curRow, column = curcol).value

while curcell != None:
    if curcell.find('T2_') == -1 and curcell.find('Q_') == -1:
        resultNameMap[curcell.lower()] = curcol
    curcol += 1
    curcell = sh.cell(row = curRow, column = curcol).value

samplerow = 2
curcell = sh.cell(row = samplerow, column = 1).value
curcolumnForSampleDate = 4
curcolumnForAnalysisPoint = 1
curcolumnForVeraxValue = 2
curcolumnForVeraxStdev = 3
curcolumnForRMSECV = 4
curcolumnFor2xRMSECV = 5
curcolumnForMaxxamValue = 6
curcolumnFortempAndPres = 2
curcolumnForresultNames = 1
curcolumnForHeaderStart = 2
curcolumnForDelta = 7
curcolumnForTs = 8
curcolumnForQ = 9
curRowForResults = 5

orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'c12+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor', 'GPM Total C2+', 'VPCR4', 'TVP_N2CO2_JP3', 'Dry Vapor Pressure (D5191)']

font = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')


#This is where I'm going to generate the validation_reports and validation_files records

validation_numsamples = len(spectraPerSample)

conn.execute('SELECT site, analysis_number, phase from sampledb.sample_info where sample_id = %s', (validationReportsampleID))
validationheaderinfo = conn.fetchone()
validation_sitename = validationheaderinfo[0]
validation_analysis_number = validationheaderinfo[1]
validation_phase = validationheaderinfo[2]

conn.execute('SELECT analysis_points.id from sampledb.analysis_points join sampledb.sites on analysis_points.site_id = sites.id where sites.name = %s and analysis_points.analysis_number = %s', (validation_sitename, validation_analysis_number))
validation_analysisid = conn.fetchone()[0]

validationfilepath = r"O:\Validation_Reports"
curdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y_%m_%d')
curtime = datetime.datetime.strftime(datetime.datetime.now(), '%H-%M')
validation_filename = curdate + '_' + validation_sitename + '_A' + str(validation_analysis_number) + '_ValidationSummary_' + curtime + '.xlsx'

validationfilepath = validationfilepath + "\\" + validation_filename

conn.execute("INSERT INTO sampledb.validation_files(path) values(%s)", (validationfilepath))

conn.execute("SELECT id from sampledb.validation_files where path = %s", (validationfilepath))

validation_fileid = conn.fetchone()[0]

conn.execute("INSERT INTO sampledb.validation_reports(analysis_id, date_validated, num_samples, file_id, phase) VALUES (%s, CURDATE(), %s, %s, %s)", (validation_analysisid, validation_numsamples, validation_fileid, validation_phase))

conn.execute("SELECT validation_reports.id from sampledb.validation_reports where analysis_id = %s and num_samples = %s and file_id = %s and phase = %s", (validation_analysisid, validation_numsamples, validation_fileid, validation_phase))

validation_report_id = conn.fetchone()[0]


#end of preparing for validation report value entry. Header information stored in validation_reports, filepath stored in validation_files

while curcell != None:
    curcol = 4
    curValue = sh.cell(row = samplerow, column = curcol).value
    
    conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s order by sample_id", (curcell, sh.cell(row = samplerow, column = 2).value, sh.cell(row = samplerow, column = 3).value))
    sample_id = conn.fetchall()
    if len(sample_id) > 1:
        print "make descions.......", curcell, sh.cell(row = curRow, column = curcol + 1).value, sh.cell(row = curRow, column = curcol + 2).value
        sample_id = sample_id[0][0]
    else:
        sample_id = sample_id[0][0]    
    
    conn.execute("SELECT starttime, temp, pres, site, analysis_number from sampledb.sample_info where sample_id = %s", (sample_id))
    
    results = conn.fetchall()
    starttime = results[0][0]
    temp = results[0][1]
    pres = results[0][2]
    sitename = results[0][3]
    analysis_number = results[0][4]
    
    summarySheet.cell(row = 1, column = curcolumnForSampleDate - 1).value = 'Sample Date'
    summarySheet.cell(row = 1, column = curcolumnForSampleDate - 1).font = font
    summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate - 1)].width = len((summarySheet.cell(row = 1, column = (curcolumnForSampleDate - 1)).value)) + 2
    summarySheet.cell(row = 1, column = curcolumnForSampleDate).value = starttime
    summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate)].width = len((summarySheet.cell(row = 1, column = curcolumnForSampleDate).value).strftime('%Y-%m-%d %H:%M:%S'))
    summarySheet.cell(row = 1, column = curcolumnForAnalysisPoint).value = str(sitename) + " A" + str(analysis_number)
    summarySheet.cell(row = 2, column = curcolumnFortempAndPres - 1).value = 'Temperature'
    summarySheet.cell(row = 2, column = curcolumnFortempAndPres - 1).font = font
    summarySheet.column_dimensions[get_column_letter(curcolumnForAnalysisPoint)].width = len((summarySheet.cell(row = 2, column = curcolumnForAnalysisPoint).value)) + 8
    summarySheet.cell(row = 2, column = curcolumnFortempAndPres).value = temp
    summarySheet.cell(row = 3, column = curcolumnFortempAndPres - 1).value = 'Pressure'
    summarySheet.cell(row = 3, column = curcolumnFortempAndPres - 1).font = font
    summarySheet.cell(row = 3, column = curcolumnFortempAndPres).value = pres
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart).value = 'Verax'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 1).value = 'Verax Stdev'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 1).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 2).value = 'RMSECV'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 2).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 3).value = '2xRMSECV'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 3).font = font    
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 4).value = 'Maxxam'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 4).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 5).value = 'Delta'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 5).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 6).value = 'Average T2'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 6).font = font
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 7).value = 'Average Q'
    summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 7).font = font
    
    orderCounter = 0
    numSpectraForSample = spectraPerSample[sample_id]
    veraxSum = 0
    while orderCounter < len(orderOfResults):
        while curValue != None:
            nextResultName = orderOfResults[orderCounter]

            if resultNameMap.has_key(nextResultName.lower()):
                resultname = nextResultName
                valueCol = resultNameMap[nextResultName.lower()]
                averageValue = 0
                averageTs = 0
                averageQ = 0
                values = []
                for i in range(0, numSpectraForSample-1):
                    averageValue += sh.cell(row = (samplerow + i), column = valueCol).value
                    averageTs += sh.cell(row = (samplerow + i), column = valueCol + 1).value
                    averageQ += sh.cell(row = (samplerow + i), column = valueCol + 2).value
                    values.append(sh.cell(row = (samplerow + i), column = valueCol).value)
                    
                #need to add this average value to the new spreadsheet with all the overview shit on it.
                
                averageValue = averageValue / (numSpectraForSample - 1)
                averageTs = averageTs / (numSpectraForSample - 1)
                averageQ = averageQ / (numSpectraForSample - 1)
                VeraxStdev = numpy.std(values)
                if resultname != 'API Gravity' and resultname != 'VPCR4' and resultname.lower().find('vapor') < 0 and resultname != 'Dry BTU' and resultname != 'Relative Density' and resultname != 'TVP_N2CO2_JP3' and resultname != 'Dry Vapor Pressure (D5191)':
                    veraxSum += averageValue
                
                
                old_unit_name = unit_name
                
                if resultname == 'API Gravity' or resultname == 'Relative Density':
                    old_unit_name = unit_name
                    unit_name = 'None'
                    
                if resultname == 'Dry BTU':
                    old_unit_name = unit_name
                    unit_name = 'BTU/cf'
                       
                if resultname == 'VPCR4':
                    old_unit_name = unit_name
                    unit_name = 'psi'
                    
                if resultname == 'Cu. Ft. Vapor':
                    resultname = 'Cu. Ft. Vapor Per Gallon'
                    unit_name = 'cf/gal'
                    
                if resultname == 'TVP_N2CO2_JP3':
                    resultname = 'TVP_N2CO2_JP3'
                    unit_name = 'psia'
                    
                if resultname == 'Dry Vapor Pressure (D5191)':
                    resultname = 'Dry Vapor Pressure (D5191)'
                    unit_name = 'psi'
                    
                    
                #is this the best spot to place the validation value entry into the database? Units are an issue.
                conn.execute("select id from sampledb.result_names where name = %s", (resultname))
                validation_resultid = conn.fetchone()[0]
                
                conn.execute("select id from sampledb.units where name = %s", (unit_name))
                validation_unitid = conn.fetchone()[0]
                
                conn.execute("insert into sampledb.validation_results(report_id, sample_id, result_id, unit_id, value, prediction_stdev, avg_t2, avg_q) values (%s, %s, %s, %s, %s, %s, %s, %s)", (validation_report_id, sample_id, validation_resultid, validation_unitid, averageValue, VeraxStdev, averageTs, averageQ))
                
                conn.execute("SELECT value from sampledb.sample_results where result_name = '" + str(resultname) + "' and unit_name = '" + str(unit_name) + "' and sample_id = " + str(sample_id))
                
                maxxamvalue = conn.fetchall()
                
                conn.execute('select rmsecv from sampledb.modeled_results where site = %s and analysis_number = %s and result_name = %s and unit_name = %s', (sitename, analysis_number, resultname, unit_name))
                
                rmsecvValue = conn.fetchall()
                
                if rmsecvValue != ():
                    rmsecvValue = rmsecvValue[0][0]
                else:
                    rmsecvValue = 'No RMSECV found.'
                
                unit_name = old_unit_name
                
                if maxxamvalue == ():
                    maxxamvalue = 'No Maxxam Result'
                else:
                    maxxamvalue = maxxamvalue[0][0]
                    
                delta = 0
                
                if maxxamvalue != 'No Maxxam Result':
                    delta = averageValue - maxxamvalue
                else:
                    delta = 'No Value'
                    
                
                if resultname == 'API Gravity' or resultname.lower().find('vapor') >= 0 or resultname == 'VPCR4' or resultname == 'Dry BTU' or resultname == 'Relative Density' or resultname == 'TVP_N2CO2_JP3' or resultname == 'Dry Vapor Pressure (D5191)':
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = resultname
                if resultname != 'API Gravity' and resultname.lower().find('vapor') < 0 and resultname != 'VPCR4' and resultname != 'Dry BTU' and resultname != 'Relative Density' and resultname != 'TVP_N2CO2_JP3':
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = (resultname + " [" + unit_name + "]")
                summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).font = font
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).value = averageValue
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).value = VeraxStdev
                summarySheet.cell(row = curRowForResults, column = curcolumnForRMSECV).value = rmsecvValue
                if rmsecvValue == 'No RMSECV found.':
                    summarySheet.cell(row = curRowForResults, column = curcolumnFor2xRMSECV).value = rmsecvValue
                #if rmsecvValue != 'No RMSECV found.':
                 #   summarySheet.cell(row = curRowForResults, column = curcolumnFor2xRMSECV).value = 2 * rmsecvValue
                summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).value = maxxamvalue
                summarySheet.cell(row = curRowForResults, column = curcolumnForTs).value = averageTs
                summarySheet.cell(row = curRowForResults, column = curcolumnForQ).value = averageQ
                summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).value = delta
                summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForRMSECV).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnFor2xRMSECV).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForTs).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForQ).number_format = '0.000'
                summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).number_format = '0.000'                
                
                #if summarySheet.cell(row = curRowForResults, column = curcolumnForDelta - 1).value != 'No Maxxam Result' and summarySheet.cell(row = curRowForResults, column = curcolumnForRMSECV).value != 'No RMSECV found.' :
                 #   summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='greaterThan', formula=[str(2 * summarySheet.cell(row = curRowForResults, column = curcolumnForRMSECV).value)], stopIfTrue=True, fill=yellowFill))
                
                  #  summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='lessThan', formula=['-' + str(2 * summarySheet.cell(row = curRowForResults, column = curcolumnForRMSECV).value)], stopIfTrue=True, fill=yellowFill))              
                
                
                curRowForResults += 1
                break
                
            curcol += 1
            curValue = sh.cell(row = samplerow, column = curcol).value
            
            
        curcol = 4
        curValue = sh.cell(row = samplerow, column = curcol).value
        orderCounter += 1
    
    summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).value = 'Verax Sum'
    summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).font = font
    summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).value = veraxSum
    summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).number_format = '0.000'
    samplerow += (numSpectraForSample - 1)
    curcell = sh.cell(row = samplerow, column = 1).value
    curcolumnForSampleDate += 10
    curcolumnForVeraxValue += 10 
    curcolumnForVeraxStdev += 10
    curcolumnForMaxxamValue += 10
    curcolumnForRMSECV += 10
    curcolumnFor2xRMSECV += 10
    curcolumnFortempAndPres += 10
    curcolumnForresultNames += 10
    curcolumnForHeaderStart += 10
    curcolumnForDelta += 10
    curcolumnForAnalysisPoint += 10
    curcolumnForTs += 10
    curcolumnForQ += 10
    curRowForResults = 5    
    
    
#part of the code that does the averages. This can't be the best way.

summarySheet.column_dimensions['E'].width = 15

numResults = len(resultNameMap)
curRowForAverageReport = numResults + 8
summarySheet.cell(row = curRowForAverageReport, column = 1).value = summarySheet.cell(row = 1, column = 1).value
summarySheet.cell(row = curRowForAverageReport, column = 1).font = font
    
numSamples = len(spectraPerSample)
summarySheet.cell(row = curRowForAverageReport + 1, column = 1).value = 'Number of Samples'
summarySheet.cell(row = curRowForAverageReport + 1, column = 1).font = font
summarySheet.cell(row = curRowForAverageReport + 1, column = 2).value = numSamples

curRowForAverageRepHeaders = curRowForAverageReport + 3

summarySheet.cell(row = curRowForAverageRepHeaders, column = 2).value = 'Verax'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 3).value = 'Verax Range'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 4).value = 'Maxxam'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 5).value = 'Maxxam Range'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 6).value = 'Delta'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 7).value = 'Delta Stdev'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 8).value = 'Average T2'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 9).value = 'Average Q'
summarySheet.cell(row = curRowForAverageRepHeaders, column = 2).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 3).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 4).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 5).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 6).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 7).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 8).font = font
summarySheet.cell(row = curRowForAverageRepHeaders, column = 9).font = font

curResultRow = 5
curAverageResultRow = curRowForAverageReport + 4

while summarySheet.cell(row = curResultRow, column = 1).value != None:
    VeraxResultValues = []
    MaxxamResultValues = []
    DeltaValues = []
    T2Values = []
    QValues = []
    
    curVeraxValueCol = 2
    curMaxxamValueCol = 6
    curDeltaValueCol = 7
    curT2ValueCol = 8
    curQValueCol = 9
    
    for i in range(0, numSamples):
        VeraxResultValues.append(summarySheet.cell(row = curResultRow, column = curVeraxValueCol).value)
        MaxxamResultValues.append(summarySheet.cell(row = curResultRow, column = curMaxxamValueCol).value)
        DeltaValues.append(summarySheet.cell(row = curResultRow, column = curDeltaValueCol).value)
        T2Values.append(summarySheet.cell(row = curResultRow, column = curT2ValueCol).value)
        QValues.append(summarySheet.cell(row = curResultRow, column = curQValueCol).value)
        
        curVeraxValueCol += 10
        curMaxxamValueCol += 10
        curDeltaValueCol += 10
        curT2ValueCol += 10
        curQValueCol += 10
        
        
    summarySheet.cell(row = curAverageResultRow, column = 1).value = summarySheet.cell(row = curResultRow, column = 1).value
    summarySheet.cell(row = curAverageResultRow, column = 1).font = font
    summarySheet.cell(row = curAverageResultRow, column = 2).value = numpy.average(VeraxResultValues)
    summarySheet.cell(row = curAverageResultRow, column = 3).value = max(VeraxResultValues) - min(VeraxResultValues)
    if not 'No Maxxam Result' in MaxxamResultValues:
        summarySheet.cell(row = curAverageResultRow, column = 4).value = numpy.average(MaxxamResultValues)
        summarySheet.cell(row = curAverageResultRow, column = 5).value = max(MaxxamResultValues) - min(MaxxamResultValues)
    if not 'No Value' in DeltaValues:
        summarySheet.cell(row = curAverageResultRow, column = 6).value = numpy.average(DeltaValues)
        summarySheet.cell(row = curAverageResultRow, column = 7).value = numpy.std(DeltaValues)
    summarySheet.cell(row = curAverageResultRow, column = 8).value = numpy.average(T2Values)
    summarySheet.cell(row = curAverageResultRow, column = 9).value = numpy.average(QValues)
    summarySheet.cell(row = curAverageResultRow, column = 2).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 3).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 4).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 5).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 6).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 7).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 8).number_format = '0.000'
    summarySheet.cell(row = curAverageResultRow, column = 9).number_format = '0.000'
    
    summarySheet.conditional_formatting.add((get_column_letter(6)+str(curAverageResultRow)), CellIsRule(operator='greaterThan', formula=['0.5'], stopIfTrue=True, fill=yellowFill))
                    
    summarySheet.conditional_formatting.add((get_column_letter(6)+str(curAverageResultRow)), CellIsRule(operator='lessThan', formula=['-0.5'], stopIfTrue=True, fill=yellowFill))       
    

    curResultRow += 1
    curAverageResultRow += 1
    
    

wb.save(validationfilepath)

db.commit()
db.close()

os.system('start excel.exe ' + '"' + str(validationfilepath) + '"') 