class DuplicateValidationReportFile(Exception):
    def __init__(self):
        Exception.__init__(self, 'Excel Validation Report Duplicate')