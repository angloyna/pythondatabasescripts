class DuplicateSample(Exception):
    def __init__(self, spectrumTime, spectrumTemp, spectrumPres):
        Exception.__init__(self, 'Spectrum duplicate'+ str(spectrumTime) + ' ' + str(spectrumTemp) + ' ' + str(spectrumPres))
    