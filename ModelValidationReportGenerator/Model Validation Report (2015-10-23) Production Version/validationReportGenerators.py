from modelPredictionOutputReaders import *
from validationReportToDBFunctions import *
import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter
import numpy
import datetime
import MySQLdb

def switchToCorrectPhysPropertyUnits(resultname, unit_name):
    if resultname == 'API Gravity' or resultname == 'Relative Density':
        unit_name = 'None'     
    elif resultname == 'Dry BTU':
        unit_name = 'BTU/cf'
    elif resultname == 'VPCR4' or resultname == 'Dry Vapor Pressure (D5191)' or resultname == 'RVP':
        unit_name = 'psi'
    elif resultname == 'Cu. Ft. Vapor Per Gallon':
        unit_name = 'cf/gal'
    elif resultname == 'TVP_N2CO2_JP3':
        unit_name = 'psia'
    return unit_name


def getDBconnection(dbName):
    return MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", dbName)

def setConditionalFormatting(summarySheet, curcolumnForDelta, curRowForResults, magnitude):
    yellowFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')
    summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='greaterThan', formula=[str(magnitude)], stopIfTrue=True, fill=yellowFill))
    summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='lessThan', formula=['-'+str(magnitude)], stopIfTrue=True, fill=yellowFill))     

def setFont(sheet, font, curRow, columns):
    for column in columns:
        sheet.cell(row = curRow, column = column).font = font

def generateAverageReport(summarySheet, spectraPerSample, resultNameMap, validation_report_id):
    reportStartColumn = 11
    veraxValueColumn = 12
    veraxRangeColumn = 13
    labValueColumn = 14
    labRangeColumn = 15
    absDeltaColumn = 16
    deltaColumn = 17
    deltaStdevColumn = 18
    T2Column = 19
    QColumn = 20
    
    averageReportStartRow = 1
    curRowForAverageRepHeaders = averageReportStartRow + 3
    numResults = len(resultNameMap)
    numRowsToNextIndividualReport = numResults + 7
    
    font = Font(name='Calibri', size=9, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')
    resultsFont = Font(name='Calibri', size=9, bold=False,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')
      
    summarySheet.cell(row = averageReportStartRow, column = reportStartColumn).value = summarySheet.cell(row = 1, column = 1).value + ' Average Report'
    setFont(summarySheet,font, averageReportStartRow, [reportStartColumn])
    
    numSamples = len(spectraPerSample)
    
    summarySheet.cell(row = averageReportStartRow, column = 12).value = "Report ID: " + str(validation_report_id)
    summarySheet.cell(row = averageReportStartRow, column = 12).font = font
    summarySheet.cell(row = averageReportStartRow + 1, column = 11).value = 'Number of Samples'
    summarySheet.cell(row = averageReportStartRow + 1, column = 11).font = font
    summarySheet.cell(row = averageReportStartRow + 1, column = 12).value = numSamples
     
    headerNameToColumnMap = {'Verax':12, 'Verax Range': 13, 'Third Party Lab': 14, 'Lab Range':15, 'Absolute Delta' : 16, 'Delta':17, 'Delta Stdev': 18, 'Average T2':19, 'Average Q':20}
           
    for key in headerNameToColumnMap:
        summarySheet.cell(row = curRowForAverageRepHeaders, column = headerNameToColumnMap[key]).value = key
        summarySheet.cell(row = curRowForAverageRepHeaders, column = headerNameToColumnMap[key]).font = font
            
    curResultRow = 5
    curAverageResultRow = averageReportStartRow + 4
    
    while summarySheet.cell(row = curResultRow, column = 1).value != None:
        
        VeraxResultValues = []
        MaxxamResultValues = []
        DeltaValues = []
        T2Values = []
        QValues = []
        
        curVeraxValueCol = 2
        curMaxxamValueCol = 6
        curDeltaValueCol = 7
        curT2ValueCol = 8
        curQValueCol = 9
        curResultNameCol = 1
        
        AverageReportResultRow = curResultRow
        curResultName = summarySheet.cell(row = curResultRow, column = curResultNameCol).value
        
        for i in range(0, numSamples):
            
            VeraxResultValues.append(summarySheet.cell(row = curResultRow, column = curVeraxValueCol).value)
            MaxxamResultValues.append(summarySheet.cell(row = curResultRow, column = curMaxxamValueCol).value)
            DeltaValues.append(summarySheet.cell(row = curResultRow, column = curDeltaValueCol).value)
            T2Values.append(summarySheet.cell(row = curResultRow, column = curT2ValueCol).value)
            QValues.append(summarySheet.cell(row = curResultRow, column = curQValueCol).value)
            
            curResultRow += numRowsToNextIndividualReport
            
        curResultRow = AverageReportResultRow
            
        summarySheet.cell(row = curAverageResultRow, column = reportStartColumn).value = summarySheet.cell(row = curResultRow, column = 1).value
        summarySheet.cell(row = curAverageResultRow, column = veraxValueColumn).value = numpy.average(VeraxResultValues)
        summarySheet.cell(row = curAverageResultRow, column = veraxRangeColumn).value = max(VeraxResultValues) - min(VeraxResultValues)
        setFont(summarySheet,font,curAverageResultRow, [reportStartColumn])
        setFont(summarySheet,resultsFont,curAverageResultRow,[veraxValueColumn,veraxRangeColumn])     
        
        if not 'No Lab Result' in MaxxamResultValues:
            summarySheet.cell(row = curAverageResultRow, column = labValueColumn).value = numpy.average(MaxxamResultValues)
            summarySheet.cell(row = curAverageResultRow, column = labRangeColumn).value = max(MaxxamResultValues) - min(MaxxamResultValues)
            absDeltaValues = [abs(x) for x in DeltaValues]
            summarySheet.cell(row = curAverageResultRow, column = absDeltaColumn).value = numpy.average(absDeltaValues)
            summarySheet.cell(row = curAverageResultRow, column = deltaColumn).value = numpy.average(DeltaValues)
            summarySheet.cell(row = curAverageResultRow, column = deltaStdevColumn).value = numpy.std(DeltaValues)
            
        summarySheet.cell(row = curAverageResultRow, column = T2Column).value = numpy.average(T2Values)
        summarySheet.cell(row = curAverageResultRow, column = QColumn).value = numpy.average(QValues)
        setFont(summarySheet, resultsFont, curAverageResultRow, [labValueColumn,labRangeColumn,absDeltaColumn,deltaColumn,deltaStdevColumn,T2Column,QColumn])      
        
        for key in headerNameToColumnMap:
            summarySheet.cell(row = curAverageResultRow, column = headerNameToColumnMap[key]).number_format = '0.000'
            
        if curResultName != 'API Gravity':
            setConditionalFormatting(summarySheet, absDeltaColumn, curAverageResultRow, .5)
            setConditionalFormatting(summarySheet, deltaColumn, curAverageResultRow, .5)
        elif curResultName == 'API Gravity':
            setConditionalFormatting(summarySheet, absDeltaColumn, curAverageResultRow, 5)
            setConditionalFormatting(summarySheet, deltaColumn, curAverageResultRow, 5)            
        
        curResultRow += 1
        curAverageResultRow += 1
        
    summarySheet.column_dimensions[get_column_letter(reportStartColumn)].width = len(summarySheet.cell(row = averageReportStartRow, column = reportStartColumn).value) + 5
    summarySheet.column_dimensions[get_column_letter(labRangeColumn)].width = len(summarySheet.cell(row = curRowForAverageRepHeaders, column = labRangeColumn).value) + 3
    summarySheet.column_dimensions[get_column_letter(absDeltaColumn)].width = len(summarySheet.cell(row = curRowForAverageRepHeaders, column = absDeltaColumn).value) + 3
    summarySheet.column_dimensions[get_column_letter(labValueColumn)].width = len(summarySheet.cell(row = curRowForAverageRepHeaders, column = labValueColumn).value) + 3
        
def getSampleHeaderInformation(sample_id):

    db = getDBconnection("sampledb")  
    conn = db.cursor() 
    
    try:
        conn.execute("SELECT starttime, temp, pres, site, analysis_number from sampledb.sample_info where sample_id = %s", (sample_id))
        results = conn.fetchone()
    
        starttime = results[0]
        temp = results[1]
        pres = results[2]
        sitename = results[3]
        analysis_number = results[4]
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)
        
    return (starttime, temp, pres, sitename, analysis_number)
         
def getSampleIDfromSpectrumInfo(time, temp, pres):
    
    db = getDBconnection("sampledb")  
    conn = db.cursor()
    
    try:
        conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s", (time, temp, pres))
    
        sample_id = conn.fetchall()[0][0]
    
        db.close()
        
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)
    
    return sample_id
      
def getLabValue(result_name, unit_name, sample_id, conn):
    
    try:
        conn.execute("SELECT value from sampledb.sample_results where result_name = '" + str(result_name) + "' and unit_name = '" + str(unit_name) + "' and sample_id = " + str(sample_id))
    
        maxxamvalue = conn.fetchall()
    
        if maxxamvalue == ():
            maxxamvalue = 'No Lab Result'
        else:
            maxxamvalue = maxxamvalue[0][0]
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)
        
    return maxxamvalue
    
    
def addVeraxPredictionToDB(validation_report_id, sample_id, resultname, unit_name, averageValue, VeraxStdev, averageTs, averageQ, conn): 
    
    try:
        conn.execute("select id from sampledb.result_names where name = %s", (resultname))
        validation_resultid = conn.fetchone()[0]
    
        conn.execute("select id from sampledb.units where name = %s", (unit_name))
        validation_unitid = conn.fetchone()[0]
    
        conn.execute("insert into sampledb.validation_results(report_id, sample_id, result_id, unit_id, value, prediction_stdev, avg_t2, avg_q) values (%s, %s, %s, %s, %s, %s, %s, %s)", (validation_report_id, sample_id, validation_resultid, validation_unitid, averageValue, VeraxStdev, averageTs, averageQ))
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)
    

def performValidationComparison(ModelPredictionWS, summarySheet, spectraPerSample, resultNameMap, unit_name, validation_report_id, selfValidatedDict):

    sh = ModelPredictionWS

    font = Font(name='Calibri', size=9, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')       
    resultsFont = Font(name='Calibri', size=9, bold=False,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')
    
    greenFill = PatternFill(start_color='33FF00', end_color='33FF00', fill_type='solid')  
    orangeFill = PatternFill(start_color='FFCC00', end_color='FFCC00', fill_type='solid')
    
    samplerow = 2
    curcell = sh.cell(row = samplerow, column = 1).value
    
    curcolumnForSampleDate = 4
    curcolumnForAnalysisPoint = 1
    curcolumnForVeraxValue = 2
    curcolumnForVeraxStdev = 3
    curcolumnForRMSECV = 4
    curcolumnFor2xRMSECV = 5
    curcolumnForMaxxamValue = 6
    curcolumnFortempAndPres = 2
    curcolumnForresultNames = 1
    curcolumnForDelta = 7
    curcolumnForTs = 8
    curcolumnForQ = 9
    
    curRowForResults = 5
    curRowForSampleDate = 1
    curRowForSite = 1
    curRowForTemperature = 2
    curRowForPressure = 3
    curRowForHeaders = 4
    
    numRowsToNextIndividualReport = 7
    
    ComparisonReportHeaderColumns = {'Verax' : curcolumnForVeraxValue, 'Verax Stdev' : curcolumnForVeraxStdev, 'RMSECV' : curcolumnForRMSECV, '2xRMSECV' : curcolumnFor2xRMSECV, 'Third Party Lab' : curcolumnForMaxxamValue, 'Delta' : curcolumnForDelta, 'Average T2' : curcolumnForTs, 'Average Q' : curcolumnForQ}
    
    nonSpeciationResults = ['API Gravity', 'VPCR4', 'Dry BTU', 'Relative Density', 'Cu. Ft. Vapor Per Gallon', 'Dry Vapor Pressure (D5191)', 'RVP', 'TVP_N2CO2_JP3', 'GPM Total C2+']
    
    orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'c12+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor Per Gallon', 'GPM Total C2+', 'VPCR4', 'RVP', 'TVP_N2CO2_JP3', 'Dry Vapor Pressure (D5191)']
    
    db = getDBconnection("sampledb")
    conn = db.cursor()

    summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate + 1)].width = 16
    summarySheet.column_dimensions['F'].width = 16
    summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate)].width = 20
    
    while curcell != None:
        curcol = 4
        curValue = sh.cell(row = samplerow, column = curcol).value
        curTemp = sh.cell(row = samplerow, column = 2).value
        curPres = sh.cell(row = samplerow, column = 3).value
        
        sample_id = getSampleIDfromSpectrumInfo(curcell, curTemp, curPres)

        starttime, temp, pres, sitename, analysis_number = getSampleHeaderInformation(sample_id)
        
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate - 1).value = 'Sample Date'
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate - 1).font = font
        summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate - 1)].width = len((summarySheet.cell(row = curRowForSampleDate, column = (curcolumnForSampleDate - 1)).value)) + 2
        
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate).value = starttime
        
        
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate + 2).value = "Sample ID: " + str(sample_id)

        
        if selfValidatedDict[sample_id] == False:
            summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate+ 1).value = "Independent"
            summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate+ 1).fill = greenFill
        elif selfValidatedDict[sample_id] == True:
            summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate+ 1).value = "Self-Validated"
            summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate+ 1).fill = orangeFill
            
        
            
        summarySheet.cell(row = curRowForSite, column = curcolumnForAnalysisPoint).value = str(sitename) + " A" + str(analysis_number)
        summarySheet.column_dimensions[get_column_letter(curcolumnForAnalysisPoint)].width = len((summarySheet.cell(row = curRowForSite, column = curcolumnForAnalysisPoint).value)) + 8
        
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres - 1).value = 'Temperature'
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres).value = temp
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres).number_format = '0.00'
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres - 1).value = 'Pressure'
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres).number_format = '0.0'
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres).value = pres
        
        for key in ComparisonReportHeaderColumns:
            summarySheet.cell(row = curRowForHeaders, column = ComparisonReportHeaderColumns[key]).value = key
            summarySheet.cell(row = curRowForHeaders, column = ComparisonReportHeaderColumns[key]).font = font            
                
        orderCounter = 0
        numSpectraForSample = spectraPerSample[sample_id]
        veraxSum = 0
                
        while orderCounter < len(orderOfResults):
            nextResultName = orderOfResults[orderCounter]
                
            if resultNameMap.has_key(nextResultName.lower()):
                resultname = nextResultName
                valueCol = resultNameMap[nextResultName.lower()]
                averageValue = 0
                averageTs = 0
                averageQ = 0
                values = []
                
                for i in range(0, numSpectraForSample):
                    averageValue += sh.cell(row = (samplerow + i), column = valueCol).value
                    averageTs += sh.cell(row = (samplerow + i), column = valueCol + 1).value
                    averageQ += sh.cell(row = (samplerow + i), column = valueCol + 2).value
                    values.append(sh.cell(row = (samplerow + i), column = valueCol).value)
                    
                
                averageValue = averageValue / (numSpectraForSample)
                averageTs = averageTs / (numSpectraForSample)
                averageQ = averageQ / (numSpectraForSample)
                VeraxStdev = numpy.std(values)
                
                if resultname not in nonSpeciationResults:
                    veraxSum += averageValue
                 
                old_unit_name = unit_name
                unit_name = switchToCorrectPhysPropertyUnits(resultname, unit_name)
                  
                addVeraxPredictionToDB(validation_report_id, sample_id, resultname, unit_name, averageValue, VeraxStdev, averageTs, averageQ,conn)
                db.commit()
                maxxamvalue = getLabValue(resultname, unit_name, sample_id, conn)
                
                unit_name = old_unit_name
                    
                delta = 0
                
                if maxxamvalue != 'No Lab Result':
                    delta = averageValue - maxxamvalue
                else:
                    delta = 'No Value'
                
                if resultname in nonSpeciationResults:
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = resultname
                else:
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = (resultname + " [" + unit_name + "]")
                
                summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).number_format = '0.000'   
                summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).font = font
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).value = averageValue
                summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).value = VeraxStdev
                summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).value = maxxamvalue
                summarySheet.cell(row = curRowForResults, column = curcolumnForTs).value = averageTs
                summarySheet.cell(row = curRowForResults, column = curcolumnForQ).value = averageQ
                summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).value = delta
                setFont(summarySheet,resultsFont,curRowForResults,[curcolumnForVeraxValue,curcolumnForVeraxStdev,curcolumnForMaxxamValue,curcolumnForTs,curcolumnForQ,curcolumnForDelta])
                
            
                if summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).value != 'No Lab Result':
                    if resultname != 'API Gravity':
                        setConditionalFormatting(summarySheet, curcolumnForDelta, curRowForResults, .5) 
                    elif resultname == 'API Gravity':
                        setConditionalFormatting(summarySheet,curcolumnForDelta, curRowForResults, 5)                           
                
                for key in ComparisonReportHeaderColumns:    
                    summarySheet.cell(row = curRowForResults, column = ComparisonReportHeaderColumns[key]).number_format = '0.000'
                    
                                                          
                curRowForResults += 1
                                    
            orderCounter += 1
        
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).value = 'Verax Sum'
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).font = font
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).value = veraxSum
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).number_format = '0.00'
        
        samplerow += (numSpectraForSample)
        curcell = sh.cell(row = samplerow, column = 1).value
                
        curRowForResults += numRowsToNextIndividualReport
        curRowForSampleDate += len(resultNameMap) + numRowsToNextIndividualReport
        curRowForSite += len(resultNameMap) + numRowsToNextIndividualReport
        curRowForTemperature += len(resultNameMap) + numRowsToNextIndividualReport
        curRowForPressure += len(resultNameMap) + numRowsToNextIndividualReport
        curRowForHeaders += len(resultNameMap) + numRowsToNextIndividualReport
        
    db.commit()
    db.close()