import unittest
from modelPredictionOutputReaders import *
from openpyxl import load_workbook
from DuplicateSampleException import *

class TestOutputReaders(unittest.TestCase):
       
    def test_mapResultNametoModelPredictorColumn(self):
        excelfilename = 'C:/Users/agloyna/Desktop/2015_10_16_NatriumFractA9_ModelPrediction.xlsx'
        wb = load_workbook(filename = excelfilename)
        sh = wb.worksheets[0]
        testResultNameMap = {'api gravity' : 4, 'c1' : 7, 'c2' : 10, 'c3' : 13, 'ic4' : 16, 'ic5' : 19, 'nc4' : 22, 'nc5' : 25}
        outputMap = mapResultNametoModelPredictorColumn(sh, 1, 4)
        for key in outputMap:
            self.assertEqual(outputMap[key], testResultNameMap[key])
            
        
    def test_findNumSpectraPerSample(self):
        excelfilename = 'C:/Users/agloyna/Desktop/2015_10_16_NatriumFractA9_ModelPrediction.xlsx'
        expectedSpectraMap = {6548 : 13, 6562 : 12, 6798 : 9}
        wb = load_workbook(filename = excelfilename)
        sh = wb.worksheets[0]      
        ModelPredictionResultsStartRow = 2
        dateStampCol = 1
        tempCol = 2
        presCol = 3        
        spectraMap = findNumSpectraPerSample(sh, ModelPredictionResultsStartRow, dateStampCol, tempCol, presCol)
        for key in spectraMap:
            self.assertEqual(spectraMap, expectedSpectraMap)
           
    def test_matchSpectrumToSampleID(self):
        TruespectraMap = {256: 8}
        testSpectraMap = {}
        spectraTimes = ['2015-02-19 12:55:09','2015-02-19 12:58:03','2015-02-19 12:59:15','2015-02-19 13:00:15','2015-02-19 13:01:15','2015-02-19 13:02:15','2015-02-19 13:03:15','2015-02-19 13:04:15']
        spectraTemps = [82.9,83.1,83.3,83.4,84.3,84,85.1,85.8]
        spectraPres = [92,93,92.4,92.2,92.7,93.6,94.1,93.2]
        for i in range(0,len(spectraTimes)):
            testSpectraMap = matchSpectrumToSampleID(testSpectraMap, spectraTimes[i], spectraTemps[i], spectraPres[i])
        for key in testSpectraMap:
            self.assertEqual(testSpectraMap, TruespectraMap)
            
    def test_matchSpectrumToSampleID_raiseException(self):
        testSpectraMap = {}
        spectraTimes = ['2015-09-23 11:57:00', '2015-09-23 11:57:30', '2015-09-23 11:58:00','2015-09-23 11:59:00','2015-09-23 11:59:30','2015-09-23 12:00:00','2015-09-23 12:00:30','2015-09-23 12:01:00','2015-09-23 12:01:30','2015-09-23 12:02:00']
        spectraTemps = [99.8,102.7,98.6,99.3,99.3,100,100,101.1,100.7,102.2,103.1]
        spectraPres = [87.8,87.6,87.9,87.8,87.7,87.4,88.1,87.8,88.2,87.3,87.5]
        with self.assertRaisesRegexp(DuplicateSample,'Spectrum duplicate'):
            for i in range(0,len(spectraTimes)):
                matchSpectrumToSampleID(testSpectraMap, spectraTimes[i], spectraTemps[i], spectraPres[i])
            

    
suite = unittest.TestLoader().loadTestsFromTestCase(TestOutputReaders)
unittest.TextTestRunner(verbosity=2).run(suite)
     
