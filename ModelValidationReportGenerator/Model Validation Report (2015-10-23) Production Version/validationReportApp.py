import openpyxl
from openpyxl import load_workbook
import os
import sys
import datetime
from modelPredictionOutputReaders import *
from validationReportToDBFunctions import *
from validationReportGenerators import *
from Tkinter import *
import tkFileDialog
import tkMessageBox
import MySQLdb

def browsebutton(entryobject,t):
    file = tkFileDialog.askopenfilename(parent=t,title='Choose a file')
    entryobject.insert(INSERT, file )

def go(modelPredictionWB, units, t, modelNotes):
    t.destroy()
    modelValidationReport(modelPredictionWB, units, modelNotes)
    
def getSelfValidationInfoForEachSample(spectraPerSample):
    selfValidatedDictionary = {}
    top = Tk()
    top.withdraw()
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    for key in spectraPerSample:
        conn.execute("SELECT starttime from sampledb.samples where sample_id = %s", (key))
        starttime = conn.fetchone()[0]
        result = tkMessageBox.askyesno("Check Self-Validation Samples", "Is sample " + str(key) + " with starttime " + str(starttime) + " self-validated?",parent=top)
        selfValidatedDictionary[key] = result
    db.close()
    top.destroy()
    return selfValidatedDictionary
    

def addSelfValidationSampleDesignationToDB(selfValidatedDict, validationReportsampleID):
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    for key in selfValidatedDict:
        if selfValidatedDict[key] == True:
            self_val_bool = 'Y'
        elif selfValidatedDict[key] == False:
            self_val_bool = 'N'
        conn.execute("INSERT INTO sampledb.validation_samples(report_id, sample_id, self_val_bool) VALUES (%s,%s,%s)", (validationReportsampleID, key, self_val_bool))
        
    db.commit()
    db.close()


def modelValidationReport(excelfilename, units, modelNotes): 
    excelfilename = excelfilename
    unit_name = units
    validationfilepath = r"O:\Validation_Reports"
    dateStampCol = 1
    tempCol = 2
    presCol = 3
    resultStartColumn = 4
    ModelPredictionResultsStartRow = 2
    resultNameRow = 1

    wb = load_workbook(filename = excelfilename)
    sh = wb.worksheets[0]
    summarySheet = wb.create_sheet()
    
    spectraPerSample = findNumSpectraPerSample(sh, ModelPredictionResultsStartRow, dateStampCol, tempCol, presCol) 
    
    selfValidatedDict = getSelfValidationInfoForEachSample(spectraPerSample)
    
    validationReportsampleID = spectraPerSample.keys()[0]
    
     
    resultNameMap = mapResultNametoModelPredictorColumn(sh, resultNameRow, resultStartColumn)
    
    validation_numsamples = len(spectraPerSample)
    
    validation_sitename, validation_analysis_number, validation_phase = getValidationSiteInformation(validationReportsampleID)

    curdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y_%m_%d')
    curtime = datetime.datetime.strftime(datetime.datetime.now(), '%H-%M')    
    
    validation_fileid, validationfilepath = addValidationFiletoDB(validationfilepath,validation_sitename, validation_analysis_number, curdate, curtime)
    
    validation_report_id = addValidationReportToDB(validation_sitename, validation_analysis_number, validation_numsamples,validation_fileid, validation_phase, modelNotes)
    
    addSelfValidationSampleDesignationToDB(selfValidatedDict, validation_report_id)
    
    performValidationComparison(sh, summarySheet, spectraPerSample, resultNameMap, unit_name, validation_report_id, selfValidatedDict)
    
    generateAverageReport(summarySheet, spectraPerSample, resultNameMap, validation_report_id)
    
    wb.save(validationfilepath)
    
    os.system('start excel.exe ' + '"' + str(validationfilepath) + '"')
     

def ValidationReportApp():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Validation Report Generator')
    labelframe = LabelFrame(top, text="Model Prediction File Entry")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    eddFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    eddFrame.pack(fill=X, padx=5, pady=5)        
    eddlabel = Label(eddFrame, text = 'Model Prediction File: ', justify = LEFT, width = 20)
    eddlabel.pack(side = LEFT)
    q = Entry(eddFrame, width = 70)
    q.pack(side = LEFT)
    eddbutton = Button(eddFrame, width = 10, text = 'Browse',command= lambda: browsebutton(q, top))
    eddbutton.pack(side = LEFT)
    
    notesFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    notesFrame.pack(fill=X, padx=5, pady=5)        
    noteslabel = Label(notesFrame, text = 'Notes about the models: ', justify = LEFT, width = 20)
    noteslabel.pack(side = LEFT)
    w = Entry(notesFrame, width = 90)
    w.pack(side = LEFT)  
        
    unitsFrame = Frame(labelframe, height=2, bd=1)
    unitsFrame.pack(padx=5, pady=5, side = TOP)    
    unitslabel = Label(unitsFrame, text = 'Units Modeled: ', justify = RIGHT, width = 13)
    unitslabel.pack(side = LEFT)
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()
    
    conn.execute("SELECT name from sampledb.units")
    
    temp = conn.fetchall()
    
    db.close()
    
    unitsOptions = []
    for item in temp:
        unitsOptions.append(item[0])
        
    unitsVar = StringVar(unitsFrame)
    unitsVar.set(unitsOptions[0])
    unitsOptions = apply(OptionMenu, (unitsFrame, unitsVar) + tuple(unitsOptions))
    unitsOptions.pack(side = LEFT)    
    
    gobutton = Button(labelframe, width = 10, text = 'Go', command= lambda: go(q.get(), unitsVar.get(), top, w.get()))
    gobutton.pack(side = BOTTOM, padx = 5, pady = 5)
        
    top.mainloop()     
    
    
ValidationReportApp()