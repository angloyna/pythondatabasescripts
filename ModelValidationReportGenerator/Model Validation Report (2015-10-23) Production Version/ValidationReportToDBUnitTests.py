import unittest
import datetime
from validationReportToDBFunctions import *
from DuplicateValidationReportFileException import *

class TestValidationReportDBFunctions(unittest.TestCase):
    
    def test_getValidationSiteInformation(self):
        
        trueSiteName = 'Lyssy'
        trueAnalysisNumber = 2
        truePhase = 'Liquid'
        sampleId = 4594
        
        self.assertEqual(getValidationSiteInformation(sampleId),(trueSiteName, trueAnalysisNumber, truePhase))
        
    def test_addValidationFileToDBexceptionHandling(self):
        
        validationFileDirectory = r"C:\Users\agloyna\Documents\pythondatabasescripts\ModelValidationReportGenerator\Cleaning Up Validation Report Code\testing"
        validationsitename = "Lyssy"
        validationanalysisnumber = 2
        curdate = '2015_10_19'
        curtime = '10-14' 
        with self.assertRaisesRegexp(DuplicateValidationReportFile,'Excel Validation Report Duplicate'):
            addValidationFiletoDB(validationFileDirectory,validationsitename, validationanalysisnumber, curdate, curtime)
        
        
        
suite = unittest.TestLoader().loadTestsFromTestCase(TestValidationReportDBFunctions)
unittest.TextTestRunner(verbosity=2).run(suite)