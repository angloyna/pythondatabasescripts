import MySQLdb
import datetime
import os
from DuplicateValidationReportFileException import *

def addValidationReportToDB(validation_sitename, validation_analysis_number, validation_numsamples,validation_fileid, validation_phase, modelNotes):
    # This function takes the site name, analysis number, number of samples validated, validation file id, and analysis point phase as parameters.
    # It loads all this information into the sampledb.validation_reports table and returns the primary key associated with the record just created.
    
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    try:
        conn.execute("SELECT analysis_points.id from sampledb.analysis_points join sampledb.sites on analysis_points.site_id = sites.id where sites.name = %s and analysis_points.analysis_number = %s", (validation_sitename, validation_analysis_number))
    
        validation_analysisid = conn.fetchall()[0][0]
    
        conn.execute("INSERT INTO sampledb.validation_reports(analysis_id, date_validated, num_samples, file_id, phase, model_notes) VALUES (%s, CURDATE(), %s, %s, %s, %s)", (validation_analysisid, validation_numsamples, validation_fileid, validation_phase, modelNotes))
        
        conn.execute("SELECT validation_reports.id from sampledb.validation_reports where analysis_id = %s and num_samples = %s and file_id = %s and phase = %s", (validation_analysisid, validation_numsamples, validation_fileid, validation_phase))
    
        validation_report_id = conn.fetchall()[0][0]
        
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)        
    
    db.commit()
    db.close()
    
    return validation_report_id

def getValidationSiteInformation(ValidationReportSampleID):
    # This function takes the token sample_id associated with this validation report to find the name of the site, analysis number, and phase.
    # The site name, analysis number, and phase are returned by the function.
    
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    try:    
        conn.execute('SELECT site, analysis_number, phase from sampledb.sample_info where sample_id = %s', (ValidationReportSampleID))
        validationheaderinfo = conn.fetchone()
        
    except MySQLdb.Error, e:
        try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        except IndexError:
            print "MySQL Error: %s" % str(e)
        
    validation_sitename = validationheaderinfo[0]
    validation_analysis_number = validationheaderinfo[1]
    validation_phase = validationheaderinfo[2]
    
    
    db.close()
    
    return (validation_sitename, validation_analysis_number, validation_phase)

def addValidationFiletoDB(validationFileDirectory, validation_sitename, validation_analysis_number, curdate, curtime):
    # This function takes the directory where the validation reports are filed and the name of the site and analysis point being validated as parameters.
    # It finds the current date and time when the function is called and creates a descriptive file name for the validation report excel file.
    # The time is used at the end of the file name to prevent overwriting a duplicate validation report file.
    # After generating the file name, it creates a record in sampledb.validation_files associated with the complete path to the excel file.
    # If an excel file with that name already exists in the directory, it raises an exception without generating a record.
    # The primary key associated with the record and the filepath are returned by the function.
    
    validation_filename = curdate + '_' + validation_sitename + '_A' + str(validation_analysis_number) + '_ValidationSummary_' + curtime + '.xlsx'    

    validationfilepath = validationFileDirectory + "\\" + validation_filename
    
    if os.path.exists(validationfilepath):
        raise DuplicateValidationReportFile
    
    else:
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        try:
            conn.execute("INSERT INTO sampledb.validation_files(path) values(%s)", (validationfilepath))
        
            conn.execute("SELECT id from sampledb.validation_files where path = %s", (validationfilepath))
        
            validation_fileid = conn.fetchone()[0]
            
        except MySQLdb.Error, e:
            try:
                print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
            except IndexError:
                print "MySQL Error: %s" % str(e)
    
        db.commit()
        db.close()
    
    return validation_fileid, validationfilepath