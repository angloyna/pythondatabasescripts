import openpyxl
import MySQLdb
from DuplicateSampleException import *

def mapResultNametoModelPredictorColumn(ModelPredictorWS, resultNamesRow, resultNameColumn):
    resultNameMap = {}
    curcol = resultNameColumn
    sh = ModelPredictorWS
    curcell = sh.cell(row = resultNamesRow, column = curcol).value
        
    while curcell != None:
        if curcell.find('T2_') == -1 and curcell.find('Q_') == -1:
            resultNameMap[curcell.lower()] = curcol
        curcol += 1
        curcell = sh.cell(row = resultNamesRow, column = curcol).value
        
    return resultNameMap

def findNumSpectraPerSample(ModelPredictorWS, predictionRowStart, dateStampColumn, tempCol, presCol):
    curRow = predictionRowStart
    sh = ModelPredictorWS
    spectraMap = {}
    curcell = sh.cell(row = curRow, column = dateStampColumn).value
    
    while curcell != None:
        spectrumTime = curcell
        spectrumTemp = sh.cell(row = curRow, column = tempCol).value
        spectrumPres = sh.cell(row = curRow, column = presCol).value
        
        spectraMap = matchSpectrumToSampleID(spectraMap, spectrumTime, spectrumTemp, spectrumPres)
            
        curRow += 1
        curcell = sh.cell(row = curRow, column = dateStampColumn).value
        
    return spectraMap

def matchSpectrumToSampleID(spectraMap, spectrumTime, spectrumTemp, spectrumPres):
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01")
    conn = db.cursor()
    
    conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s", (spectrumTime, spectrumTemp, spectrumPres))
    
    sample_id = conn.fetchall()
    
    if len(sample_id) > 1:
        raise DuplicateSample(spectrumTime, spectrumTemp, spectrumPres)
    else:
        sample_id = sample_id[0][0]
    if not spectraMap.has_key(sample_id):
        spectraMap[sample_id] = 1
    elif spectraMap.has_key(sample_id):
        numSamples = spectraMap[sample_id]
        spectraMap[sample_id] = numSamples + 1
        
    db.close()
    return spectraMap