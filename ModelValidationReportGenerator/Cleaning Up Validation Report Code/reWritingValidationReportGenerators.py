from modelPredictionOutputReaders import *
from validationReportToDBFunctions import *
import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter
import numpy
import datetime
import MySQLdb


def generateAverageReport(summarySheet, spectraPerSample, resultNameMap):

    summarySheet.column_dimensions['E'].width = 15
    yellowFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')
    font = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')         
    
    numResults = len(resultNameMap)
    curRowForAverageReport = numResults + 8
    summarySheet.cell(row = curRowForAverageReport, column = 1).value = summarySheet.cell(row = 1, column = 1).value
    summarySheet.cell(row = curRowForAverageReport, column = 1).font = font
        
    numSamples = len(spectraPerSample)
    summarySheet.cell(row = curRowForAverageReport + 1, column = 1).value = 'Number of Samples'
    summarySheet.cell(row = curRowForAverageReport + 1, column = 1).font = font
    summarySheet.cell(row = curRowForAverageReport + 1, column = 2).value = numSamples
    
    curRowForAverageRepHeaders = curRowForAverageReport + 3
    
    averageReportHeaderNames = {'Verax':2, 'Verax Range': 3, 'Maxxam': 4, 'Maxxam Range':5, 'Delta':6, 'Delta Stdev': 7, 'Average T2':8, 'Average Q':9}
    
    for key in averageReportHeaderNames:
        summarySheet.cell(row = curRowForAverageRepHeaders, column = averageReportHeaderNames[key]).value = key
        summarySheet.cell(row = curRowForAverageRepHeaders, column = averageReportHeaderNames[key]).font = font
            
    curResultRow = 5
    curAverageResultRow = curRowForAverageReport + 4
    
    while summarySheet.cell(row = curResultRow, column = 1).value != None:
        VeraxResultValues = []
        MaxxamResultValues = []
        DeltaValues = []
        T2Values = []
        QValues = []
        
        curVeraxValueCol = 2
        curMaxxamValueCol = 6
        curDeltaValueCol = 7
        curT2ValueCol = 8
        curQValueCol = 9
        
        for i in range(0, numSamples):
            VeraxResultValues.append(summarySheet.cell(row = curResultRow, column = curVeraxValueCol).value)
            MaxxamResultValues.append(summarySheet.cell(row = curResultRow, column = curMaxxamValueCol).value)
            DeltaValues.append(summarySheet.cell(row = curResultRow, column = curDeltaValueCol).value)
            T2Values.append(summarySheet.cell(row = curResultRow, column = curT2ValueCol).value)
            QValues.append(summarySheet.cell(row = curResultRow, column = curQValueCol).value)
            
            curVeraxValueCol += 10
            curMaxxamValueCol += 10
            curDeltaValueCol += 10
            curT2ValueCol += 10
            curQValueCol += 10
            
            
        summarySheet.cell(row = curAverageResultRow, column = 1).value = summarySheet.cell(row = curResultRow, column = 1).value
        summarySheet.cell(row = curAverageResultRow, column = 1).font = font
        summarySheet.cell(row = curAverageResultRow, column = 2).value = numpy.average(VeraxResultValues)
        summarySheet.cell(row = curAverageResultRow, column = 3).value = max(VeraxResultValues) - min(VeraxResultValues)
        
        if not 'No Maxxam Result' in MaxxamResultValues:
            summarySheet.cell(row = curAverageResultRow, column = 4).value = numpy.average(MaxxamResultValues)
            summarySheet.cell(row = curAverageResultRow, column = 5).value = max(MaxxamResultValues) - min(MaxxamResultValues)
            
        if not 'No Value' in DeltaValues:
            summarySheet.cell(row = curAverageResultRow, column = 6).value = numpy.average(DeltaValues)
            summarySheet.cell(row = curAverageResultRow, column = 7).value = numpy.std(DeltaValues)
        summarySheet.cell(row = curAverageResultRow, column = 8).value = numpy.average(T2Values)
        summarySheet.cell(row = curAverageResultRow, column = 9).value = numpy.average(QValues)
        
        for key in averageReportHeaderNames:
            summarySheet.cell(row = curAverageResultRow, column = averageReportHeaderNames[key]).number_format = '0.000'
        
        summarySheet.conditional_formatting.add((get_column_letter(6)+str(curAverageResultRow)), CellIsRule(operator='greaterThan', formula=['0.5'], stopIfTrue=True, fill=yellowFill))                
        summarySheet.conditional_formatting.add((get_column_letter(6)+str(curAverageResultRow)), CellIsRule(operator='lessThan', formula=['-0.5'], stopIfTrue=True, fill=yellowFill))       
        
    
        curResultRow += 1
        curAverageResultRow += 1
        
def getSampleHeaderInformation(sample_id):

    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")   
    conn = db.cursor() 
    
    conn.execute("SELECT starttime, temp, pres, site, analysis_number from sampledb.sample_info where sample_id = %s", (sample_id))
    results = conn.fetchone()
    
    starttime = results[0]
    temp = results[1]
    pres = results[2]
    sitename = results[3]
    analysis_number = results[4]
    
    return (starttime, temp, pres, sitename, analysis_number)
         
def getSampleIDfromSpectrumInfo(time, temp, pres):
    
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")   
    conn = db.cursor()    
    
    conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s", (time, temp, pres))
    
    sample_id = conn.fetchall()[0][0]
    
    db.close()
    
    return sample_id
      
def getLabValue(result_name, unit_name, sample_id):
    
    db = MySQLdb.connect("jp3m-db01.jp3m.local","WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    conn.execute("SELECT value from sampledb.sample_results where result_name = '" + str(result_name) + "' and unit_name = '" + str(unit_name) + "' and sample_id = " + str(sample_id))
    
    maxxamvalue = conn.fetchall()
       
    db.close()
    
    if maxxamvalue == ():
        maxxamvalue = 'No Maxxam Result'
    else:
        maxxamvalue = maxxamvalue[0][0]
        
    return maxxamvalue
    
    
def addVeraxPredictionToDB(validation_report_id, sample_id, resultname, unit_name, averageValue, VeraxStdev, averageTs, averageQ):  
    
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    conn.execute("select id from sampledb.result_names where name = %s", (resultname))
    validation_resultid = conn.fetchone()[0]
    
    conn.execute("select id from sampledb.units where name = %s", (unit_name))
    validation_unitid = conn.fetchone()[0]
    
    conn.execute("insert into sampledb.validation_results(report_id, sample_id, result_id, unit_id, value, prediction_stdev, avg_t2, avg_q) values (%s, %s, %s, %s, %s, %s, %s, %s)", (validation_report_id, sample_id, validation_resultid, validation_unitid, averageValue, VeraxStdev, averageTs, averageQ))
    
    db.commit()
    db.close()

def performValidationComparison(ModelPredictionWS, summarySheet, spectraPerSample, resultNameMap, unit_name, validation_report_id):

    sh = ModelPredictionWS
    
    yellowFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')
    font = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')       
    
    samplerow = 2
    curcell = sh.cell(row = samplerow, column = 1).value
    
    curcolumnForSampleDate = 4
    curcolumnForAnalysisPoint = 1
    curcolumnForVeraxValue = 2
    curcolumnForVeraxStdev = 3
    curcolumnForRMSECV = 4
    curcolumnFor2xRMSECV = 5
    curcolumnForMaxxamValue = 6
    curcolumnFortempAndPres = 2
    curcolumnForresultNames = 1
    curcolumnForHeaderStart = 2
    curcolumnForDelta = 7
    curcolumnForTs = 8
    curcolumnForQ = 9
    curRowForResults = 5
    curRowForSampleDate = 1
    curRowForSite = 1
    curRowForTemperature = 2
    curRowForPressure = 3
    curRowForHeaders = 4
    
    ComparisonReportHeaderColumns = {'Verax' : curcolumnForVeraxValue, 'Verax Stdev' : curcolumnForVeraxStdev, 'RMSECV' : curcolumnForRMSECV, '2xRMSECV' : curcolumnFor2xRMSECV, 'Lab Value' : curcolumnForMaxxamValue, 'Delta' : curcolumnForDelta, 'Average T2' : curcolumnForTs, 'Average Q' : curcolumnForQ}
    
    nonSpeciationResults = ['API Gravity', 'VPCR4', 'Dry BTU', 'Relative Density', 'Cu. Ft. Vapor Per Gallon', 'Dry Vapor Pressure (D5191)', 'RVP', 'TVP_N2CO2_JP3', 'GPM Total C2+']
    
    orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'c12+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor Per Gallon', 'GPM Total C2+', 'VPCR4', 'RVP', 'TVP_N2CO2_JP3', 'Dry Vapor Pressure (D5191)']  
    
    while curcell != None:
        curcol = 4
        curValue = sh.cell(row = samplerow, column = curcol).value
        curTemp = sh.cell(row = samplerow, column = 2).value
        curPres = sh.cell(row = samplerow, column = 3).value
        
        sample_id = getSampleIDfromSpectrumInfo(curcell, curTemp, curPres)

        starttime, temp, pres, sitename, analysis_number = getSampleHeaderInformation(sample_id)
        
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate - 1).value = 'Sample Date'
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate - 1).font = font
        summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate - 1)].width = len((summarySheet.cell(row = curRowForSampleDate, column = (curcolumnForSampleDate - 1)).value)) + 2
        
        summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate).value = starttime
        summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate)].width = len((summarySheet.cell(row = curRowForSampleDate, column = curcolumnForSampleDate).value).strftime('%Y-%m-%d %H:%M:%S'))
        
        summarySheet.cell(row = curRowForSite, column = curcolumnForAnalysisPoint).value = str(sitename) + " A" + str(analysis_number)
        summarySheet.column_dimensions[get_column_letter(curcolumnForAnalysisPoint)].width = len((summarySheet.cell(row = curRowForSite, column = curcolumnForAnalysisPoint).value)) + 8
        
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres - 1).value = 'Temperature'
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.cell(row = curRowForTemperature, column = curcolumnFortempAndPres).value = temp
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres - 1).value = 'Pressure'
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.cell(row = curRowForPressure, column = curcolumnFortempAndPres).value = pres
        
        for key in ComparisonReportHeaderColumns:
            summarySheet.cell(row = curRowForHeaders, column = ComparisonReportHeaderColumns[key]).value = key
            summarySheet.cell(row = curRowForHeaders, column = ComparisonReportHeaderColumns[key]).font = font            
                
        orderCounter = 0
        numSpectraForSample = spectraPerSample[sample_id]
        veraxSum = 0
                
        while orderCounter < len(orderOfResults):
            while curValue != None:
                nextResultName = orderOfResults[orderCounter]
    
                if resultNameMap.has_key(nextResultName.lower()):
                    resultname = nextResultName
                    valueCol = resultNameMap[nextResultName.lower()]
                    averageValue = 0
                    averageTs = 0
                    averageQ = 0
                    values = []
                    
                    for i in range(0, numSpectraForSample):
                        averageValue += sh.cell(row = (samplerow + i), column = valueCol).value
                        averageTs += sh.cell(row = (samplerow + i), column = valueCol + 1).value
                        averageQ += sh.cell(row = (samplerow + i), column = valueCol + 2).value
                        values.append(sh.cell(row = (samplerow + i), column = valueCol).value)
                        
                    
                    averageValue = averageValue / (numSpectraForSample)
                    averageTs = averageTs / (numSpectraForSample)
                    averageQ = averageQ / (numSpectraForSample)
                    VeraxStdev = numpy.std(values)
                    
                    if resultname not in nonSpeciationResults:
                        veraxSum += averageValue
                     
                    old_unit_name = unit_name
                    
                    if resultname == 'API Gravity' or resultname == 'Relative Density':
                        unit_name = 'None'
                        
                    elif resultname == 'Dry BTU':
                        unit_name = 'BTU/cf'
                           
                    elif resultname == 'VPCR4' or resultname == 'Dry Vapor Pressure (D5191)' or resultname == 'RVP':
                        unit_name = 'psi'
                        
                    elif resultname == 'Cu. Ft. Vapor Per Gallon':
                        unit_name = 'cf/gal'
                        
                    elif resultname == 'TVP_N2CO2_JP3':
                        unit_name = 'psia'
                        
                        
                    addVeraxPredictionToDB(validation_report_id, sample_id, resultname, unit_name, averageValue, VeraxStdev, averageTs, averageQ)
                    
                    maxxamvalue = getLabValue(resultname, unit_name, sample_id)
                    
                    unit_name = old_unit_name
                        
                    delta = 0
                    
                    if maxxamvalue != 'No Maxxam Result':
                        delta = averageValue - maxxamvalue
                    else:
                        delta = 'No Value'
                    
                    if resultname in nonSpeciationResults:
                        summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = resultname
                    else:
                        summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = (resultname + " [" + unit_name + "]")
                    
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).number_format = '0.000'   
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).font = font
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).value = averageValue
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).value = VeraxStdev
                    summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).value = maxxamvalue
                    summarySheet.cell(row = curRowForResults, column = curcolumnForTs).value = averageTs
                    summarySheet.cell(row = curRowForResults, column = curcolumnForQ).value = averageQ
                    summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).value = delta
                    
                    
                    for key in ComparisonReportHeaderColumns:    
                        summarySheet.cell(row = curRowForResults, column = ComparisonReportHeaderColumns[key]).number_format = '0.000'
                        
                                                              
                    curRowForResults += 1
                    break
                    
                curcol += 1
                curValue = sh.cell(row = samplerow, column = curcol).value
                
                
            curcol = 4
            curValue = sh.cell(row = samplerow, column = curcol).value
            orderCounter += 1
        
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).value = 'Verax Sum'
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames).font = font
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).value = veraxSum
        summarySheet.cell(row = curRowForResults + 1, column = curcolumnForresultNames + 1).number_format = '0.000'
        
        samplerow += (numSpectraForSample)
        curcell = sh.cell(row = samplerow, column = 1).value
        

        
        curRowForResults += 7
        curRowForSampleDate += len(resultNameMap) + 7
        curRowForSite += len(resultNameMap) + 7
        curRowForTemperature += len(resultNameMap) + 7
        curRowForPressure += len(resultNameMap) + 7
        curRowForHeaders += len(resultNameMap) + 7      