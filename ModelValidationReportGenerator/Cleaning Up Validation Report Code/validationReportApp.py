import openpyxl
from openpyxl import load_workbook
import os
import sys
import datetime
from modelPredictionOutputReaders import *
from validationReportToDBFunctions import *
#from ValidationReportGenerators import *
from reWritingValidationReportGenerators import *
def modelValidationReport(): 
    
    unit_name = 'lv %'
    excelfilename = 'C:/Users/agloyna/Desktop/NatriumPrediction.xlsx'
    validationfilepath = r"O:\Validation_Reports"
    dateStampCol = 1
    tempCol = 2
    presCol = 3
    resultStartColumn = 4
    ModelPredictionResultsStartRow = 2
    resultNameRow = 1

    wb = load_workbook(filename = excelfilename)
    sh = wb.worksheets[0]
    summarySheet = wb.create_sheet()
    
    spectraPerSample = findNumSpectraPerSample(sh, ModelPredictionResultsStartRow, dateStampCol, tempCol, presCol) 
        
    validationReportsampleID = spectraPerSample.keys()[0]
        
    resultNameMap = mapResultNametoModelPredictorColumn(sh, resultNameRow, resultStartColumn)
    
    validation_numsamples = len(spectraPerSample)
    
    validation_sitename, validation_analysis_number, validation_phase = getValidationSiteInformation(validationReportsampleID)

    curdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y_%m_%d')
    curtime = datetime.datetime.strftime(datetime.datetime.now(), '%H-%M')    
    
    validation_fileid, validationfilepath = addValidationFiletoDB(validationfilepath,validation_sitename, validation_analysis_number, curdate, curtime)
    
    validation_report_id = addValidationReportToDB(validation_sitename, validation_analysis_number, validation_numsamples,validation_fileid, validation_phase)
    
    performValidationComparison(sh, summarySheet, spectraPerSample, resultNameMap, unit_name, validation_report_id)
    
    #generateAverageReport(summarySheet, spectraPerSample, resultNameMap)
    
    wb.save(validationfilepath)
    
    os.system('start excel.exe ' + '"' + str(validationfilepath) + '"')
     
modelValidationReport()