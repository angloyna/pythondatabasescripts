import MySQLdb





db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
conn = db.cursor()


pressureFactors = {}

conn.execute("SELECT name, vp_psia_gpa2177 from sampledb.result_names where name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+')")

partialPressures = conn.fetchall()


for pressure in partialPressures:
    name = pressure[0]
    gpaValue = pressure[1]
    pressureFactors[name] = gpaValue
    
    
    
conn.execute("SELECT samples.sample_id, methane.value as 'methane', ethane.value as 'ethane', propane.value as 'propane', ibutane.value as 'ibutane', nbutane.value as 'nbutane', ipentane.value as 'ipentane',npentane.value as 'npentane', hexanesplus.value as 'c6 plus' from sampledb.samples join sampledb.results as methane on samples.sample_id = methane.sample_id and methane.result_id = 1 and methane.unit_id = 1 join sampledb.results as ethane on samples.sample_id = ethane.sample_id and ethane.result_id = 2 and ethane.unit_id = 1 join sampledb.results as propane on samples.sample_id = propane.sample_id and propane.result_id = 3 and propane.unit_id = 1 join sampledb.results as ibutane on samples.sample_id = ibutane.sample_id and ibutane.result_id = 4 and ibutane.unit_id = 1 join sampledb.results as nbutane on samples.sample_id = nbutane.sample_id and nbutane.result_id = 5 and nbutane.unit_id = 1 join sampledb.results as ipentane on samples.sample_id = ipentane.sample_id and ipentane.result_id = 6 and ipentane.unit_id = 1 join sampledb.results as npentane on samples.sample_id = npentane.sample_id and npentane.result_id = 7 and npentane.unit_id = 1 join sampledb.results as hexanesplus on samples.sample_id = hexanesplus.sample_id and hexanesplus.result_id = 11 and hexanesplus.unit_id = 1 where samples.phase = 'Liquid' and analysis_type_id in (1,4) and samples.analysis_id not in (122, 123,124)")



results = conn.fetchall()


for result in results:
    methane = (float(result[1]) / 100) * pressureFactors['c1']
    ethane = (float(result[2]) / 100) * pressureFactors['c2']
    propane = (float(result[3]) / 100) * pressureFactors['c3']
    ibutane = (float(result[4]) / 100) * pressureFactors['ic4']
    nbutane = (float(result[5]) / 100) * pressureFactors['nc4']
    ipentane = (float(result[6]) / 100) * pressureFactors['ic5']
    npentane = (float(result[7]) / 100) * pressureFactors['nc5']
    hexanesplus = (float(result[8]) / 100) * pressureFactors['c6+']
    
    
    tvp = methane + ethane + propane + ibutane + nbutane + ipentane + npentane + hexanesplus
    
    print tvp
    
    conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values (%s, 93, 11, %s)", (result[0], tvp))
    
    





db.commit()
db.close()