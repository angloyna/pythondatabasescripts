import MySQLdb
#accepts a dictionary (with db names for species and mole percent values) and a connection to sample database to calculate the TVP of a sample based on composition

def VPCalculator(dict, conn): 
    specDict = dict
    partialpressures = {}
    tvp = 0
    for key in specDict:
        species = key
        conn.execute("SELECT vp_psia_gpa2177 from result_names where name = %s", (species,))
        presfactor = conn.fetchall()[0][0]
        if presfactor == None:
            presfactor = 0
        partialpressures[species] = presfactor * float((specDict[species])) / 100
        
        
    for key in partialpressures:
        tvp += partialpressures[key]
        
        
    return tvp