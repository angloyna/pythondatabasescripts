import MySQLdb
from decimal import Decimal


def checkWtPercentConversion(repwtpercent, calculatedwtpercent):
    checkbool = True
    for key in repwtpercent:
        reportvalue = repwtpercent[key]
        calcvalue = calculatedwtpercent[key]
        if abs(reportvalue - calcvalue) >= .4:
            checkbool = False
            print key
            print 'calculated: ', calcvalue
            print 'reported: ', reportvalue
    return checkbool

def WtPercentCalculator(dict, conn):
    wtfracdict = {}
    wtpercentdict = {}
    wtfracsum = 0
    for key in dict:
        species = key
        conn.execute("select molar_mass from result_names where name = %s", (species,))
        molarmass = conn.fetchall()[0][0]
        wtfracdict[species] = (molarmass) * Decimal(dict[species])
        wtfracsum += wtfracdict[species]
    for key in dict:
        wtpercentdict[key] = (float(wtfracdict[key]) / float(wtfracsum)) * 100
    return wtpercentdict
db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor()    
    
    
conn.execute("SELECT result_name, value from sampledb.sample_results where unit_name = 'mol %' and result_name not in ('c6+', 'c7+', 'Benzene','Cyclohexane', '2-2-4 Trimethylpentane', 'Neohexane','Methylcyclopentane','Methylcyclohexane','m+p-xylenes','o-xylene','1,2,4-Trimethylbenzene', 'Ethylbenzene', 'Toluene') and sample_id = (SELECT DISTINCT sample_id from sampledb.samples where barcode = 1299)")

moldict = {}
results = conn.fetchall()

for result in results:
    moldict[result[0]] = result[1]
    
    
calcweight = WtPercentCalculator(moldict, conn)


conn.execute("SELECT result_name, value from sampledb.sample_results where unit_name = 'wt %' and result_name not in ('c6+', 'c7+', 'Benzene','Cyclohexane', '2-2-4 Trimethylpentane', 'Neohexane','Methylcyclopentane','Methylcyclohexane','m+p-xylenes','o-xylene','1,2,4-Trimethylbenzene', 'Ethylbenzene', 'Toluene') and sample_id = (SELECT DISTINCT sample_id from sampledb.samples where barcode = 1321)")

checks = conn.fetchall()
repweight = {}
print 'actual'
for check in checks:
    print 'result: ', check[0], 'value: ', check[1]
    repweight[check[0]] = check[1]
    
print 'calculated'
for key in calcweight:
    print 'result: ', key, 'value: ', calcweight[key]

    
    
print checkWtPercentConversion(repweight, calcweight)


db.close()