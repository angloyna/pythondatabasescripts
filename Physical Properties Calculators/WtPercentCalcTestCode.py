import MySQLdb
import weightPercentCalculator


#db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )


#conn = db.cursor()


#dict = {'n2':0, 'c1':2.861, 'c2':4.75, 'co2':.099, 'c3':13.719, 'ic4':11.529, 'nc4':15.174, 'ic5':10.396, 'nc5':11.285, 'c6+': 30.188}

#WtPercentCalculator(dict, conn)


def testWtPercentCalculator():
    resultmoldict = {}
    resultwtdict = {}
    wtpercentcalc = {}
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
    conn = db.cursor()
    
    conn.execute("SELECT result_name, value from sampledb.sample_results where starttime = '2015-03-11 07:33:00' and unit_name = 'mol %' and result_name <> 'Benzene' and result_name <> 'Ethylbenzene' and result_name <> 'Toluene' and result_name <> 'Xylenes' and result_name <> 'c6+' and result_name <> 'c7+'")
    rows = conn.fetchall()
    
    for row in rows:
        resultmoldict[row[0]] = row[1]
    
    conn.execute("SELECT result_name, value from sample_results where starttime = '2015-03-11 07:33:00' and unit_name = 'wt %' and result_name <> 'Benzene' and result_name <> 'Ethylbenzene' and result_name <> 'Toluene' and result_name <> 'Xylenes' and result_name <> 'c6+' and result_name <> 'c7+'")
    rows = conn.fetchall()
    
    for row in rows:
        resultwtdict[row[0]] = row[1]
        
    wtpercentcalc = weightPercentCalculator.WtPercentCalculator(resultmoldict, conn)
    
    print resultmoldict
    print resultwtdict
    print wtpercentcalc
    
    print weightPercentCalculator.checkWtPercentConversion(resultwtdict, wtpercentcalc)
    
    
testWtPercentCalculator()