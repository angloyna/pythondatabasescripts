
import matplotlib.pyplot as plt
import MySQLdb



def getXvaluesArray(min,max):
    xValues = []
    i = min
        
    while i <= max:
        xValues.append(i)
        i += .5        
    
    return xValues

def getSpectraPerSample(barcodes):
    spectraDict = {}
    
    for barcode in barcodes:
            
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        conn.execute("select sample_id, site, analysis_number, starttime, endtime from sampledb.sample_info where barcode = %s", (barcode))
        results = conn.fetchall()
        
        db.close()
        if results != ():
            sampleid = results[0][0]
            site = results[0][1]
            analysisNumber = results[0][2]
            starttime = results[0][3]
            endtime = results[0][4]
            db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
            conn = db.cursor()
            
            conn.execute("select localdb from sampledb.sites where sites.name = %s", (site))
            localdb = conn.fetchone()[0]
            
            db.close()
            
            db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", localdb)

            
            
            conn = db.cursor()
            conn.execute("select cdata from tcal where canalysis = %s and cdatestamp between %s and %s", (analysisNumber, starttime, endtime))
            
            results = conn.fetchall()
            
            db.close()
            
            yValues = []
            
            for result in results:
                yValueSet = result[0].split(",")
                yValueSet = [float(x) for x in yValueSet]
                yValues.append(yValueSet)
            spectraDict[str(barcode) + " " + str(site) + " " + str(analysisNumber) + " " + str(starttime)] = yValues    
    
    return spectraDict

def plotSpectraPerSample(barcodes):
    
    xValues = getXvaluesArray(1355,1795)

    
    spectraDict = getSpectraPerSample(barcodes)
    
    
    f, axarr = plt.subplots(len(spectraDict), sharex=True)
    subPlotindex = 0
    
    for key in spectraDict:
        print key, len(spectraDict[key])
        for yValue in spectraDict[key]:
            axarr[subPlotindex].plot(xValues, yValue)
        axarr[subPlotindex].set_title(key)
        subPlotindex += 1


db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
conn = db.cursor()

conn.execute("SELECT barcode from sampledb.sample_info where process_status = 'Sampled' and barcode is not null")

results = conn.fetchall()
barcodes = []
#barcodes = [2697,2698,2699,2700,2701,2702,1644,1643,1642,1641,2703,2672]
for result in results:
    barcodes.append(result[0])


db.close()

i = 0
while i < len(barcodes):
    plotSpectraPerSample(barcodes[i:i+4])
    i += 4
    
