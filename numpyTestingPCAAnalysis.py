import numpy as np
import mdp
import MySQLdb
import matplotlib.pyplot as plt


db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()

conn.execute("SELECT spectrum from sampledb.spectra where sample_id in (select distinct sample_id from sampledb.samples where phase = 'Liquid')")

results = conn.fetchall()


db.close()



hugearray = []

for result in results:
    resultarray = result[0].split(",")
    intresultarray = [float(x) for x in resultarray]
    hugearray.append(intresultarray)

hugearray = np.asarray(hugearray)




var = hugearray

#Create the PCA node and train it
pcan = mdp.nodes.PCANode(output_dim=3)
pcar = pcan.execute(var)

#Graph the results
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(pcar[:10,0], pcar[:10,1], 'bo')
ax.plot(pcar[10:,0], pcar[10:,1], 'ro')

#Show variance accounted for
ax.set_xlabel('PC1 (%.3f%%)' % (pcan.d[0]))
ax.set_ylabel('PC2 (%.3f%%)' % (pcan.d[1]))

plt.show()