import xlrd
import MySQLdb
from datetime import datetime


book = xlrd.open_workbook("sampedaterangebyanalysis_point.xlsx")
print "The number of worksheets is", book.nsheets
print "Worksheet name(s):", book.sheet_names()
sh = book.sheet_by_index(0)
print sh.name, sh.nrows, sh.ncols


for i in range(1,sh.nrows):
    sitename = str(sh.row(i)[3]).replace("text:u","").replace("'","").strip()
    dbname = str(sh.row(i)[6]).replace("text:u","").replace("'","").strip()
    
    print sitename, dbname
    
    samplespectra = {}
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
    conn = db.cursor()
    
    conn.execute("SELECT sample_id, starttime, endtime, analysis_points.analysis_number FROM samples JOIN analysis_points on samples.analysis_id = analysis_points.id JOIN sites on analysis_points.site_id = sites.id WHERE sites.name = %s and samples.sample_id not in (SELECT DISTINCT sample_id from spectra)", (sitename,))
    
    samplerows = conn.fetchall()
    
        
    db.close()
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", dbname )
    conn = db.cursor()
    
    
    for row in samplerows:
        sampleid = row[0]
        starttime = row[1]
        endtime = row[2]
        numanalysis = row[3]
        print sampleid, starttime, endtime, numanalysis
        
        conn.execute("SELECT ddatestamp, dtemp, dpres, dtran, ddata FROM tdata WHERE ddatestamp BETWEEN %s AND %s AND danalysis = %s", (starttime, endtime, numanalysis))
        #conn.execute("SELECT cdatestamp, ctemp, cpres, ctran, cdata from tcal where cdatestamp between %s and %s and canalysis = %s", (starttime, endtime, numanalysis))
        
        drows = conn.fetchall()
    
        if drows != ():
            samplespectra[sampleid] = drows
        
    db.close()
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
    conn = db.cursor()
    
    for key in samplespectra:
        sampleid = key
        for item in samplespectra[sampleid]:
            conn.execute("INSERT INTO spectra(sample_id, time, temp, pres, tran, spectrum) VALUES(%s, %s, %s, %s, %s, %s)", (sampleid, item[0], item[1], item[2], item[3], item[4]))


    db.commit()
    db.close()
