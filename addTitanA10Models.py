import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor()  


models = ['c6']

sitename = 'Titan'
analysisnumber = 10

conn.execute('select id from sampledb.sites where name = %s', (sitename))

siteid = conn.fetchone()[0]

conn.execute('select id from sampledb.analysis_points where site_id = %s and analysis_number = %s', (siteid, analysisnumber))

analysisid = conn.fetchone()[0]

for model in models:
    conn.execute('select id from sampledb.result_names where name = %s', (model))
    
    resultid = conn.fetchone()[0]
    
    if model != 'API Gravity':
        unitname = 'mol %'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
        
    if model == 'API Gravity':
        unitname = 'None'
        conn.execute('select id from sampledb.units where name = %s', (unitname))
        unitid = conn.fetchone()[0]
        
    conn.execute('insert into sampledb.results_modeled(analysis_id, result_id, unit_id) values (%s, %s, %s)', (analysisid, resultid, unitid))
    
db.commit()
db.close()