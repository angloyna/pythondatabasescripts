import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor() 


sitenamewithModelStats = 'Marshall'
analysisnumberwithModelStats = 6


sitenametoReceiveModelStats = 'Milton'
analysisnumbertoReceiveModelStats = 6


conn.execute('select id from sampledb.sites where name = %s', (sitenamewithModelStats))
firstsiteid = conn.fetchone()[0]

conn.execute('select id from sampledb.analysis_points where analysis_number = %s and site_id = %s', (analysisnumberwithModelStats, firstsiteid))
firstanalysisid = conn.fetchone()[0]


conn.execute('select id from sampledb.sites where name = %s', (sitenametoReceiveModelStats))
secondsiteid = conn.fetchone()[0]


conn.execute('select id from sampledb.analysis_points where analysis_number = %s and site_id = %s', (analysisnumbertoReceiveModelStats, secondsiteid))

secondanalysisPoint = conn.fetchone()[0]


conn.execute('select result_id, unit_id, r2, latent_var, rmsec, rmsecv, cal_bias, cv_bias from sampledb.results_modeled where analysis_id = %s', (firstanalysisid))


results = conn.fetchall()




for result in results:
    resultid = result[0]
    unitid = result[1]
    r2value = result[2]
    latent_var = result[3]
    rmsec = result[4]
    rmsecv = result[5]
    cal_bias = result[6]
    cv_bias = result[7]
    
    
    conn.execute('update sampledb.results_modeled set r2 = %s, latent_var = %s, rmsec = %s, rmsecv = %s, cal_bias = %s, cv_bias = %s where analysis_id = %s and result_id = %s and unit_id = %s', (r2value, latent_var, rmsec, rmsecv, cal_bias, cv_bias, secondanalysisPoint, resultid, unitid))
    
    
    
    
    

db.commit()

db.close()