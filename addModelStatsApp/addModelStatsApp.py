import MySQLdb
from Tkinter import *
import tkFileDialog


def enterStats(sitename, analysisnumber, t, conn, settingsframe, r2entry, latEntry, rmsecEntry, rmsecvEntry, modelName, unitname, db):
    r2Value = r2entry.get()
    latValue = latEntry.get()
    rmsecValue = rmsecEntry.get()
    rmsecvValue = rmsecvEntry.get()
    
    r2entry.delete(0,END)
    latEntry.delete(0,END)
    rmsecEntry.delete(0,END)
    rmsecvEntry.delete(0,END)
    
    conn.execute('select id from sampledb.result_names where name = %s', (modelName))
    
    resultid = conn.fetchone()[0]
    
    conn.execute('select analysis_points.id from sampledb.analysis_points join sampledb.sites on analysis_points.site_id = sites.id where sites.name = %s and analysis_points.analysis_number = %s', (sitename, analysisnumber))
    
    analysisid = conn.fetchone()[0]
    
    conn.execute('select id from sampledb.units where name = %s', (unitname))
    
    unitid = conn.fetchone()[0]
    
    conn.execute('select * from sampledb.results_modeled where analysis_id = %s and result_id = %s and unit_id = %s', (analysisid, resultid, unitid))
    
    if conn.fetchall() != ():
    
        conn.execute('update sampledb.results_modeled set r2 = %s, latent_var = %s, rmsec = %s, rmsecv = %s where analysis_id = %s and result_id = %s and unit_id = %s', (r2Value, latValue, rmsecValue, rmsecvValue, analysisid, resultid, unitid))
    
        db.commit()
        
    else:
        print 'could not add the following model values: ' + str(sitename) + ' ' + str(analysis_number) + ' ' + str(modelName) + ' ' + str(unitname)
    
def selectAnalysisPoint(sitename, analysisnumber, t, conn, settingsframe, analysispointbutton, optmenu, analysispointsframe, db):
    analysispointbutton.destroy()
    optmenu.destroy()
    
    analysispointlabel = Label(analysispointsframe, text = analysisnumber, justify = RIGHT, width = 13)
    analysispointlabel.pack(side = LEFT)    
    
    conn.execute('select result_name from sampledb.modeled_results where site = %s and analysis_number = %s', (sitename, analysisnumber))
                 
    resultnames = conn.fetchall()
    modelOptions = []
    
    for resultname in resultnames:
        modelOptions.append(resultname[0])    
    
    
    resultNameFrame = Frame(settingsframe, height=2, bd=1)
    resultNameFrame.pack(padx=5, pady=5, side = TOP)
    resultNamelabel = Label(resultNameFrame, text = 'Model Name: ', justify = RIGHT, width = 18)
    resultNamelabel.pack(side = LEFT)
    
    modelVar = StringVar(resultNameFrame)      
               
    modeloptions = apply(OptionMenu, (resultNameFrame, modelVar) + tuple(modelOptions))
    modelVar.set(modelOptions[0])
    modeloptions.pack(side = LEFT)
    
    
    conn.execute('select distinct unit_name from sampledb.modeled_results where site = %s and analysis_number = %s', (sitename, analysisnumber))
    
    unitnames = conn.fetchall()
    
    unitOptions = []
    
    for unit in unitnames:
        unitOptions.append(unit[0])
        
    unitNameFrame = Frame(settingsframe, height = 2, bd=1)
    unitNameFrame.pack(padx = 5, pady = 5, side = TOP)
    unitNamelabel = Label(unitNameFrame, text = 'Unit Name: ', justify = RIGHT, width = 18)
    unitNamelabel.pack(side = LEFT)
    
    unitVar = StringVar(unitNameFrame)
    
    unitoptions = apply(OptionMenu, (unitNameFrame, unitVar) + tuple(unitOptions))
    unitVar.set(unitOptions[0])
    unitoptions.pack(side = LEFT)
        
    r2Frame = Frame(settingsframe, height=2, bd=1, relief=SUNKEN)
    r2Frame.pack(fill=X, padx=5, pady=5)        
    r2label = Label(r2Frame, text = 'R2 value: ', justify = LEFT, width = 25)
    r2label.pack(side = LEFT)
    q = Entry(r2Frame, width = 15)
    q.pack(side = LEFT)
    
    latentFrame = Frame(settingsframe, height=2, bd=1, relief=SUNKEN)
    latentFrame.pack(fill=X, padx=5, pady=5)        
    latentlabel = Label(latentFrame, text = 'Latent Variables: ', justify = LEFT, width = 25)
    latentlabel.pack(side = LEFT)
    a = Entry(latentFrame, width = 15)
    a.pack(side = LEFT) 
    
    
    rmsecFrame = Frame(settingsframe, height=2, bd=1, relief=SUNKEN)
    rmsecFrame.pack(fill=X, padx=5, pady=5)        
    rmseclabel = Label(rmsecFrame, text = 'RMSEC: ', justify = LEFT, width = 25)
    rmseclabel.pack(side = LEFT)
    b = Entry(rmsecFrame, width = 15)
    b.pack(side = LEFT)
    
    rmsecVFrame = Frame(settingsframe, height=2, bd=1, relief=SUNKEN)
    rmsecVFrame.pack(fill=X, padx=5, pady=5)        
    rmsecVlabel = Label(rmsecVFrame, text = 'RMSECV: ', justify = LEFT, width = 25)
    rmsecVlabel.pack(side = LEFT)
    C = Entry(rmsecVFrame, width = 15)
    C.pack(side = LEFT)
    
    addModelStatsButton = Button(settingsframe, width = 18, text = 'Add Model Stats', command= lambda: enterStats(sitename, analysisnumber, t, conn, settingsframe, q, a, b, C, modelVar.get(), unitVar.get(), db))
    addModelStatsButton.pack(side = TOP)     


def selectSite(sitename, t, conn, settingsframe, sitebutton, optmenu, siteframe, db):
    sitebutton.destroy()
    optmenu.destroy()
    
    sitenamelabel = Label(siteframe, text = sitename, justify = RIGHT, width = 18)
    sitenamelabel.pack(side = LEFT)    
    
    settingsFrame = settingsframe
    conn.execute("select analysis_number from sampledb.analysis_points join sampledb.sites on analysis_points.site_id = sites.id where sites.name = %s", (sitename))
    
    analysispoints = conn.fetchall()
    analysisPointOptions = []
    
    for analysispoint in analysispoints:
        analysisPointOptions.append(analysispoint[0])
    
       
    analysisPointFrame = Frame(settingsFrame, height=2, bd=1)
    analysisPointFrame.pack(padx=5, pady=5, side = TOP)
    analysisPointlabel = Label(analysisPointFrame, text = 'Analysis Point: ', justify = RIGHT, width = 18)
    analysisPointlabel.pack(side = LEFT)
   
       
    analysisPointVar = StringVar(analysisPointFrame)      
           
    analysisPointoptions = apply(OptionMenu, (analysisPointFrame, analysisPointVar) + tuple(analysisPointOptions))
    analysisPointVar.set(analysisPointOptions[0])
    analysisPointoptions.pack(side = LEFT)
       
    analysisPointButton = Button(analysisPointFrame, width = 18, text = 'Select Analysis Point', command= lambda: selectAnalysisPoint(sitename, analysisPointVar.get(), t, conn, settingsFrame, analysisPointButton, analysisPointoptions, analysisPointFrame, db))
    analysisPointButton.pack(side = LEFT)    
    
    
    

def go(eddpath, zippath, phase, analysistype, containertype, analysislab, method, t):
    t.destroy()

        
        
def addModelStats():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Add Model Stats')
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()    
    labelframe = LabelFrame(top, text="Add Stats")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
      
    settingsFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    settingsFrame.pack(fill=X, padx=5, pady=5)
    siteFrame = Frame(settingsFrame, height=2, bd=1)
    siteFrame.pack(padx=5, pady=5, side = TOP)
    sitelabel = Label(siteFrame, text = 'Site: ', justify = RIGHT, width = 13)
    sitelabel.pack(side = LEFT)
    
    
    
    conn.execute("select name from sampledb.sites")
    siteOptions = []
    sitenames = conn.fetchall()
    
    for site in sitenames:
        siteOptions.append(site[0])
    
    siteVar = StringVar(siteFrame)      
        
    siteoptions = apply(OptionMenu, (siteFrame, siteVar) + tuple(siteOptions))
    siteVar.set(siteOptions[0])
    siteoptions.pack(side = LEFT)
    
    siteButton = Button(siteFrame, width = 10, text = 'Select Site', command= lambda: selectSite(siteVar.get(),top, conn, settingsFrame, siteButton, siteoptions, siteFrame, db))
    siteButton.pack(side = LEFT)
    
    
    top.mainloop()



addModelStats()