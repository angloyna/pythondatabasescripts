import MySQLdb
from openpyxl.styles.borders import Border, Side
import openpyxl
from openpyxl.drawing.image import Image
from ExcelCellFormatter import *
from openpyxl.styles import PatternFill, Font


class JP3Header:
    def __init__(self,ws):
        self.ws = ws
        self.cellFormatter = ExcelCellFormatter(self.ws)

    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        self.cellFormatter.setCellColor(1, 1, 2, 4, WhiteFill)
    
    def fillBlackCells(self):
        BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
        self.cellFormatter.setCellColor(3, 1, 3, 4, BlackFill)
        
    def fillGreyCells(self):
        GreyFill = PatternFill(start_color='999999', end_color='999999',fill_type='solid')
        self.cellFormatter.setCellColor(4, 1, 4, 4, GreyFill)
        
    def fillRedCells(self):
        RedFill = PatternFill(start_color='FF0000', end_color='FF0000',fill_type='solid')
        self.cellFormatter.setCellColor(5, 1, 6, 4, RedFill)
    
    def fillOrangeCells(self):
        OrangeFill = PatternFill(start_color='FFCC00', end_color='FFCC00',fill_type='solid')
        self.cellFormatter.setCellColor(7, 1, 9, 4, OrangeFill)
    
    def formatHeader(self):
        self.fillWhiteCells()
        self.fillBlackCells()
        self.fillGreyCells()
        self.fillRedCells()
        self.fillOrangeCells()
        self.cellFormatter.setRowHeight([1,2,3,4],17)
    
    def addHeaderDetails(self):
        underline_border = Border(bottom=Side(style='thin'))
        img = openpyxl.drawing.image.Image(r"C:\Users\agloyna\documents\pythondatabasescripts\Customer-Facing Validation Report Generator\jp3.png", size=(250,80))
        
        img.anchor(self.ws.cell('A1'))
        self.ws.add_image(img, 'A1')    
        
        self.ws.cell(row = 1, column = 7).value = "JP3 Measurement"
        self.ws.merge_cells('G1:H1')
        
        self.ws.cell(row = 2, column = 7).value = "4109 Todd Lane, Suite 200"
        self.ws.merge_cells('G2:I2')
        
        self.ws.cell(row = 3, column = 7).value = "Austin TX, 78744"
        self.ws.merge_cells('G3:H3')
        
        self.ws.cell(row = 4, column = 7).value = "512-537-8450"
        self.ws.merge_cells('G4:H4')
        
        self.cellFormatter.applyBordertoBlockOfCells(1, 4, 9, 4, underline_border)
        
        
   
    
    
