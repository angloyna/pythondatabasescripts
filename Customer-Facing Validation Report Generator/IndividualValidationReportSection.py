from ExcelCellFormatter import *
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import PatternFill, Font
from openpyxl.styles import Alignment
import MySQLdb


class IndividualValidationReportSection:
    
    def __init__(self, ws, reportid, sampleid, reportStartRow, sampleNumber):
        self.ws = ws
        self.reportid = reportid
        self.sampleid = sampleid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        self.reportStartRow = reportStartRow
        self.sampleNumber = sampleNumber
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()

        conn.execute('SELECT count(result_id) from sampledb.validation_results where sample_id = ' + str(self.sampleid) + ' and report_id = ' + str(self.reportid) + ' GROUP BY report_id, sample_id')
        
        self.numResults = conn.fetchone()[0]
        
        db.close()
        
        self.reportEndRow = self.reportStartRow + self.numResults + 3
        
        
    def getNumResults(self):
        return self.numResults
    
    def getSiteNameAndAnalysisNumber(self):
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        conn.execute("SELECT sites.name, analysis_points.analysis_number from sampledb.validation_reports join sampledb.analysis_points on validation_reports.analysis_id = analysis_points.id join sampledb.sites on analysis_points.site_id = sites.id where validation_reports.id = " + str(self.reportid))
        
        results = conn.fetchone()
        
        db.close()
        
        return results[0], results[1]
    
    def fillOrangeCells(self):
        OrangeFill = PatternFill(start_color='FFCC00', end_color='FFCC00',fill_type='solid')
        self.cellFormatter.setCellColor(1, self.reportStartRow + 3, 8, self.reportStartRow + 3, OrangeFill)
        
    def fillBlackCells(self):
        BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
        self.cellFormatter.setCellColor(1, self.reportStartRow, 9, self.reportStartRow, BlackFill)
        
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        self.cellFormatter.setCellColor(1, self.reportStartRow + 1, 9, self.reportStartRow + 2, WhiteFill)
        self.cellFormatter.setCellColor(1, self.reportStartRow + 4, 9, self.reportStartRow + 4 + self.numResults + 2, WhiteFill)
            
    def addOtherRows(self):
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
        self.cellFormatter.applyBordertoBlockOfCells(1, self.reportStartRow + self.numResults - 1, 8, self.reportStartRow + self.numResults + 2, full_box_border)
        
                
    def retrieveValidationReportValues(self):
        resultDict = {}
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        
        conn = db.cursor()
        
        conn.execute("SELECT result_names.name, units.name, TRUNCATE(validation_results.value,3), TRUNCATE(results.value,3) from sampledb.validation_results join sampledb.result_names on validation_results.result_id = result_names.id join sampledb.units on validation_results.unit_id = units.id join sampledb.results on validation_results.sample_id = results.sample_id and validation_results.result_id = results.result_id and validation_results.unit_id = results.unit_id where validation_results.report_id = " + str(self.reportid) + " and validation_results.sample_id = " + str(self.sampleid))
        
        results = conn.fetchall()
        db.close()
        
        for result in results:
            resultDict[result[0]] = (result[1], result[2], result[3])
        
        return resultDict
        
        
    def populateIndividualReportResults(self):
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
        
        curRow = self.reportStartRow + 4
        resultNameColumn = 1
        unitNameColumn = 5
        veraxColumn = 2
        ThirdPartyLabColumn = 3
        AbsoluteDeltaColumn = 4
        
        resultDict = self.retrieveValidationReportValues()
        
        orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'c12+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor Per Gallon', 'GPM Total C2+', 'VPCR4', 'RVP', 'TVP_N2CO2_JP3', 'Dry Vapor Pressure (D5191)']
            
        for result in orderOfResults:
            if resultDict.has_key(result):
                unitName = resultDict[result][0]
                veraxValue = resultDict[result][1]
                labValue = resultDict[result][2]
                if veraxValue >= 0:
                    absoluteDelta = abs(veraxValue - labValue)
                elif veraxValue < 0:
                    veraxValue = 0
                    absoluteDelta = abs(veraxValue - labValue)
                self.ws.cell(row = curRow, column = resultNameColumn).value = result
                self.ws.cell(row = curRow, column = resultNameColumn).alignment = Alignment(horizontal='right')
                self.ws.cell(row = curRow, column = veraxColumn).value = veraxValue
                self.ws.cell(row = curRow, column = veraxColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = ThirdPartyLabColumn).value = labValue
                self.ws.cell(row = curRow, column = ThirdPartyLabColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = AbsoluteDeltaColumn).value = absoluteDelta
                self.ws.cell(row = curRow, column = AbsoluteDeltaColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = unitNameColumn).value = '[' + str(unitName) + ']'
                self.ws.cell(row = curRow, column = unitNameColumn).alignment = Alignment(horizontal='center')
                             
                curRow += 1
        
        self.cellFormatter.applyBordertoBlockOfCells(1, self.reportStartRow + 4, 8, self.reportStartRow + 6 + len(resultDict), full_box_border)
                
                
        self.fillWhiteCells()
        self.fillOrangeCells()
        self.fillBlackCells() 
            
    def createIndividualReportHeader(self):
        ReportTitleFont = HeaderFont = Font(name='Calibri', size=16, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FFFFFF')
        
        HeaderFont = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
        
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
              
        sitename, analysisnumber = self.getSiteNameAndAnalysisNumber()
                
        self.ws.cell(row = self.reportStartRow, column = 1).value = str(sitename) + ' A' + str(analysisnumber) + " Sample " + str(self.sampleNumber)
        self.ws.cell(row = self.reportStartRow, column = 1).font = ReportTitleFont
        self.ws.merge_cells('A' + str(self.reportStartRow) + ':D' + str(self.reportStartRow))
        
        self.ws.cell(row = self.reportStartRow + 1, column = 1).value = "Species or"
        self.ws.cell(row = self.reportStartRow + 1, column = 1).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 1, column = 1).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 2, column = 1).value = "Parameter"
        self.ws.cell(row = self.reportStartRow + 2, column = 1).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 2, column = 1).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 1, column = 2).value = "Concentration"
        self.ws.cell(row = self.reportStartRow + 1, column = 2).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 1, column = 2).alignment = Alignment(horizontal="center")
        self.ws.merge_cells('B' + str(self.reportStartRow + 1) + ':D' + str(self.reportStartRow + 1))
        
        self.ws.cell(row = self.reportStartRow + 2, column = 2).value = "Verax"
        self.ws.cell(row = self.reportStartRow + 2, column = 2).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 2, column = 2).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 2, column = 3).value = "3rd Party Lab"
        self.ws.cell(row = self.reportStartRow + 2, column = 3).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 2, column = 3).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 2, column = 4).value = "Abs. Delta"
        self.ws.cell(row = self.reportStartRow + 2, column = 4).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 2, column = 4).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 1, column = 5).value = "Units"
        self.ws.cell(row = self.reportStartRow + 1, column = 5).font = HeaderFont
        self.ws.cell(row = self.reportStartRow + 1, column = 5).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = self.reportStartRow + 1, column = 6).value = "Additional Comments"
        self.ws.cell(row = self.reportStartRow + 1, column = 6).font = HeaderFont
        self.ws.merge_cells('F' + str(self.reportStartRow + 1) + ':H' + str(self.reportStartRow + 1))
        
        
        self.cellFormatter.applyBordertoBlockOfCells(2, self.reportStartRow + 1, 4, self.reportStartRow + 2, full_box_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, self.reportStartRow + 3, 8, self.reportStartRow + 3, full_box_border)
        
    def createSection(self):
        self.createIndividualReportHeader()
        self.populateIndividualReportResults()