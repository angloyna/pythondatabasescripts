from ExcelCellFormatter import *
from openpyxl.styles import PatternFill, Font
from openpyxl.styles.borders import Border, Side
import MySQLdb


class AnalysisDesiredHeader:
    
    def __init__(self, ws, reportid):
        self.ws = ws
        self.reportid = reportid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        
        
    def getAnalysisPointInfo(self):
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        
        conn = db.cursor()
        
        conn.execute("SELECT sites.name, analysis_points.analysis_number, validation_reports.phase from sampledb.validation_reports join sampledb.analysis_points on validation_reports.analysis_id = analysis_points.id join sampledb.sites on analysis_points.site_id = sites.id where validation_reports.id = " + str(self.reportid))
        
        results = conn.fetchone()
        
        db.close()
        
        return results[0], results[1], results[2]
        
    def fillOrangeCells(self):
        OrangeFill = PatternFill(start_color='FFCC00', end_color='FFCC00',fill_type='solid')
        self.cellFormatter.setCellColor(1, 13, 6, 13, OrangeFill)
        self.cellFormatter.setCellColor(1, 15, 6, 15, OrangeFill)
        self.cellFormatter.setCellColor(1, 17, 6, 17, OrangeFill)
        
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        self.cellFormatter.setCellColor(1, 14, 6, 14, WhiteFill)
        self.cellFormatter.setCellColor(1, 16, 6, 16, WhiteFill)
        self.cellFormatter.setCellColor(1, 18, 6, 18, WhiteFill)
        self.cellFormatter.setCellColor(7, 13, 9, 18, WhiteFill)
                            
    def fillBlackCells(self):
        BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
        self.cellFormatter.setCellColor(1, 12, 9, 12, BlackFill)       
        
    def formatAnalysisDesiredSection(self):
        self.fillWhiteCells()
        self.fillOrangeCells()
        self.fillBlackCells()
        self.cellFormatter.setRowHeight([12],21)
        self.cellFormatter.setRowHeight([13], 18)
        self.cellFormatter.setRowHeight([14,15,16,17,18], 14.4)
        
        
    def addAnalysisDesiredHeaders(self):
        HeaderFont = Font(name='Calibri', size=16, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FFFFFF')
                      
        AnalysisDetailsFont = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
        
        underline_border = Border(bottom=Side(style='thin'))
        
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
        
        top_border = Border(top=Side(style='thin'))
        
        self.formatAnalysisDesiredSection()
        
        sitename, analysisNumber, phase = self.getAnalysisPointInfo()
        
        self.ws.cell(row = 12, column = 1).value = str(sitename) + " A" + str(analysisNumber) + " (Samples Abs. Average)"
        self.ws.cell(row = 12, column = 1).font = HeaderFont
        self.ws.merge_cells('A12:F12')
        
        self.ws.cell(row = 13, column = 1).value = "Phase (gas or liquid)"
        self.ws.cell(row = 13, column = 1).font = AnalysisDetailsFont
        self.ws.merge_cells('A13:B13')
        
        self.ws.cell(row = 13, column = 3).value = "-->"
        self.ws.cell(row = 13, column = 3).font = AnalysisDetailsFont
        
        self.ws.cell(row = 13, column = 4).value = str(phase)
        self.ws.cell(row = 13, column = 4).font = AnalysisDetailsFont        
        
        self.ws.cell(row = 15, column = 1).value = "Analysis Desired"
        self.ws.cell(row = 15, column = 1).font = AnalysisDetailsFont
        self.ws.merge_cells("A15:B15")
        
        self.ws.merge_cells("D13:F13")
        self.ws.merge_cells("D15:F15")
        self.ws.merge_cells("D16:F16")
        self.ws.merge_cells("D17:F17")
        
        
        self.cellFormatter.applyBordertoBlockOfCells(3, 13, 6, 13, full_box_border)
        self.cellFormatter.applyBordertoBlockOfCells(3, 15, 6, 17, full_box_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, 18, 3, 18, top_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, 14, 3, 14, underline_border)
