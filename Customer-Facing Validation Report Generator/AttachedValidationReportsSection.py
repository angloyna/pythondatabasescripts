import MySQLdb
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import PatternFill, Font
from ExcelCellFormatter import *

class AttachedValidationReportsSection:
    
    def __init__(self, ws, reportid, averageReportRowEnd):
        self.ws = ws
        self.reportid = reportid
        self.averageReportRowEnd = averageReportRowEnd
        self.cellFormatter = ExcelCellFormatter(self.ws)
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        conn.execute("SELECT num_samples FROM sampledb.validation_reports where validation_reports.id = " + str(self.reportid))
        
        self.numSamples = conn.fetchone()[0]
        
        db.close()
        
        
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')      
        self.cellFormatter.setCellColor(1, self.averageReportRowEnd + 2 , 9, self.averageReportRowEnd + 4 + self.numSamples, WhiteFill)
        
    def formatSection(self):
        top_border_left_border = Border(top=Side(style='thin'), left=Side(style='thin'))
        underline_border = Border(bottom=Side(style='thin'))
        right_border = Border(right=Side(style='thin'))
        left_border = Border(left=Side(style='thin'))
        top_border = Border(top=Side(style='thin'))
        
        
        self.fillWhiteCells()
        self.cellFormatter.applyBordertoBlockOfCells(1,self.averageReportRowEnd + 2,1,self.averageReportRowEnd + 2, top_border_left_border)
        self.cellFormatter.applyBordertoBlockOfCells(2, self.averageReportRowEnd + 1, 8, self.averageReportRowEnd + 1, underline_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, self.averageReportRowEnd + 3, 1, self.averageReportRowEnd + 4 + self.numSamples, left_border)
        self.cellFormatter.applyBordertoBlockOfCells(9, self.averageReportRowEnd + 2, 9, self.averageReportRowEnd + 3 + self.numSamples, left_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, self.averageReportRowEnd + 4 + self.numSamples, 8, self.averageReportRowEnd + 4 + self.numSamples, top_border)
        
    def populateSection(self):
        
        HeaderFont = Font(name='Calibri', size=11, bold=True,italic=True,vertAlign=None,underline='single',strike=False, color='FF0000')
        
        self.ws.cell(row = self.averageReportRowEnd + 2, column = 1).value = "Independent Validation Reports attached behind this document."
        self.ws.cell(row = self.averageReportRowEnd + 2, column = 1).font = HeaderFont
        self.ws.merge_cells('A'+str(self.averageReportRowEnd + 2)+':E'+str(self.averageReportRowEnd + 2))
        
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        conn.execute("SELECT samples.starttime from sampledb.samples where sample_id in (SELECT DISTINCT sample_id from sampledb.validation_results where report_id = " +str(self.reportid) + ") order by samples.starttime")
        
        results = conn.fetchall()
        
        db.close()
        
        for i in range(0, len(results)):
            self.ws.cell(row = self.averageReportRowEnd + 3 + i, column = 1).value = "Sample " + str(i + 1)
            self.ws.cell(row = self.averageReportRowEnd + 3 + i, column = 2).value = str(results[i][0])
            self.ws.merge_cells('B'+str(self.averageReportRowEnd + 3 + i)+':C'+str(self.averageReportRowEnd + 3 + i))
        
        
    def createSection(self):
        self.formatSection()
        self.populateSection()