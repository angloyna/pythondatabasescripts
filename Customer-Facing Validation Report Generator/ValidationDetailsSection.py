from ExcelCellFormatter import *
from openpyxl.styles import PatternFill, Font
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
import MySQLdb



class ValidationDetailsSection:
    
    def __init__(self, ws, reportid):
        self.ws = ws
        self.reportid = reportid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        
    def fillBlackCells(self):
        BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
        self.cellFormatter.setCellColor(1, 12, 9, 12, BlackFill)        
        
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        self.cellFormatter.setCellColor(1, 13, 9, 18, WhiteFill)
                
    def formatValidationDetails(self):
        self.fillBlackCells()
        self.fillWhiteCells()
        self.cellFormatter.setRowHeight([14,15,16],18)
        self.cellFormatter.setRowHeight([13,17,18], 14.4)
        self.cellFormatter.setRowHeight([12], 28.8)
        
    def addValidationDetailsSection(self):
        
        ValidationDetailsHeaderFont = Font(name='Calibri', size=22, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FFFFFF')
            
        validationDetailsFont = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
        
        underline_border = Border(bottom=Side(style='thin'))
        
        self.formatValidationDetails()
        
        self.ws.cell(row = 12, column = 1).value = "Validation Details: "
        self.ws.cell(row = 12, column = 1).font = ValidationDetailsHeaderFont
        self.ws.merge_cells('A12:C12')
        
        self.ws.cell(row = 14, column = 1).value = "Number of Samples"
        self.ws.cell(row = 14, column = 1).font = validationDetailsFont
        self.ws.merge_cells('A14:B14')
        
        self.ws.cell(row = 15, column = 1).value = "Sample Range"
        self.ws.cell(row = 15, column = 1).font = validationDetailsFont
        
        self.ws.cell(row = 16, column = 1).value = "Analyzer Name"
        self.ws.cell(row = 16, column = 1).font = validationDetailsFont
        
        self.ws.merge_cells('D14:G14')
        self.ws.merge_cells('D15:G15')
        self.ws.merge_cells('D16:G16')
        self.cellFormatter.applyBordertoBlockOfCells(4, 14, 7, 16, underline_border)
        
        
    def populateValidationDetailsSection(self):
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
            
        conn = db.cursor()    
        
        conn.execute("SELECT num_samples from sampledb.validation_reports where id = " + str(self.reportid))
        
        numsamples = conn.fetchone()[0]
        
        self.ws.cell(row = 14, column = 4).value = str(numsamples)
        self.ws.cell(row = 14, column = 4).alignment = Alignment(horizontal="center")
        
        conn.execute("SELECT DATE(MIN(starttime)), DATE(MAX(starttime)) from sampledb.samples where sample_id in (SELECT DISTINCT sample_id FROM sampledb.validation_results where report_id = " + str(self.reportid) + ")")
        
        results = conn.fetchone()
        
        mindate, maxdate = results[0], results[1]
        
        self.ws.cell(row = 15, column = 4).value = str(mindate) + ' - ' + str(maxdate)
        self.ws.cell(row = 15, column = 4).alignment = Alignment(horizontal="center")
        
        conn.execute("SELECT analysis_points.description from sampledb.validation_reports join sampledb.analysis_points on validation_reports.analysis_id = analysis_points.id join sampledb.sites on analysis_points.site_id = sites.id where validation_reports.id = " + str(self.reportid))
        
        results = conn.fetchone()
        
        db.close()
        
        description = results[0]
        
        self.ws.cell(row = 16, column = 4).value = str(description)
        self.ws.cell(row = 16, column = 4).alignment = Alignment(horizontal="center")    