from ExcelCellFormatter import *
from JP3Header import *
from GeneralInformationSection import *
from AnalysisDesiredHeader import *
from AverageReportSection import *
from AttachedValidationReportsSection import *

class AverageValidationReportSheet:
    
    def __init__(self, wb, reportid):
        self.wb = wb
        self.ws = self.wb.create_sheet(title="Summary Val.")
        self.reportid = reportid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        
        
    def formatColumnSize(self):
        self.cellFormatter.setColumnWidth(['B', 'D'],11.33)
        self.cellFormatter.setColumnWidth(['C'], 14.67)
        self.cellFormatter.setColumnWidth(['A'],17.67)
        self.cellFormatter.setColumnWidth(['E', 'F', 'H', 'I'], 10.89)
        self.cellFormatter.setColumnWidth(['G'],7.56)
        
        
    def createSheet(self):
        
        for i in range(1,52):
            for k in range(1,10):
                self.ws.cell(row = i, column = k).value = " "
                
         
        header = JP3Header(self.ws)
        header.formatHeader()
        header.addHeaderDetails()
        
        generalInformation = GeneralInformationSection(self.ws, self.reportid)
        generalInformation.addGeneralInformationHeaders()
        generalInformation.populateGeneralInformation()
        self.ws["F7"] = "='Analzyer Description'!F7:I7"
        self.ws["F8"] = "='Analzyer Description'!F8:F8"
        self.ws["F9"] = "='Analzyer Description'!F9:F9"
        self.ws['B8'] = "='Analzyer Description'!B8:B8"
        
        analysisDesiredHeader = AnalysisDesiredHeader(self.ws, self.reportid)
        analysisDesiredHeader.addAnalysisDesiredHeaders()
        
        averageReportSection = AverageReportSection(self.ws, self.reportid)
        averageReportSection.createAverageReportHeader()
        averageReportSection.populateAverageResults()
        
        attachedValidationReportSection = AttachedValidationReportsSection(self.ws, self.reportid, averageReportSection.getLastRow())
        attachedValidationReportSection.createSection()
        self.formatColumnSize()
        self.ws.column_dimensions['E'].width = 13.33
        self.ws.sheet_properties.pageSetUpPr.fitToPage = True
        self.ws.page_setup.fitToHeight = True        