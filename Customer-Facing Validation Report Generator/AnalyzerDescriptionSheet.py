import openpyxl
from openpyxl.styles import PatternFill, Font
from JP3Header import *
from ValidationDetailsSection import *
from GeneralInformationSection import *
from SummarySection import *
from ExcelCellFormatter import *
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
import MySQLdb


class AnalyzerDescriptionSheet:
    
    def __init__(self, wb, reportid):
        self.wb = wb
        self.ws = self.wb.create_sheet(title="Analzyer Description")
        self.reportid = reportid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        
        
    def formatColumnSize(self):
        self.cellFormatter.setColumnWidth(['C','D','F','H','I'],10.89)
        self.cellFormatter.setColumnWidth(['A'],16.22)
        self.cellFormatter.setColumnWidth(['B','E'], 13.56)
        self.cellFormatter.setColumnWidth(['G'],7.56)        

    def createSheet(self):
        
        for i in range(1,52):
            for k in range(1,10):
                self.ws.cell(row = i, column = k).value = " "
                    
        header = JP3Header(self.ws)
        header.formatHeader()
        header.addHeaderDetails()
        
        generalInformation = GeneralInformationSection(self.ws, self.reportid)
        generalInformation.addGeneralInformationHeaders()
        generalInformation.populateGeneralInformation()
        
        validationDetails = ValidationDetailsSection(self.ws, self.reportid)
        validationDetails.addValidationDetailsSection()
        validationDetails.populateValidationDetailsSection()
    
        summarySection = SummarySection(self.ws)
        summarySection.addSummarySection()
    
        self.formatColumnSize()
        self.ws.sheet_properties.pageSetUpPr.fitToPage = True
        self.ws.page_setup.fitToHeight = True        