from ExcelCellFormatter import *
from JP3Header import *
from GeneralInformationSection import *
from IndividualValidationReportSection import *

class IndividualValidationReportSheet:
    
    def __init__(self, wb, reportid):
        self.wb = wb
        self.ws = wb.create_sheet(title="Independent Val.")
        self.reportid = reportid
        self.cellFormatter = ExcelCellFormatter(self.ws)
        self.individualReportStartRow = 12
        
        
    def formatColumnSize(self):
        self.cellFormatter.setColumnWidth(['B', 'D'],11.33)
        self.cellFormatter.setColumnWidth(['C'], 14.67)
        self.cellFormatter.setColumnWidth(['A'],17.67)
        self.cellFormatter.setColumnWidth(['E', 'F', 'H', 'I'], 10.89)
        self.cellFormatter.setColumnWidth(['G'],7.56)
        
    
    def populateSheet(self):
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        
        conn = db.cursor()
        
        conn.execute("SELECT DISTINCT validation_results.sample_id from sampledb.validation_results join sampledb.samples on validation_results.sample_id = samples.sample_id where report_id = " + str(self.reportid) + " order by samples.starttime")
        
        samplesInReport = conn.fetchall()
        db.close()
        
        distanceFromIndividualReportStartRow = 0
        
        for i in range(0, len(samplesInReport)):
            temp = IndividualValidationReportSection(self.ws, self.reportid, samplesInReport[i][0], self.individualReportStartRow + distanceFromIndividualReportStartRow, (i + 1))
            temp.createSection()
            distanceFromIndividualReportStartRow += temp.getNumResults() + 7
                    
        
        self.ws.sheet_properties.pageSetUpPr.fitToPage = True
        self.ws.page_setup.fitToHeight = False
        self.ws.fit_height_to_pages = int(self.individualReportStartRow + distanceFromIndividualReportStartRow / 50) + 1

    def createSheet(self):
        for i in range(1,52):
            for k in range(1,10):
                self.ws.cell(row = i, column = k).value = " "
                        
        self.formatColumnSize()
        
        header = JP3Header(self.ws)
        header.formatHeader()
        header.addHeaderDetails()
        
        generalInformation = GeneralInformationSection(self.ws, self.reportid)
        generalInformation.addGeneralInformationHeaders()
        generalInformation.populateGeneralInformation()
        self.ws["F7"] = "='Analzyer Description'!F7:I7"
        self.ws["F8"] = "='Analzyer Description'!F8:F8"
        self.ws["F9"] = "='Analzyer Description'!F9:F9"
        self.ws['B8'] = "='Analzyer Description'!B8:B8"
        self.ws.column_dimensions['E'].width = 13.33
        self.populateSheet()