from ExcelCellFormatter import *
from openpyxl.styles import PatternFill, Font
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment

class SummarySection:
    
    def __init__(self,ws):
        self.ws = ws
        self.cellFormatter = ExcelCellFormatter(self.ws)
    
    def fillBlackCells(self):
        BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
        self.cellFormatter.setCellColor(1, 19, 9, 19, BlackFill)
                
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        self.cellFormatter.setCellColor(1, 20, 9, 51, WhiteFill)    
        
    def formatSummarySection(self):
        self.fillBlackCells()
        self.fillWhiteCells()
        self.cellFormatter.setRowHeight([19], 28.8)
        self.cellFormatter.setRowHeight([20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,46,47,48,49,50,51], 14.4)
        self.cellFormatter.setRowHeight([45],15)
        
    def addSummarySection(self):
        SummaryChartsHeaderFont = Font(name='Calibri', size=22, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FFFFFF')
        
        ExecutiveSummaryHeaderFont = Font(name='Calibri', size=11, bold=True,italic=True,vertAlign=None,underline='single',strike=False, color='FF0000')
        
        AuthorizationSignOffFont = Font(name='Calibri', size=11, bold=False,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
        
        underline_border = Border(bottom=Side(style='thin'))
        thick_underline_border = Border(bottom=Side(style='thick'))
        thick_top_border = Border(top=Side(style='thick'))
        left_border = Border(left=Side(style='thick'))
        right_border = Border(right=Side(style='thick'))
        
        self.formatSummarySection()
    
        self.ws.cell(row = 19, column = 1).value = "Summary Charts"
        self.ws.cell(row = 19, column = 1).font = SummaryChartsHeaderFont
        self.ws.merge_cells('A19:B19')
        
        self.ws.cell(row = 36, column = 1).value = "Executive Summary"
        self.ws.cell(row = 36, column = 1).font = ExecutiveSummaryHeaderFont
        self.ws.merge_cells('A36:B36')
        
        self.ws.cell(row = 48, column = 1).value = "Request Authorization By: "
        self.ws.cell(row = 48, column = 1).font = AuthorizationSignOffFont
        self.ws.cell(row = 48, column = 1).alignment = Alignment(horizontal='right')
        self.ws.merge_cells('A48:B48')
        
        self.ws.cell(row = 48, column = 6).value = "Date:"
        self.ws.cell(row = 48, column = 6).font = AuthorizationSignOffFont
        self.ws.cell(row = 48, column = 6).alignment = Alignment(horizontal='right')
        
        self.ws.cell(row = 50, column = 1).value = "Approved By: "
        self.ws.cell(row = 50, column = 1).font = AuthorizationSignOffFont    
        self.ws.cell(row = 50, column = 1).alignment = Alignment(horizontal='right')
        self.ws.merge_cells('A50:B50')   
        
        self.ws.cell(row = 50, column = 6).value = "Date:"
        self.ws.cell(row = 50, column = 6).font = AuthorizationSignOffFont
        self.ws.cell(row = 50, column = 6).alignment = Alignment(horizontal='right')    
        
        self.cellFormatter.applyBordertoBlockOfCells(1, 35, 9, 35, thick_underline_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, 46, 9, 46, thick_top_border)
        self.cellFormatter.applyBordertoBlockOfCells(1, 36, 1, 45, left_border)
        self.cellFormatter.applyBordertoBlockOfCells(9, 36, 9, 45, right_border)
        self.cellFormatter.applyBordertoBlockOfCells(3, 48, 5, 48, underline_border)
        self.cellFormatter.applyBordertoBlockOfCells(3, 50, 5, 50, underline_border)
        self.cellFormatter.applyBordertoBlockOfCells(7, 48, 9, 48, underline_border)
        self.cellFormatter.applyBordertoBlockOfCells(7, 50, 9, 50, underline_border)    