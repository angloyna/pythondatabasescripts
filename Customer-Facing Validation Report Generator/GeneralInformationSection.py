from openpyxl.styles import PatternFill, Font
from openpyxl.styles.borders import Border, Side
import MySQLdb
from ExcelCellFormatter import *
from openpyxl.styles import Alignment

class GeneralInformationSection:
       
       def __init__(self, ws, reportid):
              self.ws = ws
              self.reportid = reportid
              self.cellFormatter = ExcelCellFormatter(self.ws)
              
              
       def fillWhiteCells(self):
              WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
              self.cellFormatter.setCellColor(1, 5, 9, 5, WhiteFill)
              self.cellFormatter.setCellColor(1, 7, 9, 11, WhiteFill)
                            
       def fillBlackCells(self):
              BlackFill = PatternFill(start_color='000000', end_color='000000',fill_type='solid')
              self.cellFormatter.setCellColor(1, 6, 9, 6, BlackFill)       
              
       def formatGeneralInformation(self):
              self.fillWhiteCells()
              self.fillBlackCells()
              self.cellFormatter.setRowHeight([6],18)
              self.cellFormatter.setRowHeight([7,8,9,10,11], 14.4)
              self.cellFormatter.setRowHeight([5], 28.8)

       def addGeneralInformationHeaders(self):
              generalInformationFont = Font(name='Calibri', size=14, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FFFFFF')
              
              companyInformationFont = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
              
              descriptionFont = Font(name = 'Calibri', size = 22, bold = True, italic=False, vertAlign=None, underline='none', strike=False, color='000000')
              
              underline_border = Border(bottom=Side(style='thin'))
              
              self.formatGeneralInformation()
              
              self.ws.cell(row = 5, column = 1).font = descriptionFont
              self.ws.cell(row = 6, column = 1).value = "General Information"
              self.ws.cell(row = 6, column = 1).font = generalInformationFont
              self.ws.merge_cells('A6:B6')
              
              self.ws.cell(row = 7, column = 1).value = "Company:"
              self.ws.cell(row = 7, column = 1).font = companyInformationFont    
              self.ws.cell(row = 8, column = 1).value = "Validation Rev: "
              self.ws.cell(row = 8, column = 1).font = companyInformationFont
              self.ws.cell(row = 9, column = 1).value = "Date: "
              self.ws.cell(row = 9, column = 1).font = companyInformationFont
              self.ws.cell(row = 10, column = 1).value = "Project Name: "
              self.ws.cell(row = 10, column = 1).font = companyInformationFont
              self.ws.cell(row = 7, column = 5).value = "Contact Name: "
              self.ws.cell(row = 7, column = 5).font = companyInformationFont
              self.ws.cell(row = 7, column = 6).alignment = Alignment(horizontal='left')
              self.ws.cell(row = 8, column = 5).value = "Contact #: "
              self.ws.cell(row = 8, column = 5).font = companyInformationFont
              self.ws.cell(row = 8, column = 6).alignment = Alignment(horizontal='left')
              self.ws.cell(row = 9, column = 5).value = "Email: "
              self.ws.cell(row = 9, column = 5).font = companyInformationFont
              self.ws.cell(row = 9, column = 6).alignment = Alignment(horizontal='left')
              
              self.ws.merge_cells('B7:C7')
              self.ws.merge_cells('B10:C10')
              self.ws.merge_cells('B9:C9')
              self.ws.merge_cells('F7:I7')
              self.ws.merge_cells('F8:I8')
              self.ws.merge_cells('F9:I9')
              self.cellFormatter.applyBordertoBlockOfCells(2, 7, 3, 10, underline_border)
              self.cellFormatter.applyBordertoBlockOfCells(6, 7, 9, 9, underline_border)
       
       
       def populateGeneralInformation(self):
              db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
              
              conn = db.cursor()
              
              conn.execute("SELECT companies.name, sites.name, CURDATE(), analysis_points.description from sampledb.validation_reports join sampledb.analysis_points on validation_reports.analysis_id = analysis_points.id join sampledb.sites on analysis_points.site_id = sites.id join sampledb.companies on sites.company_id = companies.id where validation_reports.id = " + str(self.reportid))
              
              results = conn.fetchone()
              
              db.close()
              
              company, sitename, date, description = results[0], results[1], results[2], results[3]
              
              self.ws.cell(row = 5, column = 1).value = description
              self.ws.merge_cells('A5:I5')
              self.ws.cell(row = 5, column = 1).alignment = Alignment(horizontal='center')
              self.ws.cell(row = 7, column = 2).value = company
              self.ws.cell(row = 10, column = 2).value = sitename
              self.ws.cell(row = 9, column = 2).value = date
              self.ws.cell(row = 9, column = 2).alignment = Alignment(horizontal='left')