from ExcelCellFormatter import *
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import PatternFill, Font
from openpyxl.styles import Alignment
import MySQLdb

class AverageReportSection:
    
    def __init__(self, ws, reportid):
        self.ws = ws
        self.reportid = reportid
        self.numResults = 0
        self.LastRow = 0
        self.cellFormatter = ExcelCellFormatter(self.ws)
        
    def getLastRow(self):
        return self.LastRow
        
    def fillWhiteCells(self):
        WhiteFill = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
        if self.numResults <= 14:    
            self.cellFormatter.setCellColor(1, 19, 9, 23 + 17, WhiteFill)
        elif self.numResults > 14:
            self.cellFormatter.setCellColor(1, 19, 9, 27 + self.numResults, WhiteFill)
            
    def addOtherRows(self):
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
        
        if self.numResults <= 14:
            self.cellFormatter.applyBordertoBlockOfCells(1, 37, 8, 39, full_box_border)
            self.LastRow = 39
        elif self.numResults > 14:
            self.cellFormatter.applyBordertoBlockOfCells(1, 23 + self.numResults + 1, 8, 23 + self.numResults + 3, full_box_border)
            self.LastRow =  23 + self.numResults + 3
            
    def retrieveAverageAbsoluteDeltaValue(self, result, unitname):
        db = MySQLdb.connect("jp3m-db01.jp3m.local","WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        conn.execute('SELECT avg(abs(validation_results.value - results.value)) from sampledb.validation_results join sampledb.result_names on validation_results.result_id = result_names.id join sampledb.units on validation_results.unit_id = units.id join sampledb.results on validation_results.sample_id = results.sample_id and validation_results.result_id = results.result_id and validation_results.unit_id = units.id where validation_results.report_id = %s and result_names.name = %s and units.name = %s group by validation_results.report_id', (self.reportid, result, unitname))
        
        results = conn.fetchone()
        
        db.close()
        
        return results[0]
        
    def retrieveValidationReportAverages(self):
        resultDict = {}
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        
        conn = db.cursor()
        
        conn.execute("SELECT result_names.name, units.name, avg(validation_results.value), TRUNCATE(avg(results.value),3) from sampledb.validation_results join sampledb.result_names on validation_results.result_id = result_names.id join sampledb.units on validation_results.unit_id = units.id join sampledb.results on validation_results.sample_id = results.sample_id and validation_results.result_id = results.result_id and validation_results.unit_id = results.unit_id where validation_results.report_id = " + str(self.reportid) + " group by validation_results.report_id, validation_results.result_id, validation_results.unit_id")
        
        results = conn.fetchall()
        db.close()
        
        for result in results:
            resultDict[result[0]] = (result[1], result[2], result[3])
        
        return resultDict
        
    def formatAverageReport(self):
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
        
        self.cellFormatter.applyBordertoBlockOfCells(1, 22, 1, 22, full_box_border)
        
        self.cellFormatter.applyBordertoBlockOfCells(2, 20, 4, 22, full_box_border)
        
        self.cellFormatter.applyBordertoBlockOfCells(5, 22, 8, 22, full_box_border)   
        
        self.fillOrangeCells()
        self.addOtherRows()
                
        if self.numResults <= 14:
            self.cellFormatter.applyBordertoBlockOfCells(1, 23, 8, 23 + 14, full_box_border)
        elif self.numResults > 14:
            self.cellFormatter.applyBordertoBlockOfCells(1, 23, 8, 23 + self.numResults, full_box_border)
     
    def populateAverageResults(self):
        curRow = 23
        resultNameColumn = 1
        unitNameColumn = 5
        veraxColumn = 2
        ThirdPartyLabColumn = 3
        AbsoluteDeltaColumn = 4
        
        resultDict = self.retrieveValidationReportAverages()
        
        orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'c12+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor Per Gallon', 'GPM Total C2+', 'VPCR4', 'RVP', 'TVP_N2CO2_JP3', 'Dry Vapor Pressure (D5191)']
            
        for result in orderOfResults:
            if resultDict.has_key(result):
                unitName = resultDict[result][0]
                veraxValue = resultDict[result][1]
                labValue = resultDict[result][2]
                self.ws.cell(row = curRow, column = resultNameColumn).value = result
                self.ws.cell(row = curRow, column = resultNameColumn).alignment = Alignment(horizontal='right')
                self.ws.cell(row = curRow, column = veraxColumn).value = veraxValue
                self.ws.cell(row = curRow, column = veraxColumn).number_format = '0.000'
                self.ws.cell(row = curRow, column = veraxColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = ThirdPartyLabColumn).value = labValue
                self.ws.cell(row = curRow, column = ThirdPartyLabColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = AbsoluteDeltaColumn).value = self.retrieveAverageAbsoluteDeltaValue(result, unitName)
                self.ws.cell(row = curRow, column = AbsoluteDeltaColumn).number_format = '0.000'
                self.ws.cell(row = curRow, column = AbsoluteDeltaColumn).alignment = Alignment(horizontal='center')
                self.ws.cell(row = curRow, column = unitNameColumn).value = '[' + str(unitName) + ']'
                self.ws.cell(row = curRow, column = unitNameColumn).alignment = Alignment(horizontal='center')
                curRow += 1
                self.numResults += 1
                
                
        self.fillWhiteCells()
        
        self.formatAverageReport()
        
    
     
        
    def fillOrangeCells(self):
        OrangeFill = PatternFill(start_color='FFCC00', end_color='FFCC00',fill_type='solid')
        self.cellFormatter.setCellColor(1, 22, 8, 22, OrangeFill)
        if self.numResults <= 14:
            self.cellFormatter.setCellColor(1, 37, 8, 37, OrangeFill)
        elif self.numResults > 14:
            self.cellFormatter.setCellColor(1, 23 + self.numResults + 1, 8, 23 + self.numResults + 1, OrangeFill)
        
        
    def createAverageReportHeader(self):
        HeaderFont = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='000000')
        
        full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))
                
        self.ws.cell(row = 20, column = 1).value = "Species or"
        self.ws.cell(row = 20, column = 1).font = HeaderFont
        self.ws.cell(row = 20, column = 1).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 21, column = 1).value = "Parameter"
        self.ws.cell(row = 21, column = 1).font = HeaderFont
        self.ws.cell(row = 21, column = 1).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 20, column = 3).value = "Concentration"
        self.ws.cell(row = 20, column = 3).font = HeaderFont
        self.ws.cell(row = 20, column = 3).alignment = Alignment(horizontal="center")
        
        self.ws.cell(row = 21, column = 2).value = "Verax"
        self.ws.cell(row = 21, column = 2).font = HeaderFont
        self.ws.cell(row = 21, column = 2).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 21, column = 3).value = "3rd Party Lab"
        self.ws.cell(row = 21, column = 3).font = HeaderFont
        self.ws.cell(row = 21, column = 3).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 21, column = 4).value = "Abs. Delta"
        self.ws.cell(row = 21, column = 4).font = HeaderFont
        self.ws.cell(row = 21, column = 4).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 20, column = 5).value = "Units"
        self.ws.cell(row = 20, column = 5).font = HeaderFont
        self.ws.cell(row = 20, column = 5).alignment = Alignment(horizontal='center')
        
        self.ws.cell(row = 20, column = 6).value = "Additional Comments"
        self.ws.cell(row = 20, column = 6).font = HeaderFont
        self.ws.merge_cells('F20:H20')
        
        