from openpyxl import Workbook
from AnalyzerDescriptionSheet import *
from AverageValidationReportSheet import *
from IndividualValidationReportSheet import *
import MySQLdb
import os
import datetime


def main():
    report_id = 275
    wb = Workbook()
    dest_path = r"O:\Validation_Books"
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    conn.execute("SELECT site, analysis_number from sampledb.validation_reports_view where validation_reports_view.id = %s", (report_id))
    
    sitename, analysisnumber = conn.fetchone()
    
    db.close()
    
    analyzerDescriptionSheet = AnalyzerDescriptionSheet(wb, report_id)
    analyzerDescriptionSheet.createSheet()
    
    averageValidationReportSheet = AverageValidationReportSheet(wb, report_id)
    averageValidationReportSheet.createSheet()
    
    individualValidationReportSheet = IndividualValidationReportSheet(wb, report_id)
    individualValidationReportSheet.createSheet()
    
    wb.remove_sheet(wb.get_sheet_by_name('Sheet'))
    
    fullpath = dest_path = r"O:\Validation_Books" + "\\" + str(sitename)
    
    curdate = datetime.date.today().strftime('%Y_%m_%d')
    
    if not os.path.exists(fullpath):
        os.makedirs(fullpath)  
        
    wb.save(fullpath + r"\\" + str(curdate) + "_" + str(sitename).replace(' ', '_') + "_A" + str(analysisnumber) + "_OfficialValidationReport.xlsx")
    
main()