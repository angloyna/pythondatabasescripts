import openpyxl
from openpyxl.styles import PatternFill, Font
from openpyxl.cell import get_column_letter
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule

class ExcelCellFormatter:
    
    def __init__(self, ws):
        self.ws = ws
        
    def setConditionalFormatting(self, curcolumnForDelta, curRowForResults, magnitude):
        yellowFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')
        self.ws.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='greaterThan', formula=[str(magnitude)], stopIfTrue=True, fill=yellowFill))
        self.ws.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='lessThan', formula=['-'+str(magnitude)], stopIfTrue=True, fill=yellowFill))     

    def setColumnWidth(self,columnLetterArray,width):  
        for columnLetter in columnLetterArray:
            self.ws.column_dimensions[columnLetter].width = width
            
    def setRowHeight(self,rowArray,height):  
        for row in rowArray:
            self.ws.row_dimensions[row].height = height
    
    def applyBordertoBlockOfCells(self, startColumn, startRow, endColumn, endRow, borderStyle):
        for i in range(startRow, endRow + 1):
            for k in range(startColumn, endColumn + 1):
                self.ws.cell(row = i, column = k).border = borderStyle
        
    def setCellColor(self, columnStart, rowStart, columnEnd, rowEnd, Color):
        for row in range(rowStart, rowEnd + 1):
            for column in range(columnStart, columnEnd + 1):
                self.ws.cell(row = row, column = column).fill = Color