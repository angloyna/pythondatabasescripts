import MySQLdb
import weightPercentCalculator

db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor()

sampleid = 5081

conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (sampleid))
        
unitsReported = conn.fetchall()
units = []

for unit in unitsReported:
    units.append(unit[0])

moldict = {}    
    
if ('wt %' in units) and ('mol %' in units):
    conn.execute('select result_name, value from sampledb.sample_results where unit_name = "mol %" and result_name in ("c1", "c2", "c3", "ic4", "nc4", "ic5", "nc5", "c6+", "co2", "n2") and sample_id = ' + str(sampleid))
    
    rows = conn.fetchall()
    
    for row in rows:
        moldict[row[0]] = row[1]
        
    calcWtpercentDict = weightPercentCalculator.WtPercentCalculator(moldict, conn)
    
    
    print calcWtpercentDict
    
    conn.execute('select result_name, value from sampledb.sample_results where unit_name = "wt %" and result_name in ("c1", "c2", "c3", "ic4", "nc4", "ic5", "nc5", "c6+", "co2", "n2") and sample_id = ' + str(sampleid))
    
    
    print conn.fetchall()
    
    
db.close()
    