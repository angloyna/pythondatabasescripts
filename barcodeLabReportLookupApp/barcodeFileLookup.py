from Tkinter import *
import tkMessageBox
import tkFileDialog
import MySQLdb
import os
import sys

def go(barcode, t):
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()
    
    conn.execute("SELECT pdf_id, edd_id from sampledb.samples where barcode = " + str(barcode))
    
    results = conn.fetchall()
    
    if results == ():
        tkMessageBox.showinfo("Could Not Find Sample", "This sample barcode could not be found in the database.")
    
    if results != ():
        for result in results:
            pdf_id = result[0]
            edd_id = result[1]
            if pdf_id != None:
                conn.execute("SELECT path from sampledb.pdf_files where id = %s", (pdf_id))
                pdfpath = conn.fetchone()[0]
                os.system('explorer "' + str(pdfpath) + '"')
            if edd_id != None:
                conn.execute("SELECT path from sampledb.edd_files where id = %s", (edd_id))
                eddpath = conn.fetchone()[0]
                os.system('start excel.exe "' + str(eddpath) +'"')
                
            if pdf_id == None and edd_id == None:
                tkMessageBox.showinfo("Could Not Find File", "Neither pdf nor the edd could be found in the database.")
                 
    db.close()
        
    

def findLabReport():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Lab Report Finder')
    labelframe = LabelFrame(top, text="Lab Report Finder")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    barcodeFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    barcodeFrame.pack(fill=X, padx=5, pady=5)        
    barcodelabel = Label(barcodeFrame, text = 'barcode: ', justify = LEFT, width = 8)
    barcodelabel.pack(side = LEFT)
    q = Entry(barcodeFrame, width = 10)
    q.pack(side = LEFT)
    barcodebutton = Button(barcodeFrame, width = 10, text = 'Find Report',command= lambda: go(q.get(), top))
    barcodebutton.pack(side = LEFT)
    
    top.mainloop()
    
findLabReport()