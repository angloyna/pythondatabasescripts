import MySQLdb

def createVeraxCTXinnodb(dbname):
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01")
    
    conn = db.cursor()
    
    conn.execute("create database " + str(dbname))
    
    #dgain is for later versions beyond 1.4...
    #tdataCreate = "CREATE TABLE " + str(dbname) + ".`tdata` (`dID` bigint(11) NOT NULL AUTO_INCREMENT,`dDateStamp` datetime NOT NULL,`dData` text NOT NULL,`dAnalysis` int(11) NOT NULL DEFAULT '1', `dMUX` int(11) NOT NULL DEFAULT '1',`dTemp` double NOT NULL,`dPres` double NOT NULL,`dValve` int(11) NOT NULL DEFAULT '0',`dAlarm` int(11) NOT NULL DEFAULT '0',`dTran` double NOT NULL,`dGrand` double NOT NULL,`dDaily` double NOT NULL,`dHourly` double NOT NULL,`dPrev` double NOT NULL,`dGain` int(11) NOT NULL,PRIMARY KEY (`dID`),KEY `dDateStamp` (`dDateStamp`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
    
    #this is the vcon 1.4 tdata
    tdataCreate = "CREATE TABLE " + str(dbname) + ".`tdata` (`dID` bigint(11) NOT NULL AUTO_INCREMENT,`dDateStamp` datetime NOT NULL,`dData` text NOT NULL,`dAnalysis` int(11) NOT NULL DEFAULT '1', `dMUX` int(11) NOT NULL DEFAULT '1',`dTemp` double NOT NULL,`dPres` double NOT NULL,`dValve` int(11) NOT NULL DEFAULT '0',`dAlarm` int(11) NOT NULL DEFAULT '0',`dTran` double NOT NULL,`dGrand` double NOT NULL,`dDaily` double NOT NULL,`dHourly` double NOT NULL,`dPrev` double NOT NULL,PRIMARY KEY (`dID`),KEY `dDateStamp` (`dDateStamp`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
    
    conn.execute(tdataCreate)
    
    #this tcal is for vcon 2.0
    #tcalCreate = "CREATE TABLE " + dbname + ".`tcal` (`cID` bigint(11) NOT NULL AUTO_INCREMENT,`cDateStamp` datetime NOT NULL,`cData` text NOT NULL,`cAnalysis` int(11) NOT NULL DEFAULT '1',`cMUX` int(11) NOT NULL DEFAULT '1',`cTemp` double NOT NULL,`cPres` double NOT NULL,`cValve` int(11) NOT NULL DEFAULT '0',`cAlarm` int(11) NOT NULL DEFAULT '0',`cTran` double NOT NULL,`cGrand` double NOT NULL,`cDaily` double NOT NULL,`cHourly` double NOT NULL,`cPrev` double NOT NULL,`cGain` int(11) NOT NULL,PRIMARY KEY (`cID`) USING BTREE,KEY `dDateStamp` (`cDateStamp`) USING BTREE) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
    
    #this is the tcal for vcon 1.4
    tcalCreate = "CREATE TABLE " + dbname + ".`tcal` (`cID` bigint(11) NOT NULL AUTO_INCREMENT,`cDateStamp` datetime NOT NULL,`cData` text NOT NULL,`cAnalysis` int(11) NOT NULL DEFAULT '1',`cMUX` int(11) NOT NULL DEFAULT '1',`cTemp` double NOT NULL,`cPres` double NOT NULL,`cValve` int(11) NOT NULL DEFAULT '0',`cAlarm` int(11) NOT NULL DEFAULT '0',`cTran` double NOT NULL,`cGrand` double NOT NULL,`cDaily` double NOT NULL,`cHourly` double NOT NULL,`cPrev` double NOT NULL,PRIMARY KEY (`cID`) USING BTREE,KEY `dDateStamp` (`cDateStamp`) USING BTREE) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
    
    conn.execute(tcalCreate)
    
    tresultsCreate = "CREATE TABLE " + str(dbname) + ".`tresults` (`rID` bigint(11) NOT NULL AUTO_INCREMENT,`rModelID` varchar(200) NOT NULL,`rDateStamp` datetime NOT NULL, `rName` varchar(45) NOT NULL,`rUnit` varchar(10) NOT NULL,`rValue` double NOT NULL,`rVOut` double NOT NULL,`rT2` double NOT NULL,`rQ` double NOT NULL,`rAnalysis` int(11) NOT NULL,PRIMARY KEY (`rID`),KEY `rDateStamp` (`rDateStamp`),KEY `rName` (`rName`),KEY `rModelID` (`rModelID`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
    
    conn.execute(tresultsCreate)
    
    #tspectrometerCreate = "CREATE TABLE " + str(dbname) + ".`tspectrometer` (`sID` bigint(11) NOT NULL AUTO_INCREMENT,`sDateStamp` datetime NOT NULL,`sSerial` int(10) NOT NULL,`sFirmware` varchar(45) CHARACTER SET latin1 NOT NULL,`sDriver` varchar(10) CHARACTER SET latin1 NOT NULL,`sVCON` varchar(10) CHARACTER SET latin1 NOT NULL,`s0VoltCounts` int(10) NOT NULL,`s0Volt` double NOT NULL,`sNeg3p65Volt` double NOT NULL,`s3p3VoltRef` double NOT NULL,`s5Volt` double NOT NULL,`s5VoltRefCounts` int(10) NOT NULL,`s5VoltRef` double NOT NULL,`s15Volt` double NOT NULL,`s150Volt` double NOT NULL,`sBoardTemp` double NOT NULL,`sRemoteDetTemp` double NOT NULL, `sSLED0TECVolt` double NOT NULL, `sSLED0TECCurr` double NOT NULL, `sSLED0TECTherm` double NOT NULL,`sSLED1TECVolt` double NOT NULL,`sSLED1TECCurr` double NOT NULL,`sSLED1TECTherm` double NOT NULL,`sOvenTECVolt` double NOT NULL,`sOvenTECCurr` double NOT NULL,`sOvenTECTherm` double NOT NULL, `sDet0TECVolt` double NOT NULL,`sDet0TECCurr` double NOT NULL,`sDet0TECTherm` double NOT NULL,`sDet1TECVolt` double NOT NULL,`sDet1TECCurr` double NOT NULL, `sDet1TECTherm` double NOT NULL, `sDet2TECVolt` double NOT NULL, `sDet2TECCurr` double NOT NULL,`sDet2TECTherm` double NOT NULL,`sDet3TECVolt` double NOT NULL,`sDet3TECCurr` double NOT NULL,`sDet3TECTherm` double NOT NULL,`sWARMTECVolt` double NOT NULL,`sWARMTECCurr` double NOT NULL,`sWARMTECTherm` double NOT NULL,`sSLED0FSRShift` double NOT NULL,`sSLED1FSRShift` double NOT NULL,`sSLED0FSRShiftWL` double NOT NULL,`sSLED1FSRShiftWL` double NOT NULL,`sSLED0Correct` int(10) NOT NULL,`sSLED1Correct` int(10) NOT NULL,PRIMARY KEY (`sID`) USING BTREE) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC"
    
    #conn.execute(tspectrometerCreate)
    
    #tampfreqCreate = "CREATE TABLE " + str(dbname) + ".`tampfreq` (`aID` bigint(11) NOT NULL AUTO_INCREMENT, `aDateStamp` datetime NOT NULL, `aAmp` text NOT NULL, `aFreq` text NOT NULL, PRIMARY KEY (`aID`)) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC"
    
    #conn.execute(tampfreqCreate)
    
    
    db.commit()
    db.close()
    

dbname = r"p1024b"    
createVeraxCTXinnodb(dbname)