can't find Pseudo Critical Pressure
can't find Pseudo Critical Temp.
can't find Vapor Pressure
can't find C7+ Mol Mass
can't find C7+ Rel Dens
can't find C7+ Abs Dens
can't find C7+ GHV
can't find H2S Method
can't find Sample Point Code
can't find Meter ID
can't find LSD
can't find Test Recovery
can't find Interval 1 From
can't find Interval 1 To
can't find Interval 2 From
can't find Interval 2 To
can't find Interval 3 From
can't find Interval 3 To
can't find Production Rate Water
can't find Production Rate Oil
can't find Production Rate Gas
can't find KB
can't find GR
can't find Analyst
can't find In Lab H2S (mg/L)
can't find In Lab H2S Method
can't find Date Off
can't find Number of Strokes


site: Titan CTX analysis number: 2 barcode: 1702
	lab starttime: 12-29-2015 11:20:00
	lab endtime: 12-29-2015 11:20:00
	temperature: 56.48
	pressure: 384.03944



	DB starttime: 2015-12-29 11:20:25
	DB endtime: 2015-12-29 11:20:25
	DB temperature: 56.4
	DB pressure: 384.0


	 Header Information Checks

		 analysis point match: True

		 temp match: True

		 pres match: True
	Found 1 spectra for sample 7628

		Dry BTU converted from MJ/m3 to BTU/cf
		C6+ value [mol %] was calculated for this sample

		Xylenes [mol %] was calculated for this sample.

	Sum Checks
	mol %: 100.0
