import MySQLdb

class Sample:
    
    def __init__(self, sitename, analysisnumber, barcode, starttime, endtime, temp, pressure, quote_no, job_code, analysisnotes, process_status, receiveddate, labReport, f):
        self.sitename = sitename
        self.analysisnumber = analysisnumber
        self.barcode = barcode
        self.starttime = starttime
        self.endtime = endtime
        self.temp = temp
        self.pressure = pressure
        self.quote_no = quote_no
        self.job_code = job_code
        self.analysisnotes = analysisnotes
        self.process_status = process_status
        self.receiveddate = receiveddate
        self.labReport = labReport
        self.phase = self.labReport.getPhase()
        self.lab = self.labReport.getLabName()
        self.containertype = self.labReport.getContainerType()
        self.f = f
        self.val_bool = 'Y'
        self.sampleid = None
        
    #this function takes the starttime and endtime in the database and matches the spectra in the appropriate local database to the sample in the sample database    
    def addSpectraForSample(self, dbStarttime, dbEndTime):
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"        
        
        conn.execute("SELECT count(id) from sampledb.spectra where sample_id = %s", (self.sampleid,))
                
        numberofSpectra = conn.fetchall()[0][0]
        
        if numberofSpectra == 0:
        
            conn.execute("SELECT localdb from sampledb.sites where name = %s", (self.sitename,))
            
            localdb = conn.fetchall()[0][0]
            db.close()
            
            try:
                db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db=localdb, connect_timeout=100)
                conn = db.cursor()      
            except MySQLdb.Error, e:
                print "MySQL error"
                db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db=localdb, connect_timeout=100)
                conn = db.cursor()
                print "reconnected"  
                
            conn.execute("SELECT cdatestamp, ctemp, cpres, ctran, cdata from tcal where cdatestamp between %s and %s and canalysis = %s", (dbStarttime, dbEndTime, self.analysisnumber))
            
            spectra = conn.fetchall()
            
            if spectra == ():
                self.f.write("\n\tNo records found for this sample.\n")
    
            db.close()
            try:
                db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
                conn = db.cursor()      
            except MySQLdb.Error, e:
                print "MySQL error"
                db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
                conn = db.cursor()
                print "reconnected" 
            if spectra != ():
                for spectrum in spectra:
                    conn.execute("INSERT INTO sampledb.spectra(sample_id, time, temp, pres, tran, spectrum) VALUES(%s, %s, %s, %s, %s, %s)", (self.sampleid, spectrum[0], spectrum[1], spectrum[2], spectrum[3], spectrum[4]))
                    
                conn.execute("SELECT count(id) from sampledb.spectra where sample_id = %s", (self.sampleid,))
                
                spectraCount = conn.fetchall()
                if spectraCount != ():
                    spectraCount = spectraCount[0][0]
                    self.f.write("\n\tFound " + str(spectraCount) + " spectra for sample " + str(self.sampleid) + "\n")
            db.commit()
            db.close()
        else:
            db.close()
            self.f.write("\n\tThere were already spectra in the database with sample_id " + str(self.sampleid) + "\n")  
            
            
    def getPhase(self):
        return self.phase
    
    def getSampleID(self):
        return self.sampleid
    
    def getContainerType(self):
        return self.containertype
    
    def getLabName(self):
        return self.lab
        
    #this function updates the sample's status in the db and links the spectra to the sample record.    
    def process(self):
        tempCheck = False
        presCheck = False
        analysisCheck = False

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        
        self.f.write("\n\nsite: " + str(self.sitename) + " analysis number: " + str(self.analysisnumber) + " barcode: " + str(self.barcode) + "\n\tlab starttime: " + self.starttime.strftime('%m-%d-%Y %H:%M:%S') + "\n\tlab endtime: " + self.endtime.strftime('%m-%d-%Y %H:%M:%S') + "\n\ttemperature: " + str(self.temp) + "\n\tpressure: " + str(self.pressure) + "\n")
                
        conn.execute("SELECT id FROM sites WHERE name = %s", (self.sitename,))
        siteid = conn.fetchall()[0][0]
        
        conn.execute("SELECT id FROM analysis_points WHERE analysis_number = %s AND site_id = %s",(self.analysisnumber, siteid))
        analysisid = conn.fetchall()[0][0]
                
        conn.execute("SELECT sample_id from sampledb.samples where barcode = %s and analysis_id = %s and process_status = 'Sampled'", (self.barcode, analysisid))
        sampleid = conn.fetchall()
        
        if sampleid != ():
            self.sampleid = sampleid[0][0]
            
        else:
            self.f.write("\n\n sample with barcode " + str(self.barcode) + " could not be found in the database.")
            return False
    
        conn.execute("SELECT analysis_id, temp, pres, starttime, endtime from sampledb.samples where sample_id = %s", (self.sampleid))
        
        sampleInfoResults = conn.fetchone()
        
        dbanalysisid = sampleInfoResults[0]
        dbTemp = sampleInfoResults[1]
        dbPres = sampleInfoResults[2]
        dbStarttime = sampleInfoResults[3]
        dbEndTime = sampleInfoResults[4]
        
        self.f.write("\n\n\n\tDB starttime: " + str(dbStarttime) + "\n\tDB endtime: " + str(dbEndTime) + "\n\tDB temperature: " + str(dbTemp) + "\n\tDB pressure: " + str(dbPres) + "\n")

        if dbanalysisid == analysisid:
            analysisCheck = True
        if abs(float(self.temp) - dbTemp) <= 1:
            tempCheck = True
        if abs(float(self.pressure) - dbPres) <= 1:
            presCheck = True
            
        self.f.write("\n\n\t Header Information Checks")
        self.f.write("\n\n\t\t analysis point match: " + str(analysisCheck))
        self.f.write("\n\n\t\t temp match: " + str(tempCheck))
        self.f.write("\n\n\t\t pres match: " + str(presCheck))
                
        conn.execute("UPDATE sampledb.samples set process_status = %s, lab_id = %s, edd_id = %s, pdf_id = %s, method_id = %s, date_received = %s, job_code = %s, quote_no = %s where sample_id = %s", (self.process_status, self.labReport.getLabID(), self.labReport.getEddID(), self.labReport.getPdfID(), self.labReport.getMethodID(), self.receiveddate, self.job_code, self.quote_no, self.sampleid))
            
        if self.analysisnotes != None:
            conn.execute("INSERT INTO sampledb.sample_tweets(author_id, sample_id, tweet) values (1, %s, %s)", (self.sampleid, self.analysisnotes))
                        
        db.commit()
        db.close()
                
        self.addSpectraForSample(dbStarttime, dbEndTime)
        
        return True