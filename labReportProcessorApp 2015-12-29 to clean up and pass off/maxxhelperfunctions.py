import datetime
import MySQLdb
from resultProcessor import *
from Sample import *
from openpyxl import Workbook


#this function takes the sheet of the lab report, the current row being read of the lab report, and the name of the from which the lab report was received. It returns the analysis number, pressure, temperature, barcode, quote number, individual job code, and the notes from the lab associated with the sample.
def getHeaderInformation(sheet_ranges, samplerowstr, lab):
    analysisnumber = sheet_ranges['T' + samplerowstr].value[1:]
    pressure = sheet_ranges['U' + samplerowstr].value
    if lab == 'Maxxam Edmonton':
        pressure = float(pressure) * .14503
    temp = sheet_ranges['X' + samplerowstr].value
    if lab == 'Maxxam Edmonton':
        temp = (float(temp) * (9/5.0)) + 32
    barcode = sheet_ranges['G' + samplerowstr].value
    quote_no = sheet_ranges['F' + samplerowstr].value.split('-')[0].strip()
    job_code = sheet_ranges['F' + samplerowstr].value.split('-')[1].strip()    
    analysisnotes = sheet_ranges['AB' + samplerowstr].value
    sitename = sheet_ranges['B' + samplerowstr].value
    
    return analysisnumber, pressure, temp, barcode, quote_no, job_code, analysisnotes, sitename



#this function finds the date received indicated by the lab report for a given sample in the EDD. If this field is blank in the EDD for this sample, the function returns Null.
def getReceivedDate(sheet_ranges, samplerowstr):
    if sheet_ranges['S' + samplerowstr].value != None:
        receiveddatesplit = sheet_ranges['S' + samplerowstr].value.split(' ')
        receiveddate = datetime.date(int(receiveddatesplit[0]), int(receiveddatesplit[1]), int(receiveddatesplit[2]))        
    if sheet_ranges['S' + samplerowstr].value == None:
        receiveddate = None
        
    return receiveddate


#this function takes the EDD excel worksheet and the current sample row being processed. It converts the starttime and endtime into a datetime format. If there is no endtime, it returns the starttime as the endtime.
def getSamplingTimes(sheet_ranges, samplerowstr):
    starttime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['P' + samplerowstr].value,'%H:%M:%S').time())           
    if sheet_ranges['R' + samplerowstr].value == None:
        endtime = starttime + datetime.timedelta(minutes=1)
    else:
        endtime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['R' + samplerowstr].value,'%H:%M:%S').time())
        
    return starttime, endtime

#this function takes two dictionaries: one that maps the name of the result according to the database table result_names to the primary key of that name in result_names and one that maps the name of the result and unit pair according to the EDD to the column in the EDD where that result/unit pair is located. This comment isn't finished.
def populateNameToResultIDMap(f, dbnames, repSpecMap):
    nameToresultID = {}
    for item in repSpecMap:
        curspecies = repSpecMap[item][0]
        if dbnames.has_key(curspecies):
            nameToresultID[curspecies] = dbnames[curspecies]
        else:
            f.write("can't find " + str(curspecies) + "\n")
    return nameToresultID

#this function populates the unit dictionary that maps the spl_name field of the units table to the primary key of that unit name
def populateDBUnitMap():
    dbUnitMap = {}
    try:
        db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
        conn = db.cursor()      
    except MySQLdb.Error, e:
        print "MySQL error"
        db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
        conn = db.cursor()
        print "reconnected"   
        
    conn.execute("SELECT id, spl_name FROM units")
        
    units = conn.fetchall()
    
    for unit in units:
        dbUnitMap[unit[1].lower()] = unit[0]
    
    db.close()    
    return dbUnitMap


#this function takes the EDD worksheet for the lab report and the column where the results start in the EDD (after the header information) and maps the result name/unit pair to the column number in the EDD
def populateRepSpecMap(sheet_ranges, resultsStartCol):
    repSpecMap = {}
    curcol = resultsStartCol
    curcell = sheet_ranges.cell(row = 1, column = curcol).value
    while curcell != None:
        resultunitpair = curcell.split('[')
        if curcell.find("[") >= 0:
            spec = resultunitpair[0].strip()
            units = resultunitpair[1].split(']')[0].strip().lower()
            if units == 'wt %':
                units = 'wt. %'
            if units == 'lv %':
                units = 'l.v. %'
        else:
            spec = resultunitpair[0].strip()
            units = 'None'
        repSpecMap[curcol] = (spec, units)
        curcol += 1
        curcell = sheet_ranges.cell(row = 1, column = curcol).value
        
    return repSpecMap


#this function takes the phase of the samples in the lab report and the name of the laboratory that reported the lab report and populates a dictionary based on these values. It maps the primary key in the result_names table to the appropriate name of the result used in the EDD template currently being processed
def populateDbNamesDictionary(phase, lab):
    dbnames = {}
    
    try:
        db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
        conn = db.cursor()      
    except MySQLdb.Error, e:
        print "MySQL error"
        db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
        conn = db.cursor()
        print "reconnected"  
        
    if phase == 'Gas' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c30plus from result_names;")
    if phase == 'Gas' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c30plus from result_names;")       
        
    rows = conn.fetchall()
    db.close()
    for row in rows:
        dbnames[row[1]] = row[0]
        
    return dbnames
   
def createTimeStamp(datestr, timestr):
    startdatesplit = datestr.split()
    startdate = datetime.date(int(startdatesplit[0]), int(startdatesplit[1]), int(startdatesplit[2]))
    starttime = timestr
    starttimestamp = datetime.datetime.combine(startdate, starttime)
    return starttimestamp