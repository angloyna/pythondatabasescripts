from openpyxl import Workbook
import MySQLdb

class ResultProcessor:
    
    def __init__(self, ws, resultsStartCol, samplerow, repSpecMap, nameToresultID, dbUnitMap, f, curSample):
        self.ws = ws
        self.curcol = resultsStartCol
        self.samplerow = samplerow
        self.repSpecMap = repSpecMap
        self.nameToresultID = nameToresultID
        self.dbUnitMap = dbUnitMap
        self.curSample = curSample
        self.phase = self.curSample.getPhase()
        self.lab = self.curSample.getLabName()
        self.f = f
        self.sampleid = self.curSample.getSampleID()
        self.containertype = self.curSample.getContainerType()
        
    #this function calculates TVP based on the GPA 2145 partial pressure coefficients and includes the values stored for N2 and CO2 in the summation.  
    def calculateTVPN2CO2(self):
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"
            
        conn.execute("SELECT result_name, value from sampledb.sample_results where sample_id = " + str(self.sampleid) + " and unit_name = 'mol %' and result_name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'n2', 'co2')")
                                    
        tvpmolpercentsf = conn.fetchall()
        
        if len(tvpmolpercentsf) == 10:
            conn.execute("SELECT name, vapor_pressure_psia from sampledb.result_names join sampledb.gpa_2145_coefficients on result_names.id = gpa_2145_coefficients.result_id where name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'n2', 'co2')")
            
            partialPressureFactorDict = {}
            partialPressureFactorResults = conn.fetchall()
            
            for partialPressureFactor in partialPressureFactorResults:
                partialPressureFactorDict[partialPressureFactor[0]] = partialPressureFactor[1]
        
            TVPwN2CO2 = 0    
            for molpercent in tvpmolpercentsf:
                name = molpercent[0]
                molfract = (float(molpercent[1]) / 100)
                factor = partialPressureFactorDict[name]
                TVPwN2CO2 += (factor * molfract)
                
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 94, 11, %s)", (self.sampleid, TVPwN2CO2))
            
            self.f.write("\n\tThe TVP_N2CO2_JP3 value for sample " + str(self.sampleid) + " has been calculated\n.")
            
        elif len(tvpmolpercents) != 10:
            self.f.write("\n\tThe TVP_N2CO2_JP3 value for sample " + str(self.sampleid) + " could not be calculated\n.")
            
        db.commit()
        db.close()
    
    
    
    #this function calculates TVP from GPA 2145 coefficients without including the partial pressures of N2 and CO2. 
    def calculateTVPNoN2CO2(self):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        conn.execute("SELECT result_name, value from sampledb.sample_results where sample_id = " + str(self.sampleid) + " and unit_name = 'mol %' and result_name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+')")
                    
        tvpmolpercents = conn.fetchall()
        
        if len(tvpmolpercents) == 8:
            conn.execute("SELECT name, vapor_pressure_psia from sampledb.result_names join sampledb.gpa_2145_coefficients on result_names.id = gpa_2145_coefficients.result_id where name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+')")
            
            partialPressureFactorDict = {}
            partialPressureFactorResults = conn.fetchall()
            
            for partialPressureFactor in partialPressureFactorResults:
                partialPressureFactorDict[partialPressureFactor[0]] = partialPressureFactor[1]
        
            TVPnoN2CO2 = 0    
            for molpercent in tvpmolpercents:
                name = molpercent[0]
                molfract = (float(molpercent[1]) / 100)
                factor = partialPressureFactorDict[name]
                TVPnoN2CO2 += (factor * molfract)
                
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 93, 11, %s)", (self.sampleid, TVPnoN2CO2))
            self.f.write("\n\tThe TVPnoN2CO2 value for sample " + str(self.sampleid) + " has been calculated\n.")
            
        elif len(tvpmolpercents) != 8:
            self.f.write("\n\tThe TVPnoN2CO2 value for sample " + str(self.sampleid) + " could not be calculated\n.")
            
        db.commit()
        db.close()
        
    #this function converts VPCR4 to RVPE if the container used is Water displacement. The factor used is .834   
    def convertVPCR4ToRVPE(self): 
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"
            
        conn.execute("SELECT value from sampledb.results where result_id = 72 and unit_id = 6 and sample_id = %s and sample_id not in (select distinct sample_id from sampledb.results where result_id = 70 and unit_id = 6)", (self.sampleid))
                        
        vpcrpsiValue = conn.fetchall()
        
        if vpcrpsiValue != ():
            vpcrpsiValue = vpcrpsiValue[0][0]
            rvpepsiValue = float(vpcrpsiValue) * .834
            
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 70, 6, %s)", (self.sampleid, rvpepsiValue))
            
            self.f.write("\n\tThe VPCR4 value for sample " + str(self.sampleid) + " was converted to rvpe from vpcr4 (psi)\n.") 
            
        db.commit()
        db.close()
    
    
    #this function converts VPCR4 from kPascals to PSI if the lab that sent the report was Maxxam Edmonton (the Canadian lab that sends everything in SI units.) It then stores the PSI value along with the kPa value
    def convertVPCR4kPaToPSI(self):
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        
        conn.execute("SELECT value from sampledb.results where result_id = 72 and unit_id = 7 and sample_id = %s and sample_id not in (select distinct sample_id from sampledb.results where result_id = 72 and unit_id = 6)", (self.sampleid))
                    
        siVaporPressure = conn.fetchall()
        
        if siVaporPressure != ():
            siVaporPressure = siVaporPressure[0][0]
            psiVaporPressure = float(siVaporPressure) * .14503
            
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 72, 6, %s)", (self.sampleid, psiVaporPressure))
            
            self.f.write("\n\tThe VPCR4 value for sample " + str(self.sampleid) + " was converted to psi from kpa\n.")
            
        db.commit()
        db.close()
   
    #this calculates the sum from the values commited to the database for a particular sample. The value is calculated using c6+.            
    def doSumChecks(self):
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        
        self.f.write("\n\n\tSum Checks")
        conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (self.sampleid))
        
        units = conn.fetchall()
    
        for unit in units:
            if unit[0] in ['mol %', 'wt %', 'lv %']:
                conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(unit[0]) + "' and result_name in ('he', 'h2', 'co2', 'n2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+') and sample_id = " + str(self.sampleid))
    
                sumvalues = conn.fetchall()
                sum = 0
                for value in sumvalues:
                    sum += value[0]
            
                self.f.write("\n\t" + str(unit[0]) + ": " + str(sum) + "\n") 
                
        db.close()    
    
    #this function calculates C7+ from C6+ for a particular unit, assuming c7+ does not already exist. First, it finds the c6+ value in that unit set. If it exists, it finds the c6 and subtracts the value from c6+ if they both exist.
    def calculateC7plusFromC6Plus(self, specUnitname, specUnitId):
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+' and sample_id = " + str(self.sampleid))
                            
        c6plusNoc7plus = conn.fetchall()
        #if there is a c6 plus value
        if c6plusNoc7plus != ():
            c6plusNoc7plus = c6plusNoc7plus[0][0]
            #find c6 value if c6 plus value exists
            conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'C6' and sample_id = " + str(self.sampleid))
            
            c6valueNoc7 = conn.fetchall()
            
            #if c6 value exists
            if c6valueNoc7 != ():
                c6valueNoc7 = c6valueNoc7[0][0]
                #subtract c6 from c6 plus to arrive at c7 plus
                c7plusvalueLiq = float(c6plusNoc7plus) - float(c6valueNoc7)
                #add c7 plus to database
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values (%s, 15, %s, %s)", (self.sampleid, specUnitId, c7plusvalueLiq))
            #if there is no c6 value in addition to a c6+ value, it cannot calculate c7 plus easily, so it won't and will tell the user this.
            else:
                self.f.write("\n\tThis sample has a c6+ value but has neither a c7+ value nor a c6 value. C7+ could not be calculated.")
        #if the sample doesn't have a c6 plus value or a c7 plus value, the program does nothing     
        else:
            self.f.write("\n\tThis sample has neither a c6 plus nor a c7 plus value. c7 plus has not been calculated.")
            
        db.commit()
        db.close()
      
    #this function calculates c6+ from C7+ by adding c6 to c7+ if they both exist for a particular unit. The function takes a c7+ value as a parameter.
    def calculateC6PlusFromC7Plus(self, c7plusvalue, specUnitname, specUnitId):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'C6' and sample_id = " + str(self.sampleid))
        c6value = conn.fetchall()
        #if c6 value exists
        if c6value != ():
            
            c6value = c6value[0][0]
            #add c6 to c7+ to arrive at c6 plus value
            c6plusvalue = float(c6value) + float(c7plusvalue)
            #insert this c6 plus value into the db
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 11, %s, %s)", (self.sampleid, specUnitId, c6plusvalue))
            #tell the user it did this
            self.f.write("\n\t\tC6+ value [" + str(specUnitname) + "] was calculated for this sample\n")
            
        db.commit()
        db.close()
        
    #this function calculates c6+ from C15+ for a given unit. It first checks to see if there is a c6+ value already entered into the database. It then attempts to find the values c6-c15+. If it finds them all in the database, it calculates c6+.
    def calculateC6plusFromC15Plus(self, specUnitname, specUnitId):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
            
        #check to see if there is already a c6 plus value
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+' and sample_id = " + str(self.sampleid))
        
        c6plusgasCheck = conn.fetchall()
        
        #if there's no c6 plus value in the db already
        if c6plusgasCheck == ():
            #Find the values which make up c6 plus if no c6 plus or c7 plus values exist
            conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15+') and sample_id = " + str(self.sampleid))
        
            c6plusValuesGas = conn.fetchall()
            c6plus = 0
            
            #make sure the db has all the values asked for
            if len(c6plusValuesGas) == 10:
                #sum the values
                for value in c6plusValuesGas:
                    c6plus += float(value[0])
                    
                #add the c6 plus value to the db
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 11, %s, %s)", (self.sampleid, specUnitId, c6plus))
                self.f.write("\n\t\tC6 plus [" + str(specUnitname) + "] was calculated for this sample.")
                
            else:
                #if the db doesn't have all the values asked for, don't calculate the c6 plus value
                self.f.write("\n\t\tNo C6 plus value calculated for sample " + str(self.sampleid) + " because could not find c6 through c15+ in database.")
                
        db.commit()
        db.close()
        
    #this function calculates c7+ from c15+ for a given units. It assumes there is no c7+ value already in the database for the sample.   
    def calculateC7plusFromC15Plus(self, specUnitname, specUnitId):
   
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"  
            
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15+') and sample_id = " + str(self.sampleid))
                                                    
        c7plusValuesGas = conn.fetchall()
                            
        c7plus = 0
        #make sure it has all the values asked for
        if len(c7plusValuesGas) == 9:
            
            for value in c7plusValuesGas:
                c7plus += float(value[0])
                
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values (%s, 15, %s, %s)", (self.sampleid, specUnitId, c7plus))
            self.f.write("\n\t\tC7 plus [" + str(specUnitname) + "] was calculated for this sample.")
            
        else:
            self.f.write("\n\t\tNo C7 plus value calculated for sample " + str(self.sampleid) + " because could not find c7 through c15+ in database.")
            
        db.commit()
        db.close()
        
    #this function calculates xylenes from meta/para xylenes and orthoxylenes individual values if they exist.
    def calculateXylenes(self, specUnitname, specUnitId):
 
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and sample_id = " + str(self.sampleid) + " and result_name in ('m+p-xylenes', 'o-xylene') and sample_id not in (SELECT DISTINCT sample_id from sampledb.sample_results where unit_name = '" + str(specUnitname)  + "' and result_name = 'Xylenes')")
                        
        xyleneValues = conn.fetchall()
        totalXylenes = 0
        if len(xyleneValues) == 2:
            for xylene in xyleneValues:
                totalXylenes += xylene[0]
            
            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 46, %s, %s)", (self.sampleid, specUnitId, totalXylenes))
            self.f.write("\n\t\tXylenes [" + str(specUnitname) + "] was calculated for this sample.")
            
        db.commit()
        db.close()

       
    #this function calculates c12+ from c12-c30+ values for a given unit if possible. If all of those values don't exist, it attempts to calculate from c12-c15+ values.    
    def calculateC12Plus(self, specUnitname, specUnitId):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and sample_id = " + str(self.sampleid) + " and result_name = 'c12+'")
                            
        if conn.fetchall() == ():    
        
            conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30+') and sample_id = " + str(self.sampleid))
                                    
            c12plusValues = conn.fetchall()
            c12plus = 0
            
            if len(c12plusValues) == 19:
                for value in c12plusValues:
                    c12plus += float(value[0])
                    
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) VALUES (%s, 73, %s, %s)", (self.sampleid, specUnitId, c12plus))
                self.f.write("\n\t\tC12 plus [" + str(specUnitname) + "] was calculated for this sample.")
                
            if len(c12plusValues) != 19:
                conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c12', 'c13', 'c14', 'c15+') and sample_id = " + str(self.sampleid))
                
                c15plusC12values = conn.fetchall()
                
                c12plus = 0
                
                if len(c15plusC12values) == 4:
                    for value in c15plusC12values:
                        c12plus += float(value[0])
                        
                    conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) VALUES (%s, 73, %s, %s)", (self.sampleid, specUnitId, c12plus))
                
                else:
                    self.f.write("\n\n\t\tCount not calculate c12 plus for this sample.")
                
        db.commit()
        db.close()
    
    #this function converts the heating value in the lab report to BTU/cf assuming the parameter passed into it is in MJ/m3    
    def convertHeatingValueToBTU(self, value, resultid):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"
            
        value = (float(value) * 947.81712) / 35.314
        unitid = 8
        conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) values(%s, %s, %s, %s)", (self.sampleid, resultid, unitid, value))
        self.f.write("\n\t\tDry BTU converted from MJ/m3 to BTU/cf")
        db.commit()
        db.close()
    
    #this function calculates the API gravity of a liquid from its relative density.
    def calulateAPIGravityFromRelDensity(self, value):
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        resultid = 51
        value = (141.5/float(value)) - 131.5
        unitid = 4
        conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) values (%s, %s, %s, %s)", (self.sampleid, resultid, unitid, value))
        self.f.write("\n\t\tAPI Gravity calculated for this sample\n")
        
        db.commit()
        db.close()
    
    #this function reads through the results for a given row in the EDD and enters the values into the database. It multiples values by 100 for maxxam edmonton because they report their values in fractions in the EDD.            
    def processLabResults(self):        

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        curcell = self.ws.cell(row = self.samplerow, column = self.curcol).value
        while self.ws.cell(row = 1, column = self.curcol).value != None:
            resultname = self.repSpecMap[self.curcol][0]
            unitname = self.repSpecMap[self.curcol][1].lower()
            value = curcell
            if unitname == 'volume fraction':
                value = float(value) * 100
                unitname = 'l.v. %'
            if unitname == 'mass fraction':
                value = float(value) * 100
                unitname = 'wt. %'
            if unitname == 'mol fraction':
                value = float(value) * 100
                unitname = 'mol %'
            if self.nameToresultID.has_key(resultname) and self.dbUnitMap.has_key(unitname):
                resultid = self.nameToresultID[resultname]
                unitid = self.dbUnitMap[unitname]
                if resultid == 62 and self.phase == 'Gas' and self.lab == 'Maxxam Edmonton' and unitid == 4:
                    unitid = 13
                if value != None and not (resultid == 72 and value == 0):
                    conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) VALUES (%s, %s, %s, %s)", (self.sampleid, resultid, unitid, value))
                    #Dry BTU conversion from MJ/m3 to BTU/cf
                    if resultid == 62 and unitid == 13 and self.phase == 'Gas' and self.lab == 'Maxxam Edmonton':                            
                        self.convertHeatingValueToBTU(value, resultid)                            
                    #API gravity calculation
                    if resultid == 60 and unitid == 4 and self.phase == 'Liquid':
                        self.calulateAPIGravityFromRelDensity(value)                            
            self.curcol += 1
            curcell = self.ws.cell(row = self.samplerow, column = self.curcol).value
         
        db.commit()    
        db.close()
   #this function loops through the three speciation units and calculates all the additional speciation values usually needed for modeling (c6+, c7+, c12+, etc). It also performs conversions for vapor pressure.   
    def performAdditionalResultCalculations(self):

        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"  
            
        conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (self.sampleid))                        
        speciationUnits = conn.fetchall()
                
        #add c6 plus and c7 plus for all the units each sample is reported in
        for specUnit in speciationUnits:
            #makes sure the unit pulled is a speciation unit
            specUnitname = specUnit[0]
            if specUnitname in ['mol %', 'wt %', 'lv %']:
                if specUnitname == 'lv %':
                    specUnitname = 'l.v. %'
                if specUnitname == 'wt %':
                    specUnitname = 'wt. %'
                specUnitId = self.dbUnitMap[specUnitname]
                if specUnitname == 'l.v. %':
                    specUnitname = 'lv %'
                if specUnitname == 'wt. %':
                    specUnitname = 'wt %'                
                #finds c7+ value for the sample in the appropriate units if there isn't a c6plus value also.
                conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c7+' and sample_id = " + str(self.sampleid) + " and sample_id not in (select distinct sample_id from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+')")
            
                c7plusvalue = conn.fetchall()
                #checks to see if there is a c7 plus value. This handles the case where there is a c7 plus value and no c6 plus value
                if c7plusvalue != ():
                    c7plusvalue = c7plusvalue[0][0]
                    #finds the c6 value for the sample in appropriate units
                    self.calculateC6PlusFromC7Plus(c7plusvalue, specUnitname, specUnitId)
                                            
                #This handles the case where there is no c7 plus value, but there may be a c6 plus value
                if self.phase == 'Liquid' and c7plusvalue == ():
                    #check to see if there is a c6+ value
                    self.calculateC7plusFromC6Plus(specUnitname, specUnitId)
                    
                #This handles the case where it is a gas sample and it does not have a c7 plus value but may have a c6 plus value    
                if self.phase == 'Gas' and c7plusvalue == ():
                    self.calculateC6plusFromC15Plus(specUnitname, specUnitId)
                                                                                            
                    #Now do c7 plus  
                    self.calculateC7plusFromC15Plus(specUnitname, specUnitId)
                                                        
                #Xylenes calculation
                self.calculateXylenes(specUnitname, specUnitId)
                                                                                    
                #c12 plus calculation
                
                if self.phase == 'Liquid':
                        self.calculateC12Plus(specUnitname, specUnitId)
                                                                    
        if self.phase == 'Liquid':
            self.convertVPCR4kPaToPSI()
            self.calculateTVPNoN2CO2()
            self.calculateTVPN2CO2()                                                                            
            if self.containertype == 'Water Disp Cylinder':
                self.convertVPCR4ToRVPE()
                
                
        db.commit()
        db.close()
                                                                       

                    