
import matplotlib.pyplot as plt
import MySQLdb


def getXvaluesArray(min,max):
    xValues = []
    i = min
        
    while i <= max:
        xValues.append(i)
        i += .5        
    
    return xValues

def getSpectraPerSample(sampleids):
    spectraDict = {}
    
    for sampleid in sampleids:
            
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        
        conn.execute("select site, analysis_number, starttime from sampledb.sample_info where sample_id = %s", (sampleid))
        headerinfo = conn.fetchall()
        
        conn.execute("select spectrum from sampledb.spectra where sample_id = %s", (sampleid))
        
        results = conn.fetchall()
        
        db.close()
        
        yValues = []
        
        for result in results:
            yValueSet = result[0].split(",")
            yValueSet = [float(x) for x in yValueSet]
            yValues.append(yValueSet)
        spectraDict[str(sampleid) + " " + str(headerinfo[0][0]) + " " + str(headerinfo[0][1]) + " " + str(headerinfo[0][2])] = yValues    
    
    return spectraDict


def plotSpectraPerSample(sampleids):
    
    xValues = getXvaluesArray(1355,1795)
   
    spectraDict = getSpectraPerSample(sampleids)
    
    if len(spectraDict) > 1:
        f, axarr = plt.subplots(len(spectraDict), sharex=True)
        subPlotindex = 0
        
        for key in spectraDict:
            for yValue in spectraDict[key]:
                axarr[subPlotindex].plot(xValues, yValue)
            axarr[subPlotindex].set_title(key)
            subPlotindex += 1
    else:
        f, axarr = plt.subplots(len(spectraDict), sharex=True)
        for key in spectraDict:
            for yValue in spectraDict[key]:
                axarr.plot(xValues, yValue)
            axarr.set_title(key)


sampleids = [7212L, 7211L, 7228L, 7225L, 7202L, 7203L, 7204L]


i = 0
while i < len(sampleids):
    plotSpectraPerSample(sampleids[i:i+4])
    i += 4
    
