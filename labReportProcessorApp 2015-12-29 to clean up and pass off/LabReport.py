import MySQLdb

class LabReport:
    
    def __init__(self, lab, method, excelfilepath, pdfpath, containertype, analysistype, phase):
        self.lab = lab
        self.method = method
        self.excelfilepath = excelfilepath
        self.pdfpath = pdfpath
        self.analysistype = analysistype
        self.containertype = containertype
        self.phase = phase
        self.labid, self.methodid, self.samplerid, self.containerid, self.analysistypeid = self.getHeaderForeignKeys()
        self.pdf_id, self.edd_id = self.addReportFilesToDB()
        
        
    def getHeaderForeignKeys(self):
        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected"         
        
        conn.execute("SELECT id from sampledb.labs where name = %s", (self.lab))
            
        labid = conn.fetchall()[0][0]  
        
        conn.execute('SELECT id from sampledb.analysis_types where name = %s', self.analysistype)
        analysistypeid = conn.fetchall()[0][0]    
            
        conn.execute("SELECT id from sampledb.methods where name = '" + str(self.method) + "'")
        methodid = conn.fetchall()[0][0]    
            
        conn.execute("SELECT id from companies where name = 'JP3 Measurement'")
        samplerid = conn.fetchall()[0][0]    
            
        conn.execute("SELECT id FROM container_type where name = %s", (self.containertype))
        containerid = conn.fetchall()[0][0]
        
        db.close()
        
        return labid, methodid, samplerid, containerid, analysistypeid
    
    
    def addReportFilesToDB(self):        
        try:
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()      
        except MySQLdb.Error, e:
            print "MySQL error"
            db = MySQLdb.connect(host="jp3m-db01.jp3m.local",user="WHoward",passwd="Austin01", db="sampledb", connect_timeout=100)
            conn = db.cursor()
            print "reconnected" 
            
        conn.execute("INSERT INTO sampledb.edd_files(path, date_received, lab_id) values(%s, NOW(), %s)", (self.excelfilepath, self.labid))
        
        if self.pdfpath != 'None':
            conn.execute("INSERT INTO sampledb.pdf_files(path, date_received, lab_id) values(%s, NOW(), %s)", (self.pdfpath, self.labid))
            conn.execute("SELECT id from sampledb.pdf_files where path = %s", (self.pdfpath))
            pdf_id = conn.fetchall()[0][0]
            
        if self.pdfpath == 'None':
            pdf_id = None
                
        conn.execute("SELECT id from sampledb.edd_files where path = %s", (self.excelfilepath))
          
        edd_id = conn.fetchall()[0][0]
        
        db.commit()
        db.close()
        
        return pdf_id, edd_id    
        
        
    def getLabID(self):
        return self.labid
    
    def getEddID(self):
        return self.edd_id
    
    def getPdfID(self):
        return self.pdf_id
    
    def getMethodID(self):
        return self.methodid
    
    def getPhase(self):
        return self.phase
    
    def getContainerType(self):
        return self.containertype
    
    def getLabName(self):
        return self.lab
    