from openpyxl import Workbook
from openpyxl import load_workbook
from maxxhelperfunctions import *
import MySQLdb
from Sample import *
from LabReport import *
from resultProcessor import *
import os

def maxxamReader(excelfilepath, pdfpath, analysistype, phase, containertype, analysislab, Method):
    sampleids = []    
    
    f = open('sampleProcessingSummary.txt', 'w')
    
    wb = load_workbook(filename = excelfilepath)
    sheet_ranges = wb.worksheets[0]
    resultsStartCol = 29
                  
    lab = analysislab
    method = Method 
    process_status = "Processed"
    
    repSpecMap = populateRepSpecMap(sheet_ranges, resultsStartCol)
        
    dbnames = populateDbNamesDictionary(phase, lab)
    
    nameToresultID = populateNameToResultIDMap(f, dbnames, repSpecMap)    
        
    dbUnitMap = populateDBUnitMap()
            
    samplerow = 2
    
    labReport = LabReport(lab, method, excelfilepath, pdfpath, containertype, analysistype, phase)
    
    cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
                
    while cursamplecell != None:
        
        samplerowstr = str(samplerow)
               
        starttime, endtime = getSamplingTimes(sheet_ranges, samplerowstr)
        
        receiveddate = getReceivedDate(sheet_ranges, samplerowstr)
        
        analysisnumber, pressure, temp, barcode, quote_no, job_code, analysisnotes, sitename = getHeaderInformation(sheet_ranges, samplerowstr, lab)
        
        curSample = Sample(sitename, analysisnumber, barcode, starttime, endtime, temp, pressure, quote_no, job_code, analysisnotes, process_status, receiveddate, labReport, f)
        foundSample = curSample.process()
        if foundSample == False:
            break
                                                 
        resultProcessor = ResultProcessor(sheet_ranges, resultsStartCol, samplerow, repSpecMap, nameToresultID, dbUnitMap, f, curSample)
        resultProcessor.processLabResults()
        resultProcessor.performAdditionalResultCalculations()
        resultProcessor.doSumChecks()
        
        samplerow += 1
        cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
           
    f.close()                 
    
    os.system("notepad.exe " + "sampleProcessingSummary.txt")
