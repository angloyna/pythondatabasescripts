import MySQLdb
import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter
import numpy
import datetime
import os
import sys



def modelValidationReport(predictionpath, unitsmodeled):
    spectraPerSample = {}
    resultNameMap = {}
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")
    
    conn = db.cursor()
    
    unit_name = unitsmodeled
    excelfilename = predictionpath
    dateStampCol = 1
    tempCol = 2
    presCol = 3
    resultCol = 4
    
    redFill = PatternFill(start_color='FFFF80', end_color='FFFF80', fill_type='solid')
    
    wb = load_workbook(filename = excelfilename)
    sh = wb.worksheets[0]
    
    summarySheet = wb.create_sheet()
    
    curcol = dateStampCol
    curRow = 2
    
    curcell = sh.cell(row = curRow, column = curcol).value
    #run through each row in Model Predictor Output and do a count of spectra per sample
    
    while curcell != None:
        #find the sample_id associated
        conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s", (curcell, sh.cell(row = curRow, column = curcol + 1).value, sh.cell(row = curRow, column = curcol + 2).value))
        sample_id = conn.fetchall()[0][0]
        if not spectraPerSample.has_key(sample_id):
            spectraPerSample[sample_id] = 1
        if spectraPerSample.has_key(sample_id):
            numSamples = spectraPerSample[sample_id]
            spectraPerSample[sample_id] = numSamples + 1
            
        curRow += 1
        curcell = sh.cell(row = curRow, column = curcol).value
        
    #run through each column title and map the title to the column number
    curcol = resultCol
    curRow = 1
    
    curcell = sh.cell(row = curRow, column = curcol).value
    
    while curcell != None:
        if curcell.find('T2_') == -1 and curcell.find('Q_') == -1:
            resultNameMap[curcell.lower()] = curcol
        curcol += 1
        curcell = sh.cell(row = curRow, column = curcol).value
    
    samplerow = 2
    curcell = sh.cell(row = samplerow, column = 1).value
    curcolumnForSampleDate = 4
    curcolumnForAnalysisPoint = 1
    curcolumnForVeraxValue = 2
    curcolumnForVeraxStdev = 3
    curcolumnForMaxxamValue = 4
    curcolumnFortempAndPres = 2
    curcolumnForresultNames = 1
    curcolumnForHeaderStart = 2
    curcolumnForDelta = 5
    curcolumnForTs = 6
    curcolumnForQ = 7
    curRowForResults = 5
    
    orderOfResults = ['n2', 'co2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5','nc5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30', 'c30+', 'c6+', 'c7+', 'Relative Density', 'API Gravity', 'Dry BTU', 'Cu. Ft. Vapor', 'GPM Total C2+', 'VPCR4']
    
    font = Font(name='Calibri', size=11, bold=True,italic=False,vertAlign=None,underline='none',strike=False, color='FF000000')
    
    while curcell != None:
        curcol = 4
        curValue = sh.cell(row = samplerow, column = curcol).value
        
        conn.execute("SELECT sample_id from sampledb.spectra where time = %s and temp = %s and pres = %s", (curcell, sh.cell(row = samplerow, column = 2).value, sh.cell(row = samplerow, column = 3).value))
        sample_id = conn.fetchall()[0][0]
        
        conn.execute("SELECT starttime, temp, pres, site, analysis_number from sampledb.sample_info where sample_id = %s", (sample_id))
        
        results = conn.fetchall()
        starttime = results[0][0]
        temp = results[0][1]
        pres = results[0][2]
        sitename = results[0][3]
        analysis_number = results[0][4]
        
        
        
        summarySheet.cell(row = 1, column = curcolumnForSampleDate - 1).value = 'Sample Date'
        summarySheet.cell(row = 1, column = curcolumnForSampleDate - 1).font = font
        summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate - 1)].width = len((summarySheet.cell(row = 1, column = (curcolumnForSampleDate - 1)).value)) + 2
        summarySheet.cell(row = 1, column = curcolumnForSampleDate).value = starttime
        summarySheet.column_dimensions[get_column_letter(curcolumnForSampleDate)].width = len((summarySheet.cell(row = 1, column = curcolumnForSampleDate).value).strftime('%Y-%m-%d %H:%M:%S'))
        summarySheet.cell(row = 1, column = curcolumnForAnalysisPoint).value = str(sitename) + " A" + str(analysis_number)
        summarySheet.cell(row = 2, column = curcolumnFortempAndPres - 1).value = 'Temperature'
        summarySheet.cell(row = 2, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.column_dimensions[get_column_letter(curcolumnForAnalysisPoint)].width = len((summarySheet.cell(row = 2, column = curcolumnForAnalysisPoint).value)) + 5
        summarySheet.cell(row = 2, column = curcolumnFortempAndPres).value = temp
        summarySheet.cell(row = 3, column = curcolumnFortempAndPres - 1).value = 'Pressure'
        summarySheet.cell(row = 3, column = curcolumnFortempAndPres - 1).font = font
        summarySheet.cell(row = 3, column = curcolumnFortempAndPres).value = pres
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart).value = 'Verax'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart).font = font
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 1).value = 'Verax Stdev'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 1).font = font
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 2).value = 'Maxxam'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 2).font = font
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 3).value = 'Delta'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 3).font = font
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 4).value = 'Average T2'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 4).font = font
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 5).value = 'Average Q'
        summarySheet.cell(row = 4, column = curcolumnForHeaderStart + 5).font = font
        
        orderCounter = 0
        numSpectraForSample = spectraPerSample[sample_id]
        while orderCounter < len(orderOfResults):
            while curValue != None:
                nextResultName = orderOfResults[orderCounter]
    
                if resultNameMap.has_key(nextResultName.lower()):
                    resultname = nextResultName
                    valueCol = resultNameMap[nextResultName.lower()]
                    averageValue = 0
                    averageTs = 0
                    averageQ = 0
                    values = []
                    for i in range(0, numSpectraForSample-1):
                        averageValue += sh.cell(row = (samplerow + i), column = valueCol).value
                        averageTs += sh.cell(row = (samplerow + i), column = valueCol + 1).value
                        averageQ += sh.cell(row = (samplerow + i), column = valueCol + 2).value
                        values.append(sh.cell(row = (samplerow + i), column = valueCol).value)
                        
                    #need to add this average value to the new spreadsheet with all the overview shit on it.
                    
                    averageValue = averageValue / (numSpectraForSample - 1)
                    averageTs = averageTs / (numSpectraForSample - 1)
                    averageQ = averageQ / (numSpectraForSample - 1)
                    VeraxStdev = numpy.std(values)
                    
                    old_unit_name = unit_name
                    
                    if resultname == 'API Gravity' or resultname == 'Relative Density':
                        old_unit_name = unit_name
                        unit_name = 'None'
                        
                    if resultname == 'Dry BTU':
                        old_unit_name = unit_name
                        unit_name = 'BTU/cf'
                           
                    if resultname == 'VPCR4':
                        old_unit_name = unit_name
                        unit_name = 'psi'
                    
                    conn.execute("SELECT value from sampledb.sample_results where result_name = '" + str(resultname) + "' and unit_name = '" + str(unit_name) + "' and sample_id = " + str(sample_id))
                    
                    unit_name = old_unit_name
                    
                    maxxamvalue = conn.fetchall()
                    
                    if maxxamvalue == ():
                        maxxamvalue = 'No Maxxam Result'
                    else:
                        maxxamvalue = maxxamvalue[0][0]
                        
                    delta = 0
                    
                    if maxxamvalue != 'No Maxxam Result':
                        delta = averageValue - maxxamvalue
                    else:
                        delta = 'No Value'
                    
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).value = resultname
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).font = font
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).value = averageValue
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).value = VeraxStdev
                    summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).value = maxxamvalue
                    summarySheet.cell(row = curRowForResults, column = curcolumnForTs).value = averageTs
                    summarySheet.cell(row = curRowForResults, column = curcolumnForQ).value = averageQ
                    summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).value = delta
                    summarySheet.cell(row = curRowForResults, column = curcolumnForresultNames).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxValue).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForVeraxStdev).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForMaxxamValue).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForTs).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForQ).number_format = '0.000'
                    summarySheet.cell(row = curRowForResults, column = curcolumnForDelta).number_format = '0.000'                
                    
                    if resultname != 'API Gravity' and resultname != 'Relative Density' and summarySheet.cell(row = curRowForResults, column = curcolumnForDelta - 1).value != 'No Maxxam Result':
                        summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='greaterThan', formula=['0.5'], stopIfTrue=True, fill=redFill))
                    
                        summarySheet.conditional_formatting.add((get_column_letter(curcolumnForDelta)+str(curRowForResults)), CellIsRule(operator='lessThan', formula=['-0.5'], stopIfTrue=True, fill=redFill))              
                    
                    
                    curRowForResults += 1
                    break
                    
                curcol += 1
                curValue = sh.cell(row = samplerow, column = curcol).value
                
                
            curcol = 4
            curValue = sh.cell(row = samplerow, column = curcol).value
            orderCounter += 1
        
                  
        samplerow += (numSpectraForSample - 1)
        curcell = sh.cell(row = samplerow, column = 1).value
        curcolumnForSampleDate += 8
        curcolumnForVeraxValue += 8  
        curcolumnForVeraxStdev += 8
        curcolumnForMaxxamValue += 8
        curcolumnFortempAndPres += 8
        curcolumnForresultNames += 8
        curcolumnForHeaderStart += 8
        curcolumnForDelta += 8
        curcolumnForAnalysisPoint += 8
        curcolumnForTs += 8
        curcolumnForQ += 8
        curRowForResults = 5    
        
    wb.save('c:\\Users\\agloyna\\Documents\\pythondatebasescripts\\ModelValidationSummaryOutput.xlsx')
    
    db.close()
    
    os.system('start excel.exe "c:\\Users\\agloyna\\Documents\\pythondatebasescripts\\ModelValidationSummaryOutput.xlsx"') 