import shutil
from openpyxl import Workbook
from openpyxl import load_workbook
import csv
import datetime
from openpyxl.cell import get_column_letter
import re
import os

# This code takes the paths of a csv EDD file and its associated pdf file. 
#Reformats the csv into an excel workbook and moves both the newly created excel 
#workbook and pdf file into a new subdirectory named according to the contents 
#of the EDD.

def formatReportFilesWithZip(eddpath, zippath, analysislab):          
     dateset = set()
     sitedict = {}
     foldername = ''
     
     wb = Workbook()
     dest_path = r"O:\Sample Cylinders\Incoming Lab Reports\Automatic_Report_Filing"
     
     ws = wb.worksheets[0]
     #open csv and copy all non-null cells to a newly created workbook
     with open(eddpath, 'rb') as csvfile:
          reader = csv.reader(csvfile, delimiter=',')
               
          for row_index, row in enumerate(reader):
               for column_index, cell in enumerate(row):
                    if analysislab == 'Maxxam Edmonton':
                         cell = cell.replace('Volume Fraction', '[Volume Fraction]')
                         cell = cell.replace('Liq Volume', '[Liq Volume]')
                         if cell.find('Mol Fraction2') >=0:
                              cell = cell.replace('Mol Fraction2', '[Mol Fraction2]')
                         elif cell.find('Mol Fraction') >= 0 and cell.find('Mol Fraction2') < 0:
                              cell = cell.replace('Mol Fraction', '[Mol Fraction]')
                         cell = cell.replace('Mass Fraction', '[Mass Fraction]')
                         cell = cell.replace('Sample Mol Mass', 'Sample Mol Mass [g/mol]')
                         cell = cell.replace('PPM', '[PPM]')
                         cell = cell.replace('Vapor Pressure', 'Vapor Pressure [kPa]')                         
                    column_letter = get_column_letter((column_index + 1))
                    ws.cell('%s%s'%(column_letter, (row_index + 1))).value = cell
     
     #create a dictionary with keys corresponding to names of sites
     #with value pairs corresponding to a set containing all the site's 
     #distinct analysis
     #points which have sample results reported in the workbook
     currow = 2
     curcell = ws.cell(row = currow, column = 2).value
     
     while curcell != None:
          curanalysispoint = ws.cell('T' + str(currow)).value
          if sitedict.has_key(curcell) == False:
               sitedict[curcell] = set()
               sitedict[curcell].add(curanalysispoint.strip())
          else:
               sitedict[curcell].add(curanalysispoint.strip())
          currow += 1
          curcell = ws.cell(row = currow, column = 2).value
     #create a set with all unique date values corresponding to the samples
     #found in the workbook
     currow = 2
     curcell = ws.cell('O' + str(currow)).value
     
     while curcell != None:
          datesplit = curcell.split()
          timestamp = datetime.date(int(datesplit[0]), int(datesplit[1]), int(datesplit[2]))
          dateset.add(timestamp)
          currow += 1
          curcell = ws.cell('O' + str(currow)).value
     #add all the unique date values to the name of the folder where the workbook
     #and pdf file will be saved
     curdate = 0 
     for item in dateset:
          oldcurdate = curdate
          curdate = item.year
          if curdate != oldcurdate:
               fmt = '%Y-%m-%d'
          else:
               fmt = '%m-%d'
          item = item.strftime(fmt)
          foldername = foldername + str(item) + "_"
          
     #add the site name with the unique analysis points for each site
     #to the name of the folder
     for item in sitedict:
          foldername = foldername + re.sub(' ',"_",str(item)) + '_'
          templst = list(sitedict[item])
          for i in range(len(templst)):
               foldername = foldername + str(templst[i])
               if i != len(templst) - 1:
                    foldername = foldername + ','
                    
     #add the phase code from the sample report to the end of the folder name               
     foldername = foldername + '_' + ws.cell('I2').value
     fullpath = dest_path + "\\" + foldername
     
     #create the subdirectory if it doesn't already exist
     if not os.path.exists(fullpath):
          os.makedirs(fullpath)
     
     zipfilepatharray = zippath.split("/")
     zipfilename = zipfilepatharray[len(zipfilepatharray)-1]
     
     curtime = datetime.datetime.strftime(datetime.datetime.now(), '%H-%M')
          
     #save the workbook to this location and rename and move the pdf to this
     #same location
     wb.save(filename = fullpath + '\\' + 'edd' + '_' + curtime +  '.xlsx')
     if zippath != 'None':
          shutil.move(zippath, fullpath +'\\' + 'pdf' + '_' + curtime + '.zip')
          return (fullpath + '\\' + 'edd' + '_' + curtime +  '.xlsx',  fullpath +'\\' + 'pdf' + '_' + curtime + '.zip')
     
     else:
          return (fullpath + '\\' + 'edd' + '_' + curtime +   '.xlsx', 'None')
     