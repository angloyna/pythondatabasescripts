import MySQLdb

starttime = '2015-06-22 03:42:22'
endtime = '2015-06-22 13:42:22'

db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()

localdbDict = {}

conn.execute("SELECT sites.name, sites.localdb from sampledb.sites")

rows = conn.fetchall()

for row in rows:
    localdbDict[row[0]] = row[1]
    
db.close()

f = open('CopyMachineCheckReport', 'w')

for key in localdbDict:
    localdb = localdbDict[key]
    if localdb != None and localdb != "":
        db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", localdb)
        conn = db.cursor()
        
        conn.execute("SELECT distinct danalysis, max(dalarm) from tdata where ddatestamp between %s and %s group by danalysis", (starttime, endtime))
        
        print key
        print localdb
        f.write('\n' + key + '\t' + localdb + '\n')
        
        results = conn.fetchall()
        for result in results:
            maxAlarmDecimal = result[1]
            alarmMessage = ""
            maxalarm = None
            if maxAlarmDecimal & 8192 == 8192:
                alarmMessage = "Fault - configuration"
                maxalarm = 8192
            elif maxAlarmDecimal & 4096 == 4096:
                alarmMessage = "Fault - multiplexer"
                maxalarm = 4096
            elif maxAlarmDecimal & 2048 == 2048:
                alarmMessage = "Fault - spectrometer"
                maxalarm = 2048
            elif maxAlarmDecimal & 1024 == 1024:
                alarmMessage = "Fault - solo_predictor"
                maxalarm = 1024
            elif maxAlarmDecimal & 512 == 512:
                alarmMessage = "Fault - transmission low"
                maxalarm = 512
            elif maxAlarmDecimal & 256 == 256:
                alarmMessage = "Fault - absorption high"
                maxalarm = 256
            elif maxAlarmDecimal & 128 == 128:
                alarmMessage = "Fault - high T2, Q"
                maxalarm = 128
            elif maxAlarmDecimal & 64 == 64:
                alarmMessage = "Warning - MySQL"
                maxalarm = 64
            elif maxAlarmDecimal & 32 == 32:
                alarmMessage = "Warning - modbus"
                maxalarm = 32
            elif maxAlarmDecimal & 16 == 16:            
                alarmMessage = "Warning - transmission low"
                maxalarm = 16
            elif maxAlarmDecimal & 8 == 8:      
                alarmMessage = "Warning - high T2, Q"
                maxalarm = 8
            elif maxAlarmDecimal & 4 == 4:
                alarmMessage = "Warning - yhat out of range"
                maxalarm = 4
            elif maxAlarmDecimal & 2 == 2:              
                alarmMessage = "Warning -  P out of range"
                maxalarm = 2
            elif maxAlarmDecimal & 1 == 1:
                alarmMessage = "Warning - T out of range"
                maxalarm = 1
            else:
                alarmMessage = "No alarm"
                maxalarm = 0
            
            if maxalarm != 0:
                conn.execute("SELECT count(did) from tdata where (dalarm & %s = %s) and ddatestamp between %s and %s and danalysis = %s", (maxalarm, maxalarm, starttime, endtime, result[0]))
                numalarms = conn.fetchall()[0][0]
                conn.execute("SELECT count(did) from tdata where ddatestamp between %s and %s and danalysis = %s", (starttime, endtime, result[0]))
                totalrecords = conn.fetchall()[0][0]
                percentThrown = (float(numalarms) / float(totalrecords)) * 100
                f.write("\tanalysis channel: " + str(result[0]) + "\n\t\tmax alarm: " + str(result[1]) + "\n\t\talarm message: " + str(alarmMessage) + "\n\t\tpercent thrown: " + str(percentThrown) + "\n")
                 
                 
    
                   
        db.close()
        
        
        
        
for key in localdbDict:
    localdb = localdbDict[key]
    if localdb != None and localdb != "" and localdb.lower() != 'p1047' and localdb.lower() != 'p1048' and localdb.lower() != 'p1045':
        db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", localdb)
        conn = db.cursor()
        
        print localdb
        conn.execute("SELECT distinct ranalysis, avg(rT2), min(rT2), max(rT2), avg(rQ), min(rQ), max(rQ), avg(dtemp), avg(dpres) from tresults join tdata on tresults.rdatestamp = tdata.ddatestamp where rdatestamp between %s and %s and rT2 <> -1 group by ranalysis", (starttime, endtime))
        
        tandqresults = conn.fetchall()
        f.write('\n' + key + '\t' + localdb + '\n')
        
        for tandq in tandqresults:
            if tandq[6] > 100 and tandq[8] > 10:
                f.write("\tanalysis channel: " + str(tandq[0]) + "\n\t\taverage temp: " + str(tandq[7]) + "\n\t\taverage pres: " + str(tandq[8]) +  "\n\t\taverage t2: " + str(tandq[1]) + "\n\t\tmin t2: " + str(tandq[2]) + "\n\t\tmax t2: " + str(tandq[3]) + "\n\t\taverage q: " + str(tandq[4]) + "\n\t\tmin q: " + str(tandq[5]) + "\n\t\tmax q: " +str(tandq[6]) + "\n")
            
        db.close()

f.close()