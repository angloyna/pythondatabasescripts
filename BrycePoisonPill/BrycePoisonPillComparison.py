import openpyxl
from openpyxl import Workbook
from openpyxl.cell import get_column_letter
from openpyxl.styles.borders import Border, Side
import MySQLdb
import os
from Tkinter import *

def go(phase, units, firstBarcode, secondBarcode, t):
    t.destroy()
    generateBryceReport(phase, units, firstBarcode, secondBarcode)

def BrycePoisonPillApp():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Poison Pill Comparison')
    labelframe = LabelFrame(top, text="Poison Pill Information Entry")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    barcodeOneFrame = Frame(labelframe, height=2, bd=1)
    barcodeOneFrame.pack(padx=5, pady=5, side = TOP)
    
    barcodeOnelabel = Label(barcodeOneFrame, text = 'first barcode: ', justify = LEFT, width = 15)
    barcodeOnelabel.pack(side = LEFT)
    barcodeOne = Entry(barcodeOneFrame, width = 10)
    barcodeOne.pack(side = LEFT)        
    
    barcodeTwoFrame = Frame(labelframe, height=2, bd=1)
    barcodeTwoFrame.pack(padx=5, pady=5, side = TOP)
    
    barcodeTwolabel = Label(barcodeTwoFrame, text = 'second barcode: ', justify = LEFT, width = 15)
    barcodeTwolabel.pack(side = LEFT)
    barcodeTwo = Entry(barcodeTwoFrame, width = 10)
    barcodeTwo.pack(side = LEFT)    
        
    phaseFrame = Frame(labelframe, height=2, bd=1)
    phaseFrame.pack(padx=5, pady=5, side = TOP)    
    phaselabel = Label(phaseFrame, text = 'Phase Of Sample: ', justify = RIGHT, width = 20)
    phaselabel.pack(side = LEFT)    
    
        
    phaseOptions = ['Gas', 'Liquid', 'NGL']
        
    phaseVar = StringVar(phaseFrame)
    phaseVar.set(phaseOptions[0])
    phaseOptions = apply(OptionMenu, (phaseFrame, phaseVar) + tuple(phaseOptions))
    phaseOptions.pack(side = LEFT)    
              
    unitsFrame = Frame(labelframe, height=2, bd=1)
    unitsFrame.pack(padx=5, pady=5, side = TOP)    
    unitslabel = Label(unitsFrame, text = 'Units: ', justify = RIGHT, width = 13)
    unitslabel.pack(side = LEFT)
        
    unitsOptions = ['mol %', 'wt %', 'lv %']
        
    unitsVar = StringVar(unitsFrame)
    unitsVar.set(unitsOptions[0])
    unitsOptions = apply(OptionMenu, (unitsFrame, unitsVar) + tuple(unitsOptions))
    unitsOptions.pack(side = LEFT)    
    
    gobutton = Button(labelframe, width = 10, text = 'Go', command= lambda: go(phaseVar.get(), unitsVar.get(), barcodeOne.get(), barcodeTwo.get(), top))
    gobutton.pack(side = BOTTOM, padx = 5, pady = 5)
        
    top.mainloop()     


def generateBryceReport(phase, units, firstBarcode, secondBarcode):
 
    speciationUnitName = units
    #options for phase requested where "Liquid," "NGL," "Gas"
    
    if phase == 'Gas':
        speciationResults = ['c1', 'c2', 'c3']
        physicalProperties = ['Dry BTU', 'Relative Density', 'Absolute Density']
    if phase == 'NGL':
        speciationResults = ['c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'c7+']
        physicalProperties = ['API Gravity', 'Relative Density']
    if phase == 'Liquid':
        speciationResults = ['ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'c7+']
        physicalProperties = ['API Gravity', 'Relative Density']    
        
    full_box_border = Border(top=Side(style='thin'),bottom=Side(style='thin'),left=Side(style='thin'),right=Side(style='thin'))    
    barcodeRow = 2
    pressureRow = 4
    speciationResultsStartRow = 7
    physicalPropertiesStartRow = speciationResultsStartRow + len(speciationResults)
    
    barcodeHeaderColumn = 1
    pressureHeaderColumn = 1
    resultNameColumn = 1
    deltaColumn = 4
    absoluteDeltaColumn = 5
    percentDifferenceColumn = 6
    
    firstSampleColumn = 2
    secondSampleColumn = 3
    
       
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    
    conn.execute("SELECT sample_id, pres from sampledb.samples where barcode = %s", (firstBarcode))
    
    results = conn.fetchone()
    
    firstSampleID = results[0]
    firstPressure = results[1]
    
    conn.execute("SELECT sample_id, pres from sampledb.samples where barcode = %s", (secondBarcode))
    
    results = conn.fetchone()
    
    secondSampleID = results[0]
    secondPressure = results[1]
    
    wb = Workbook()
    ws = wb.active
    
    ws.cell(row = barcodeRow, column = barcodeHeaderColumn).value = 'Barcode/Client ID'
    ws.cell(row = barcodeRow, column = firstSampleColumn).value = firstBarcode
    ws.cell(row = barcodeRow, column = secondSampleColumn).value = secondBarcode
    ws.cell(row = barcodeRow, column = barcodeHeaderColumn).border = full_box_border
    ws.cell(row = barcodeRow, column = firstSampleColumn).border = full_box_border
    ws.cell(row = barcodeRow, column = secondSampleColumn).border = full_box_border
    
    ws.cell(row = pressureRow, column = pressureHeaderColumn).value = 'Source Pressure [psig]'
    ws.cell(row = pressureRow, column = firstSampleColumn).value = firstPressure
    ws.cell(row = pressureRow, column = secondSampleColumn).value = secondPressure
    ws.cell(row = pressureRow, column = pressureHeaderColumn).border = full_box_border
    ws.cell(row = pressureRow, column = firstSampleColumn).border = full_box_border
    ws.cell(row = pressureRow, column = secondSampleColumn).border = full_box_border   
    
    
    ws.cell(row = speciationResultsStartRow - 1, column = resultNameColumn).value = 'Result'
    ws.cell(row = speciationResultsStartRow - 1, column = firstSampleColumn).value = 'Sample ' + str(firstBarcode)
    ws.cell(row = speciationResultsStartRow - 1, column = secondSampleColumn).value = 'Sample ' + str(secondBarcode)
    ws.cell(row = speciationResultsStartRow - 1, column = deltaColumn).value = 'Delta'
    ws.cell(row = speciationResultsStartRow - 1, column = absoluteDeltaColumn).value = 'Absolute Delta'
    ws.cell(row = speciationResultsStartRow - 1, column = percentDifferenceColumn).value = 'Percent Difference'
    ws.cell(row = speciationResultsStartRow - 1, column = resultNameColumn).border = full_box_border
    ws.cell(row = speciationResultsStartRow - 1, column = firstSampleColumn).border = full_box_border
    ws.cell(row = speciationResultsStartRow - 1, column = secondSampleColumn).border = full_box_border
    ws.cell(row = speciationResultsStartRow - 1, column = deltaColumn).border = full_box_border
    ws.cell(row = speciationResultsStartRow - 1, column = absoluteDeltaColumn).border = full_box_border
    ws.cell(row = speciationResultsStartRow - 1, column = percentDifferenceColumn).border = full_box_border   
    
        
    curRow = speciationResultsStartRow

    
    for result in speciationResults:
        conn.execute("SELECT value from sampledb.sample_results where sample_id = %s and result_name = %s and unit_name = %s", (firstSampleID, result, speciationUnitName))
        results = conn.fetchone()
        if results != None:
            firstSampleResult = results[0]
        else:
            firstSampleResult = 'No result'
                    
        conn.execute("SELECT value from sampledb.sample_results where sample_id = %s and result_name = %s and unit_name = %s", (secondSampleID, result, speciationUnitName))
        
        results = conn.fetchone()
        if results != None:
            secondSampleResult = results[0]
        else:
            secondSampleResult = 'No result'
        
        ws.cell(row = curRow, column = resultNameColumn).value = result + ' [' + speciationUnitName + ']'
        ws.cell(row = curRow, column = firstSampleColumn).value = firstSampleResult
        ws.cell(row = curRow, column = secondSampleColumn).value = secondSampleResult
        if firstSampleResult != 'No result' and secondSampleResult != 'No result':
            ws.cell(row = curRow, column = deltaColumn).value = firstSampleResult - secondSampleResult
            ws.cell(row = curRow, column = absoluteDeltaColumn).value = abs(firstSampleResult - secondSampleResult)
            ws.cell(row = curRow, column = percentDifferenceColumn).value = (abs(firstSampleResult - secondSampleResult)/((firstSampleResult + secondSampleResult)/2)) * 100
        ws.cell(row = curRow, column = firstSampleColumn).number_format = '0.000'
        ws.cell(row = curRow, column = secondSampleColumn).number_format = '0.000'
        ws.cell(row = curRow, column = deltaColumn).number_format = '0.000'
        ws.cell(row = curRow, column = absoluteDeltaColumn).number_format = '0.000'
        ws.cell(row = curRow, column = percentDifferenceColumn).number_format = '0.000'      
        
        ws.cell(row = curRow, column = resultNameColumn).border = full_box_border
        ws.cell(row = curRow, column = firstSampleColumn).border = full_box_border
        ws.cell(row = curRow, column = secondSampleColumn).border = full_box_border
        ws.cell(row = curRow, column = deltaColumn).border = full_box_border
        ws.cell(row = curRow, column = absoluteDeltaColumn).border = full_box_border 
        ws.cell(row = curRow, column = percentDifferenceColumn).border = full_box_border         
        
        curRow += 1
        
    
    curRow = physicalPropertiesStartRow
    
    for result in physicalProperties:
        if result in ['API Gravity', 'Relative Density', 'Absolute Density']:
            unitName = 'None'
        elif result in ['Dry BTU']:
            unitName = 'btu/cf'
        else:
            print "could not find units for " + result
            break
                       
        conn.execute("SELECT value from sampledb.sample_results where sample_id = %s and result_name = %s and unit_name = %s", (firstSampleID, result, unitName))
        results = conn.fetchone()

        if results != None:           
            firstSampleResult = results[0]
        else:
            firstSampleResult = 'No result'
            
        
        conn.execute("SELECT value from sampledb.sample_results where sample_id = %s and result_name = %s and unit_name = %s", (secondSampleID, result, unitName))
        results = conn.fetchone()
        if results != None:
            secondSampleResult = results[0]
        else:
            secondSampleResult = 'No result'
        
        ws.cell(row = curRow, column = resultNameColumn).value = result + ' [' + unitName + ']'
        ws.cell(row = curRow, column = firstSampleColumn).value = firstSampleResult
        ws.cell(row = curRow, column = secondSampleColumn).value = secondSampleResult
        ws.cell(row = curRow, column = firstSampleColumn).number_format = '0.000'
        ws.cell(row = curRow, column = secondSampleColumn).number_format = '0.000'       
        if firstSampleResult != 'No result' and secondSampleResult != 'No result':
            ws.cell(row = curRow, column = deltaColumn).value = firstSampleResult - secondSampleResult
            ws.cell(row = curRow, column = absoluteDeltaColumn).value = abs(firstSampleResult - secondSampleResult)
            ws.cell(row = curRow, column = percentDifferenceColumn).value = (abs(firstSampleResult - secondSampleResult)/((firstSampleResult + secondSampleResult)/2)) * 100
            ws.cell(row = curRow, column = deltaColumn).number_format = '0.000'
            ws.cell(row = curRow, column = absoluteDeltaColumn).number_format = '0.000'
            ws.cell(row = curRow, column = percentDifferenceColumn).number_format = '0.000' 
            
            
        ws.cell(row = curRow, column = resultNameColumn).border = full_box_border
        ws.cell(row = curRow, column = firstSampleColumn).border = full_box_border
        ws.cell(row = curRow, column = secondSampleColumn).border = full_box_border            
        ws.cell(row = curRow, column = deltaColumn).border = full_box_border
        ws.cell(row = curRow, column = absoluteDeltaColumn).border = full_box_border
        ws.cell(row = curRow, column = percentDifferenceColumn).border = full_box_border         
        
               
        curRow += 1        
              
    db.close()
    
    ws.column_dimensions[get_column_letter(resultNameColumn)].width = 25
    ws.column_dimensions[get_column_letter(deltaColumn)].width = 25
    ws.column_dimensions[get_column_letter(absoluteDeltaColumn)].width = 25
    ws.column_dimensions[get_column_letter(percentDifferenceColumn)].width = 25 
    
    wb.save('BrycePoisonPull_' + str(firstBarcode) + '_' + str(secondBarcode) + '.xlsx')
            
    os.system('start excel.exe ' + '"' + 'BrycePoisonPull_' + str(firstBarcode) + '_' + str(secondBarcode) + '.xlsx' + '"')    
    

        
BrycePoisonPillApp()    