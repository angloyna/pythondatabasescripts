import openpyxl
from openpyxl import load_workbook
import MySQLdb

import datetime



excelfilename = 'C:/Users/agloyna/Desktop/Copy of Sample Log Tracker_revB_080815.xlsx'
sheetindex = 0
process_status = "Sampled"
val_bool = "Y"

db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()

wb = load_workbook(filename =  excelfilename)

sh = wb.worksheets[sheetindex]

curRow = 2
curcell = sh.cell(row = curRow, column = 2).value
while curcell != None:
    sitename = sh.cell(row = curRow, column = 2).value
    analysisNumber = sh.cell(row = curRow, column = 3).value[1:]
    date = sh.cell(row = curRow, column = 4).value
    starttime = sh.cell(row = curRow, column = 5).value
    endtime = sh.cell(row = curRow, column = 6).value
    cylinderno = sh.cell(row = curRow, column = 7).value
    barcode = sh.cell(row = curRow, column = 8).value
    temp = sh.cell(row = curRow, column = 9).value
    pres = sh.cell(row = curRow, column = 10).value
    phase = sh.cell(row = curRow, column = 12).value
    method = sh.cell(row = curRow, column = 13).value
    sampler_id = 15
    
    
    startTS = datetime.datetime.combine(date.date(), starttime)
    if phase != "Gas":
        endTS = datetime.datetime.combine(date.date(), endtime)
    else:
        endTS = startTS

    
    conn.execute("SELECT analysis_points.id from sampledb.analysis_points join sampledb.sites on analysis_points.site_id = sites.id where sites.name = %s and analysis_number = %s", (sitename, analysisNumber))
    
    analysis_id = conn.fetchone()[0]
    print analysis_id
    conn.execute("SELECT id from sampledb.methods where name = %s", (method))
    
    method_id = conn.fetchone()[0]
    print method_id
    
    conn.execute("INSERT INTO sampledb.samples(barcode, analysis_id, starttime, endtime, process_status, phase, temp, pres, sampler_id, valid_bool, lab_id, container_type_id, cylinder_no, method_id) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (barcode, analysis_id, startTS, endTS, process_status, phase, temp, pres, sampler_id, val_bool, 5, 2, cylinderno, method_id))
    
    
    curRow += 1
    curcell = sh.cell(row = curRow, column = 2).value
    
db.commit()
db.close()
    