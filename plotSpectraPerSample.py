import matplotlib.pyplot as plt
import MySQLdb


def getXvaluesArray(min,max):
    xValues = []
    i = min
        
    while i <= max:
        xValues.append(i)
        i += .5        
    
    return xValues

def getSpectraPerSample(sampleids):
    spectraDict = {}
    
    for sampleid in sampleids:
            
        db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
        conn = db.cursor()
        
        conn.execute("select spectrum from sampledb.spectra where sample_id = %s", (sampleid))
        
        results = conn.fetchall()
        
        db.close()
        
        yValues = []
        
        for result in results:
            yValueSet = result[0].split(",")
            yValueSet = [float(x) for x in yValueSet]
            yValues.append(yValueSet)
        spectraDict[sampleid] = yValues    
    
    return spectraDict


def plotSpectraPerSample(sampleids):
    
    xValues = getXvaluesArray(1355,1795)
   
    spectraDict = getSpectraPerSample(sampleids)
    
    
    f, axarr = plt.subplots(len(spectraDict), sharex=True)
    subPlotindex = 0
    
    for key in spectraDict:
        print key, len(spectraDict[key])
        for yValue in spectraDict[key]:
            axarr[subPlotindex].plot(xValues, yValue)
        axarr[subPlotindex].set_title(key)
        subPlotindex += 1

    
    
    
plotSpectraPerSample([6580,6581])
plotSpectraPerSample([6489,6564,6565,6566,6625])