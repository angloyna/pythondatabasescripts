import MySQLdb


def writeSpectralInformationToFile(timeFile, spectraFile, barcodes):

    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    conn = db.cursor()
    for barcode in barcodes:
        conn.execute("SELECT time, spectra.temp, spectra.pres, spectrum from sampledb.spectra join sampledb.samples on spectra.sample_id = samples.sample_id where barcode = %s", barcode)
        results = conn.fetchall()
        for result in results:
            timeFile.write(str(result[0]) + "\t" + str(result[1]) + "\t" + str(result[2]) + "\n")
            spectrum = result[3].split(",")
            for i in range (0,len(spectrum)):
                if i < (len(spectrum)-1):
                    spectraFile.write(str(spectrum[i]) + "\t")
                else:
                    spectraFile.write(str(spectrum[i]) + "\n")    

def generateSampleIDsqlString(barcodes):
    
    barcodestr = ''
    for k in range(0, len(barcodes)):
        if k < (len(barcodes)-1):
            barcodestr += str(barcodes[k]) + ","
        else:
            barcodestr += str(barcodes[k])    
    
    return barcodestr

def main():

    directoryPath = r"C:\Users\agloyna\Desktop\validationreportMaverick"
    barcodes = [2889,2899,2900,2901,2902]

    
    tempAndPresFile = open(directoryPath + '\T&P.txt', 'w')
    SpectraFile = open(directoryPath + '\spectra.txt', 'w')
       
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "sampledb")
    
    conn = db.cursor()
    
    barcodestr = generateSampleIDsqlString(barcodes)

    conn.execute("SELECT count(id) from sampledb.spectra join sampledb.samples on spectra.sample_id = samples.sample_id where barcode in (" + barcodestr + ")")
    numberSpectra = conn.fetchone()[0]
    
    db.close()
    
    tempAndPresFile.write(str(numberSpectra) + "\n")
    tempAndPresFile.write("Datestamp\tTemperature\tPressure\n")

    writeSpectralInformationToFile(tempAndPresFile, SpectraFile, barcodes)
    
    tempAndPresFile.close()
    SpectraFile.close()
    
main()