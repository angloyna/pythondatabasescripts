import MySQLdb
import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter
import numpy
import datetime
import os
import sys
import numpy
from openpyxl.charts import ScatterChart, LineChart, Reference, Series


OldModelfilename = 'C:/Users/agloyna/Desktop/2015_08_12_MoundsvilleOldModelPrediction.xlsx'
NewModelfilename = 'C:/Users/agloyna/Desktop/2015_08_12_MoundsvilleModelPrediction.xlsx'

oldModelwb = load_workbook(filename = OldModelfilename)
oldModelws = oldModelwb.worksheets[0]

diffWs = oldModelwb.create_sheet(title="Difference Values")


NewModelwb = load_workbook(filename = NewModelfilename)
OriginalNewModelws = NewModelwb.worksheets[0]


NewModelws = oldModelwb.create_sheet(title="New Model Prediction")

newModelcolumnMap = {}
oldModelcolumnMap = {}

resultStartCol = 4
OldModelValueCount = 0
NewModelValueCount = 0


copyRow = 1
copyColumn = 1
curcell = OriginalNewModelws.cell(row = copyRow, column = copyColumn).value

while curcell != None:
    while curcell != None:
        NewModelws.cell(row = copyRow, column = copyColumn).value = OriginalNewModelws.cell(row = copyRow, column = copyColumn).value
        copyRow += 1
        curcell = OriginalNewModelws.cell(row = copyRow, column = copyColumn).value
    copyRow = 1
    copyColumn += 1
    curcell = OriginalNewModelws.cell(row = copyRow, column = copyColumn).value




#get the result name column map of the OldModel Workbook

curcol = resultStartCol
curRow = 1

curcell = oldModelws.cell(row = curRow, column = curcol).value

while curcell != None:
    if curcell.find('T2_') == -1 and curcell.find('Q_') == -1:
        oldModelcolumnMap[curcell.lower()] = curcol
    curcol += 1
    curcell = oldModelws.cell(row = curRow, column = curcol).value


#get the result name column map of the NewModel Workbook

curcol = resultStartCol
curRow = 1

curcell = NewModelws.cell(row = curRow, column = curcol).value

while curcell != None:
    if curcell.find('T2_') == -1 and curcell.find('Q_') == -1:
        newModelcolumnMap[curcell.lower()] = curcol
    curcol += 1
    curcell = NewModelws.cell(row = curRow, column = curcol).value


#find out how many Old Model results there are

curcol = 1
curRow = 2

curcell = oldModelws.cell(row = curRow, column = curcol).value

while curcell != None:
    OldModelValueCount += 1
    curRow += 1
    curcell = oldModelws.cell(row = curRow, column = curcol).value
    


#find out how many New Model results there are

curcol = 1
curRow = 2

curcell = NewModelws.cell(row = curRow, column = curcol).value

while curcell != None:
    NewModelValueCount += 1
    curRow += 1
    curcell = NewModelws.cell(row = curRow, column = curcol).value

oldModelxValues = Reference(oldModelws, (2,1),(OldModelValueCount + 1,1))
newModelxValues = Reference(NewModelws, (2,1), (NewModelValueCount + 1, 1))


for i in range(2, OldModelValueCount+1):
    diffWs.cell(row = i, column = 1).value = oldModelws.cell(row = i, column = 1).value

firstcharttopValue = 25
firstchartleftValue = 40
secondcharttopValue = 25
secondchartleftValue = 900
thirdcharttopValue = 25
thirdchartleftValue = 1800

curDiffRow = 2
curDiffCol = 2



for key in oldModelcolumnMap:
    yvalues = Reference(oldModelws, (2,oldModelcolumnMap[key]), (OldModelValueCount + 1, oldModelcolumnMap[key]))
    series = Series(yvalues, title = str(key) + " old model", xvalues = oldModelxValues)
    chart = ScatterChart()
    chart.append(series)
    yvalues = Reference(oldModelws, (2,oldModelcolumnMap[key] + 1), (OldModelValueCount + 1, oldModelcolumnMap[key] + 1))
    series = Series(yvalues, title = str(key) + " T2 old model", xvalues = oldModelxValues)  
    chart.append(series)
    yvalues = Reference(oldModelws, (2,oldModelcolumnMap[key] + 2), (OldModelValueCount + 1, oldModelcolumnMap[key] + 2))
    series = Series(yvalues, title = str(key) + " Q old model", xvalues = oldModelxValues)
    
    chart.append(series)
    chart.drawing.top = firstcharttopValue
    chart.drawing.left = firstchartleftValue
    oldModelws.add_chart(chart)
    firstcharttopValue += 420


    if newModelcolumnMap.has_key(key):
        yvalues = Reference(NewModelws, (2,newModelcolumnMap[key]), (NewModelValueCount + 1, newModelcolumnMap[key]))
        series = Series(yvalues, title = str(key) + " new model", xvalues = newModelxValues)
        chart = ScatterChart()
        chart.append(series)
        yvalues = Reference(NewModelws, (2,newModelcolumnMap[key] + 1), (NewModelValueCount + 1, newModelcolumnMap[key] + 1))
        series = Series(yvalues, title = str(key) + " T2 new model", xvalues = newModelxValues)  
        chart.append(series)
        yvalues = Reference(NewModelws, (2,newModelcolumnMap[key] + 2), ((NewModelValueCount + 1), (newModelcolumnMap[key] + 2)))
        series = Series(yvalues, title = str(key) + " Q new model", xvalues = newModelxValues)
        
        chart.append(series)
        chart.drawing.top = secondcharttopValue
        chart.drawing.left = secondchartleftValue
        oldModelws.add_chart(chart)
        
        
        
        #calculate the difference between the two model values
        
        result = key
        newModelValCol = newModelcolumnMap[key]
        oldModelValCol = oldModelcolumnMap[key]
        
        diffWs.cell(row = 1, column = curDiffCol).value = result

        
        
        for i in range(2, OldModelValueCount + 1):
            diffWs.cell(row = curDiffRow, column = curDiffCol).value = (float(oldModelws.cell(row = i, column = oldModelValCol).value) - float(NewModelws.cell(row = i, column = newModelValCol).value))
            curDiffRow += 1
        
        xvalues = Reference(diffWs, (2,1), (OldModelValueCount + 1, 1))
        yvalues = Reference(diffWs, (2,curDiffCol), (OldModelValueCount + 1, curDiffCol))
        series = Series(yvalues, title = str(result) + ' Difference', xvalues = xvalues)
        chart = ScatterChart()
        chart.append(series)        
        chart.drawing.top = thirdcharttopValue
        chart.drawing.left = thirdchartleftValue
        oldModelws.add_chart(chart)           
        
      
    curDiffCol += 1
    curDiffRow = 2
    thirdcharttopValue += 420
    secondcharttopValue += 420
    

    

oldModelwb.save("SampleChart.xlsx")
