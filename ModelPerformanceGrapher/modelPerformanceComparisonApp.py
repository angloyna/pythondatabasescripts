from modelPerformanceGrapherFunction import *
from Tkinter import *
import tkFileDialog


def browsebutton(entryobject,t):
    file = tkFileDialog.askopenfilename(parent=t,title='Choose a file')
    entryobject.insert(INSERT, file )
    
def go(oldModel, newModel, t):
    t.destroy()
    grapher(oldModel, newModel)
        
                
def grapherApp():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Model Performance Grapher')
    labelframe = LabelFrame(top, text="Model Prediction File Entry")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    eddFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    eddFrame.pack(fill=X, padx=5, pady=5)        
    eddlabel = Label(eddFrame, text = 'old Model file: ', justify = LEFT, width = 15)
    eddlabel.pack(side = LEFT)
    q = Entry(eddFrame, width = 70)
    q.pack(side = LEFT)
    eddbutton = Button(eddFrame, width = 10, text = 'Browse',command= lambda: browsebutton(q, top))
    eddbutton.pack(side = LEFT)
    
    zipFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    zipFrame.pack(fill=X, padx=5, pady=5)     
    ziplabel = Label(zipFrame, text = 'new Model file: ', justify = LEFT, width = 15)
    ziplabel.pack(side = LEFT)
    n = Entry(zipFrame, width = 70)
    n.pack(side = LEFT)
    zipbutton = Button(zipFrame, width = 10, text = 'Browse', command= lambda: browsebutton(n,top))
    zipbutton.pack(side = LEFT)
    
    
    gobutton = Button(labelframe, width = 10, text = 'Go', command= lambda: go(q.get(), n.get(), top))
    gobutton.pack(side = BOTTOM, padx = 5, pady = 5)
        
    top.mainloop()    
    
    
grapherApp()