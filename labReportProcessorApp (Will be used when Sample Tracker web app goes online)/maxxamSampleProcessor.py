from maxxamReader import *
from formatReportFiles import *
from Tkinter import *
import tkFileDialog


def browsebutton(entryobject,t):
    file = tkFileDialog.askopenfilename(parent=t,title='Choose a file')
    entryobject.insert(INSERT, file )
    
def go(eddpath, zippath, phase, analysistype, containertype, analysislab, method, t):
    t.destroy()
    if zippath == '':
        excelEDD = formatReportFilesWithZip(eddpath, 'None', analysislab)[0]
        pdfpath = 'None'
    else:
        returnTuple = formatReportFilesWithZip(eddpath, zippath, analysislab)
        excelEDD = returnTuple[0]
        pdfpath = returnTuple[1]
    maxxamReader(excelEDD, pdfpath, analysistype, phase, containertype, analysislab, method)
    

def processMaxxamReports():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Sample Report Processor')
    labelframe = LabelFrame(top, text="Sample Report File Entry")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    eddFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    eddFrame.pack(fill=X, padx=5, pady=5)        
    eddlabel = Label(eddFrame, text = 'edd file: ', justify = LEFT, width = 8)
    eddlabel.pack(side = LEFT)
    q = Entry(eddFrame, width = 70)
    q.pack(side = LEFT)
    eddbutton = Button(eddFrame, width = 10, text = 'Browse',command= lambda: browsebutton(q, top))
    eddbutton.pack(side = LEFT)
    
    zipFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    zipFrame.pack(fill=X, padx=5, pady=5)     
    ziplabel = Label(zipFrame, text = 'zip file: ', justify = LEFT, width = 8)
    ziplabel.pack(side = LEFT)
    n = Entry(zipFrame, width = 70)
    n.pack(side = LEFT)
    zipbutton = Button(zipFrame, width = 10, text = 'Browse', command= lambda: browsebutton(n,top))
    zipbutton.pack(side = LEFT)
      
    settingsFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    settingsFrame.pack(fill=X, padx=5, pady=5)
    phaseFrame = Frame(settingsFrame, height=2, bd=1)
    phaseFrame.pack(padx=5, pady=5, side = TOP)
    phaselabel = Label(phaseFrame, text = 'Phase: ', justify = RIGHT, width = 13)
    phaselabel.pack(side = LEFT)
    phaseVar = StringVar(phaseFrame)
    phaseVar.set("Gas")
    phaseoptions = OptionMenu(phaseFrame, phaseVar, "Gas", "Liquid")
    phaseoptions.pack(side = LEFT)    
    
    analysisFrame = Frame(settingsFrame, height=2, bd=1)
    analysisFrame.pack(padx=5, pady=5, side = TOP)    
    analysislabel = Label(analysisFrame, text = 'Analysis Type: ', justify = RIGHT, width = 13)
    analysislabel.pack(side = LEFT)
    
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()
    
    conn.execute("SELECT name from sampledb.analysis_types")
    
    temp = conn.fetchall()
    analysisOptions = []
    for item in temp:
        analysisOptions.append(item[0])
        
    analysisVar = StringVar(analysisFrame)
    analysisVar.set(analysisOptions[0])
    analysisoptions = apply(OptionMenu, (analysisFrame, analysisVar) + tuple(analysisOptions))
    analysisoptions.pack(side = LEFT)    
    
    containerFrame = Frame(settingsFrame, height=2, bd=1)
    containerFrame.pack(padx = 5, pady = 5, side = TOP)
    containerlabel = Label(containerFrame, text = 'Container Type: ', justify = RIGHT, width = 13)
    containerlabel.pack(side = LEFT)    
    
    conn.execute("SELECT name from sampledb.container_type")
        
    temp = conn.fetchall()
    containerOptions = []
    for item in temp:
        containerOptions.append(item[0])    
    

    
    containVar = StringVar(containerFrame)
    containVar.set(containerOptions[0])
    containeroptions = apply(OptionMenu, (containerFrame, containVar) + tuple(containerOptions))
    containeroptions.pack(side = LEFT)
    
    labOptions = ['Maxxam Houston', 'Maxxam Edmonton']
    
    labnameFrame = Frame(settingsFrame, height=2,bd=1)
    labnameFrame.pack(padx = 5, pady = 5, side = TOP)
    labnameLabel = Label(labnameFrame, text = 'Analysis Lab: ', justify = RIGHT, width = 13)
    labnameLabel.pack(side = LEFT)
    
    labVar = StringVar(labnameFrame)
    labVar.set(labOptions[0])
    laboptions = apply(OptionMenu, (labnameFrame, labVar) + tuple(labOptions))
    laboptions.pack(side = LEFT)
    
    conn.execute("SELECT name from sampledb.methods")
    
    temp = conn.fetchall()
    
    db.close()
    
    methodOptions = []
    
    for method in temp:
        methodOptions.append(method[0])
        
    
    methodnameFrame = Frame(settingsFrame, height=2,bd=1)
    methodnameFrame.pack(padx = 5, pady = 5, side = TOP)
    methodnameLabel = Label(methodnameFrame, text = 'Method Name: ', justify = RIGHT, width = 13)
    methodnameLabel.pack(side = LEFT)
    
    methodVar = StringVar(methodnameFrame)
    methodVar.set(methodOptions[0])
    methodoptions = apply(OptionMenu, (methodnameFrame, methodVar) + tuple(methodOptions))
    methodoptions.pack(side = LEFT) 

    gobutton = Button(labelframe, width = 10, text = 'Go', command= lambda: go(q.get(), n.get(), phaseVar.get(), analysisVar.get(), containVar.get(), labVar.get(), methodVar.get(), top))
    gobutton.pack(side = BOTTOM, padx = 5, pady = 5)
    
    top.mainloop()
    
    
processMaxxamReports()