import datetime
import MySQLdb
import openpyxl
from openpyxl import load_workbook
#import datetime
#from datetime import datetime
from maxxhoustonhelperfunctions import *

startResultCol = 16
phase = 'Liquid'
#templatebool indicates which template was used when sample results were entered into spreadsheet
templatebool = 3
#name of the excel file to be read
excelfilename = 'Placid_Port_Allen'
#index of the worksheet containing the results to be entered into DB
sheetindex = 0
#analysis type id is a field in the sample table indicating whether a sample has either speciation results, vapor pressure results, or both.
analysistypeid = 4
process_status = "Processed"
#sample is considered valid when it is first entered into the database
val_bool = "Y"

#0 is jp3 january 2nd sample template
#1 is jp3 sample template updated for maxxam edmonton samples

db = MySQLdb.connect("JP3M-DB01.jp3m.local", "WHoward", "Austin01", "sampledb")

conn = db.cursor()

repSpecMap = {}
nameToResultID = {}
dbResultNames = {}
dbUnitMap = {}

wb = load_workbook(filename = 'O:\Sample Cylinders\OfficialCopyLabReportsInExcelFormat\\' + excelfilename + '.xlsx')

sh = wb.worksheets[sheetindex]

curcol = startResultCol

curcell = sh.cell(row = 1, column = curcol).value
#link the column number to the names of the result and units from the template using a dictionary
while curcell != None:
    if curcell.find("[") >= 0 and curcell.find("[Total]") < 0:
        resultunitpair = curcell.split('[')
        spec = resultunitpair[0].strip()
        units = resultunitpair[1].split(']')[0].strip().lower()
        if units == 'wt %':
            units = 'wt. %'
        if units == 'btu/cf':
            units = 'btu/cf'            
    else:
        spec = curcell.strip()
        units = 'None'
    repSpecMap[curcol] = (spec, units)
    curcol += 1
    curcell = sh.cell(row = 1, column = curcol).value
    
#chooses sql statement to pull result names from the database based on the template
if templatebool == 1 and phase == 'Liquid':
    dictsql = "SELECT id, jp3_maxxam_liq from result_names;"
if templatebool == 1 and phase == 'Gas':
    dictsql = "SELECT id, jp3_maxxam_gas from result_names;"
if templatebool == 0 and phase == 'Liquid':
    dictsql = "SELECT id, jan_jp3_liquid from result_names;"
if templatebool == 0 and phase == 'Gas':
    dictsql = "SELECT id, jan_jp3_gas from result_names;"
if templatebool == 2 and phase == 'Liquid':
    dictsql = "SELECT id, spl_liquid from result_names;"
if templatebool == 2 and phase == 'Gas':
    dictsql = "SELECT id, spl_gas from result_names;"
if templatebool == 3 and phase == 'Liquid':
    dictsql = "SELECT id, name from result_names;"
     
     
conn.execute(dictsql)

rows = conn.fetchall()

#map result_names primary key to the result name
for row in rows:
    dbResultNames[row[1]] = row[0]
    
#map name of the species to the result_id
for item in repSpecMap:
    curspecies = repSpecMap[item][0]
    if dbResultNames.has_key(curspecies):
        nameToResultID[curspecies] = dbResultNames[curspecies]
    else:
        print "can't find", curspecies
               
conn.execute("SELECT id, spl_name FROM units")

units = conn.fetchall()

#map unit names to unit_id
for unit in units:
    dbUnitMap[unit[1].lower()] = unit[0]

samplerow = 2
curcell = sh.cell(row = samplerow, column = 1).value


#enter each sample into the database
while curcell != None:
    sitename = sh.cell(row = samplerow, column = 1).value
    
    starttime = datetime.datetime.combine(sh.cell(row = samplerow, column = 4).value,sh.cell(row = samplerow, column = 5).value)
    endtime = datetime.datetime.combine(sh.cell(row = samplerow, column = 6).value,sh.cell(row = samplerow, column = 7).value)
    analysisnumber = sh.cell(row = samplerow, column = 3).value
    pressure = sh.cell(row = samplerow, column = 9).value
    temp = sh.cell(row = samplerow, column = 10).value
    barcode = sh.cell(row = samplerow, column = 11).value
    lab = sh.cell(row = samplerow, column = 12).value
    method = sh.cell(row = samplerow, column = 13).value
    
    if str(barcode).lower().find('none') >= 0:
        barcode = None
    
    #retrieve primary keys corresponding to the above header information for the sample
    conn.execute("SELECT id FROM sites WHERE name = %s", (sitename,))
    siteid = conn.fetchall()[0][0]
    
    conn.execute("SELECT id FROM analysis_points WHERE analysis_number = %s AND site_id = %s",(analysisnumber, siteid))
    analysisid = conn.fetchall()[0][0]
    
    conn.execute("SELECT id FROM labs WHERE name = %s", (lab,))
    labid = conn.fetchall()[0][0]
    
    conn.execute("SELECT id from companies where name = 'JP3 Measurement'")
    samplerid = conn.fetchall()[0][0]
    
    conn.execute('SELECT id from sampledb.methods where name = %s', (method))
    methodid = conn.fetchone()[0]
    
    #insert header information into database
    conn.execute("INSERT INTO samples(barcode, analysis_id, starttime, endtime, process_status, phase, temp, pres, sampler_id, lab_id, method_id, valid_bool, analysis_type_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)", (barcode, analysisid, starttime, endtime, process_status, phase, temp, pressure, samplerid, labid, methodid, val_bool, analysistypeid))
    
    #retrieve the newly created sample's id from DB
    conn.execute("SELECT sample_id FROM samples WHERE starttime = %s AND analysis_id = %s AND lab_id = %s", (starttime, analysisid, labid))
    sampleid = conn.fetchall()[0][0]
    
    #enter results for the sample
    curcol = startResultCol
    curcell = sh.cell(row = samplerow, column = curcol).value
    while curcell != None:
            resultname = repSpecMap[curcol][0]
            unitname = repSpecMap[curcol][1].lower()
            if nameToResultID.has_key(resultname):
                resultid = nameToResultID[resultname]
                unitid = dbUnitMap[unitname]
                value = curcell
                print resultid, unitid, value
                #if value != 0:
                conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) VALUES (%s, %s, %s, %s)", (sampleid, resultid, unitid, value))
            curcol += 1
            curcell = sh.cell(row = samplerow, column = curcol).value    
    
    samplerow += 1
    curcell = sh.cell(row = samplerow, column = 1).value
    
db.commit()
db.close()