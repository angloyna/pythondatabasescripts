import csv
import MySQLdb


db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    
    
conn = db.cursor()


conn.execute('select analysis_id, result_id, unit_id, r2, latent_var, rmsec, rmsecv, cal_bias, cv_bias from sampledb.results_modeled')

results = conn.fetchall()

for result in results:
    conn.execute('insert into sampledb.model_log(analysis_id, result_id, unit_id, date_changed, r2, latent_var, rmsec, rmsecv, cal_bias, cv_bias) values (%s, %s, %s, "2012-01-01", %s, %s, %s, %s, %s, %s)', (result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8]))
        
db.commit()
db.close()