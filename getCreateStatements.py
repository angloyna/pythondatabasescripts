import MySQLdb
 
 
def getCreateStatements(schema):
    f = open('sampleDatabaseCreateStatements.txt', 'w')
    db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01")
    conn = db.cursor()
    
    conn.execute("select table_name from INFORMATION_SCHEMA.tables where table_schema = 'sampledb' and table_type = 'BASE TABLE'")
    
    tableNames = conn.fetchall()

    
    for table in tableNames:
        tableName = table[0]
        print tableName
        conn.execute("USE sampledb")
        conn.execute('SHOW CREATE TABLE ' + str(tableName))
        createStatement = conn.fetchone()[1]
        f.write(str(tableName))
        f.write("\n\n\t" + str(createStatement) + "\n\n")
        
    db.close()
getCreateStatements('sampledb')