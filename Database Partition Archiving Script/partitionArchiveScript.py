#need to pull the first partition off of the local database
#move the partition to a new table
#logical backup of the table
#restore the table backup to the archive database
#merge the archived table to the tdata table

import MySQLdb
import os
#this db connection is to my local computer
#db = MySQLdb.connect(host="localhost",user="root",passwd="8016Cgg1", db="testdb" )

#this db connection is to the tech pointe server
db = MySQLdb.connect("jp3m-db01.jp3m.local", "WHoward", "Austin01", "testdb")  


#this db connection is when connecting locally using the engineering computer
#db = MySQLdb.connect(host="localhost", user="root", passwd="Austin01", db="testdb")

conn = db.cursor()

#get oldest partition from the database
conn.execute("select min(PARTITION_NAME) from information_schema.partitions where table_name = 'tdata'")

minpartition = conn.fetchone()[0]

print minpartition

#create a new table with no partitions to move the oldest partition to. the create statement has to have exactly the same fields as the partitioned table

conn.execute("create table " + minpartition + " (did int auto_increment, ddatestamp datetime not null, danalysis int, constraint pk_dataID PRIMARY KEY (did, ddatestamp))")

#exchange min partition with temp table.

conn.execute("ALTER TABLE tdata EXCHANGE PARTITION " + minpartition + " WITH TABLE " + minpartition)

#back up the temp table and restore to the archive server

path = r"C:/Program Files/MySQL/MySQL Server 5.6/bin/"

os.chdir(path)

#the below system call is what i'll demo with...
#os.system("mysqldump -h localhost -P 3306 -uroot -p8016Cgg1 testdb " + minpartition + " | mysql -h 10.35.250.6 -P 3306 -uWHoward -pAustin01 receivedb")

#dump from the tech point server to my local computer
os.system("mysqldump -h 10.35.250.6 -P 3306 -uWHoward -pAustin01 testdb " + minpartition + " | mysql -h localhost -P 3306 -uroot -p8016Cgg1 testdb")

#drop the temp table

conn.execute("DROP TABLE " + minpartition)

#after all this, the partition in the working database is empty, but still exists... needs to be dropped.

conn.execute('ALTER TABLE tdata DROP PARTITION ' + minpartition)

db.commit()    
db.close()

#the next step is to exchange the partition in the archive database with the empty partition in the archive tdata, so that all the archive data is stored in one table


#connect to the server containing the archive database

db = MySQLdb.connect(host="localhost", user="root", passwd="8016Cgg1", db="testdb")

conn = db.cursor()

#switch the full partition from the active database and the empty archive partition, so that all the data for a particular table is in the same table

conn.execute("ALTER TABLE tdata EXCHANGE PARTITION " + minpartition + " WITH TABLE " + minpartition)

#drop the partition table which is now empty
conn.execute("DROP TABLE testdb." + minpartition)

db.commit()
db.close()