import openpyxl
from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting import ColorScaleRule, CellIsRule, FormulaRule
from openpyxl.cell import get_column_letter

excelfilename = 'C:/Users/agloyna/Desktop/Calibration reports (1).xlsx'

wb = load_workbook(filename = excelfilename)
sh = wb.worksheets[0]
formattedSheet = wb.worksheets[1]

#old spreadsheet column variables
meterNumberColumn = 2
DescriptionColumn = 4
DatePerformedColumn = 12
PressureBaseColumn = 11
HeatingValueColumn = 13
nitrogenValueColumn = 13
specificGravityColumn = 13
co2ValueColumn = 13
tempBaseColumn = 11
AtmosphericPressureColumn = 11
RTUColumn = 3
foundcolumn = 2

formattedSheetcurRow = 2

originalSheetcurRow = 1

while originalSheetcurRow <= 17062:
    curCell = sh.cell(row = originalSheetcurRow, column = 1).value
    if type(curCell) is not float and type(curCell) is not int and curCell != None:
        if (curCell).find('Meter:') >= 0:

            formattedSheet.cell(row = formattedSheetcurRow, column = 1).value = originalSheetcurRow
            
            meterNumber = sh.cell(row = originalSheetcurRow, column = meterNumberColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 2).value = meterNumber
            
            description = sh.cell(row = originalSheetcurRow, column = DescriptionColumn).value
            
            if description == None:
                description = sh.cell(row = originalSheetcurRow, column = DescriptionColumn + 1).value
                if description == None:
                    description = sh.cell(row = originalSheetcurRow, column = DescriptionColumn + 2).value                
                
            formattedSheet.cell(row = formattedSheetcurRow, column = 3).value = description
            
            date = sh.cell(row = originalSheetcurRow, column = DatePerformedColumn).value
            if date != None and type(date) is not int:
                date = date.split(": ")[1].split("Rev")[0]
                
            formattedSheet.cell(row = formattedSheetcurRow, column = 4).value = date
            
            pressureBaseValue = sh.cell(row = originalSheetcurRow + 10, column = PressureBaseColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 5).value = pressureBaseValue
            
            tempBaseValue = sh.cell(row = originalSheetcurRow + 12, column = tempBaseColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 10).value = tempBaseValue
            
            heatingValue = sh.cell(row = originalSheetcurRow + 8, column = HeatingValueColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 6).value = heatingValue
            
            nitrogenValue = sh.cell(row = originalSheetcurRow + 10, column = nitrogenValueColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 7).value = nitrogenValue
            
            co2Value = sh.cell(row = originalSheetcurRow + 12, column = co2ValueColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 9).value = co2Value
            
            specificGravityValue = sh.cell(row = originalSheetcurRow + 9, column = specificGravityColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 8).value = specificGravityValue
            
            atmosphericPressure = sh.cell(row = originalSheetcurRow + 9, column = AtmosphericPressureColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 11).value = atmosphericPressure
            
            RTUValue = sh.cell(row = originalSheetcurRow + 13, column = RTUColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 12).value = RTUValue
            
            FoundValue = sh.cell(row = originalSheetcurRow + 14, column = RTUColumn).value
            
            formattedSheet.cell(row = formattedSheetcurRow, column = 13).value = FoundValue
            
            formattedSheetcurRow += 1
            
    originalSheetcurRow += 1   
    
    
wb.save(r"C:/Users/agloyna/Desktop/testingExcel.xlsx")