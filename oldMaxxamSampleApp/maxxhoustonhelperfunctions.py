import datetime

def createTimeStamp(datestr, timestr):
    startdatesplit = datestr.split()
    startdate = datetime.date(int(startdatesplit[0]), int(startdatesplit[1]), int(startdatesplit[2]))
    starttime = timestr
    starttimestamp = datetime.datetime.combine(startdate, starttime)
    return starttimestamp