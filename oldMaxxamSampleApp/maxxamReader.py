from openpyxl import Workbook
from openpyxl import load_workbook
from maxxhelperfunctions import *
import MySQLdb
import os

def maxxamReader(excelfilepath, pdfpath, analysistype, phase, containertype, analysislab, Method):
    db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
    conn = db.cursor()
    
    f = open('sampleProcessingSummary.txt', 'w')
    
    wb = load_workbook(filename = excelfilepath)
    sheet_ranges = wb.worksheets[0]
    resultsStartCol = 29
    curcol = resultsStartCol
    
    conn.execute('SELECT id from sampledb.analysis_types where name = %s', analysistype)
    analysistypeid = conn.fetchall()[0][0]
  
    curcell = sheet_ranges.cell(row = 1, column = curcol).value
    
    repSpecMap = {}
    nameToresultID = {}
    dbnames = {}
    dbUnitMap = {}
    
    lab = analysislab
    method = Method 
    process_status = "Processed"
    val_bool = "Y"
    containername = containertype
        
    while curcell != None:
        resultunitpair = curcell.split('[')
        if curcell.find("[") >= 0:
            spec = resultunitpair[0].strip()
            units = resultunitpair[1].split(']')[0].strip().lower()
            if units == 'wt %':
                units = 'wt. %'
            if units == 'lv %':
                units = 'l.v. %'
        else:
            spec = resultunitpair[0].strip()
            units = 'None'
        repSpecMap[curcol] = (spec, units)
        curcol += 1
        curcell = sheet_ranges.cell(row = 1, column = curcol).value
        
    if phase == 'Gas' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Houston':
        conn.execute("SELECT id, maxxhou_c30plus from result_names;")
    if phase == 'Gas' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c15plus from result_names;")
    if phase == 'Liquid' and lab == 'Maxxam Edmonton':
        conn.execute("SELECT id, maxxEd_c30plus from result_names;")       
        
    rows = conn.fetchall()
    
    for row in rows:
        dbnames[row[1]] = row[0]
         
    for item in repSpecMap:
        curspecies = repSpecMap[item][0]
        if dbnames.has_key(curspecies):
            nameToresultID[curspecies] = dbnames[curspecies]
        else:
            f.write("can't find " + str(curspecies) + "\n")
     
    conn.execute("SELECT id, spl_name FROM units")
    
    units = conn.fetchall()
    
    for unit in units:
        dbUnitMap[unit[1].lower()] = unit[0]
        
    samplerow = 2
    cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
    
    conn.execute("SELECT id from sampledb.labs where name = %s", (lab))
    
    labid = conn.fetchall()[0][0]
    
    conn.execute("INSERT INTO sampledb.edd_files(path, date_received, lab_id) values(%s, NOW(), %s)", (excelfilepath, labid))
    
    if pdfpath != 'None':
        conn.execute("INSERT INTO sampledb.pdf_files(path, date_received, lab_id) values(%s, NOW(), %s)", (pdfpath, labid))
        conn.execute("SELECT id from sampledb.pdf_files where path = %s", (pdfpath))
        pdf_id = conn.fetchall()[0][0]
        
    if pdfpath == 'None':
        pdf_id = None
            
    conn.execute("SELECT id from sampledb.edd_files where path = %s", (excelfilepath))
      
    edd_id = conn.fetchall()[0][0]
    
    conn.execute("SELECT id from sampledb.methods where name = '" + str(method) + "'")
    methodid = conn.fetchall()[0][0]    
    
    
    conn.execute("SELECT id from companies where name = 'JP3 Measurement'")
    samplerid = conn.fetchall()[0][0]    
    
    
    conn.execute("SELECT id FROM container_type where name = %s", (containername))
    containerid = conn.fetchall()[0][0]    
    
    
    while cursamplecell != None:
        samplerowstr = str(samplerow)
        sitename = sheet_ranges['B' + samplerowstr].value
        starttime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['P' + samplerowstr].value,'%H:%M:%S').time())
        
        if sheet_ranges['R' + samplerowstr].value == None:
            endtime = starttime + datetime.timedelta(minutes=1)
        else:
            endtime = createTimeStamp(sheet_ranges['O' + samplerowstr].value, datetime.datetime.strptime(sheet_ranges['R' + samplerowstr].value,'%H:%M:%S').time())
            
            
        if sheet_ranges['S' + samplerowstr].value != None:
            receiveddatesplit = sheet_ranges['S' + samplerowstr].value.split(' ')
            receiveddate = datetime.date(int(receiveddatesplit[0]), int(receiveddatesplit[1]), int(receiveddatesplit[2]))
            
        if sheet_ranges['S' + samplerowstr].value == None:
            receiveddate = None
            
        
        analysisnumber = sheet_ranges['T' + samplerowstr].value[1:]
        pressure = sheet_ranges['U' + samplerowstr].value
        temp = sheet_ranges['X' + samplerowstr].value
        barcode = sheet_ranges['G' + samplerowstr].value
        
        f.write("\n\nsite: " + str(sitename) + " analysis number: " + str(analysisnumber) + " barcode: " + str(barcode) + "\n\tstarttime: " + starttime.strftime('%m-%d-%Y %H:%M:%S') + "\n\tendtime: " + endtime.strftime('%m-%d-%Y %H:%M:%S') + "\n\ttemperature: " + str(temp) + "\n\tpressure: " + str(pressure) + "\n")
        
        conn.execute("SELECT id FROM sites WHERE name = %s", (sitename,))
        siteid = conn.fetchall()[0][0]
        
        conn.execute("SELECT id FROM analysis_points WHERE analysis_number = %s AND site_id = %s",(analysisnumber, siteid))
        analysisid = conn.fetchall()[0][0]
        
        
        conn.execute("INSERT INTO samples(barcode, analysis_id, starttime, endtime, process_status, phase, temp, pres, sampler_id, lab_id, valid_bool, analysis_type_id, container_type_id, edd_id, pdf_id, lib_bool, method_id, date_received) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s,%s, %s, %s, %s)", (barcode, analysisid, starttime, endtime, process_status, phase, temp, pressure, samplerid, labid, val_bool, analysistypeid, containerid, edd_id, pdf_id, 'N', methodid, receiveddate))
        
        conn.execute("SELECT sample_id FROM samples WHERE starttime = %s AND analysis_id = %s AND lab_id = %s", (starttime, analysisid, labid))
        sampleid = conn.fetchall()[0][0]
        
        curcol = resultsStartCol
        curcell = sheet_ranges.cell(row = samplerow, column = curcol).value
        while sheet_ranges.cell(row = 1, column = curcol).value != None:
                resultname = repSpecMap[curcol][0]
                unitname = repSpecMap[curcol][1].lower()
                value = curcell
                if unitname == 'volume fraction':
                    value = float(value) * 100
                    unitname = 'l.v. %'
                if unitname == 'mass fraction':
                    value = float(value) * 100
                    unitname = 'wt. %'
                if unitname == 'mol fraction':
                    value = float(value) * 100
                    unitname = 'mol %'
                if nameToresultID.has_key(resultname) and dbUnitMap.has_key(unitname):
                    resultid = nameToresultID[resultname]
                    unitid = dbUnitMap[unitname]
                    if resultid == 62 and phase == 'Gas' and lab == 'Maxxam Edmonton' and unitid == 4:
                        unitid = 13
                    if value != None and not (resultid == 72 and value == 0):
                        conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) VALUES (%s, %s, %s, %s)", (sampleid, resultid, unitid, value))
                        #Dry BTU conversion from MJ/m3 to BTU/cf
                        if resultid == 62 and unitid == 13 and phase == 'Gas' and lab == 'Maxxam Edmonton':
                            value = (float(value) * 947.81712) / 35.314
                            unitid = 8
                            conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) values(%s, %s, %s, %s)", (sampleid, resultid, unitid, value))
                            f.write("\n\t\tDry BTU converted from MJ/m3 to BTU/cf")
                        #API gravity calculation
                        if resultid == 60 and unitid == 4 and phase == 'Liquid':
                            resultid = 51
                            value = (141.5/float(value)) - 131.5
                            unitid = 4
                            conn.execute("INSERT INTO results(sample_id, result_id, unit_id, value) values (%s, %s, %s, %s)", (sampleid, resultid, unitid, value))
                            f.write("\n\t\tAPI Gravity calculated for this sample\n")
                else:
                    "Can't find: ", resultname, unitname
                curcol += 1
                curcell = sheet_ranges.cell(row = samplerow, column = curcol).value
                
        samplerow += 1
        cursamplecell = sheet_ranges.cell(row = samplerow, column = 2).value
        
        
        #Xylenes needs to be calculated from meta and ortho/para values for mol% lv% and wt%. This can be done at the same time as the c6plus and c7 plus calculations
        
        #add c6+ if c7+ exists but c6+ does not
        
        conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (sampleid))
                
        speciationUnits = conn.fetchall()        
        #add c6 plus and c7 plus for all the units each sample is reported in
        for specUnit in speciationUnits:
            #makes sure the unit pulled is a speciation unit
            specUnitname = specUnit[0]
            if specUnitname in ['mol %', 'wt %', 'lv %']:
                if specUnitname == 'lv %':
                    specUnitname = 'l.v. %'
                if specUnitname == 'wt %':
                    specUnitname = 'wt. %'
                specUnitId = dbUnitMap[specUnitname]
                if specUnitname == 'l.v. %':
                    specUnitname = 'lv %'
                if specUnitname == 'wt. %':
                    specUnitname = 'wt %'                
                #finds c7+ value for the sample in the appropriate units if there isn't a c6plus value also.
                conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c7+' and sample_id = " + str(sampleid) + " and sample_id not in (select distinct sample_id from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+')")
            
                c7plusvalue = conn.fetchall()
                #checks to see if there is a c7 plus value. This handles the case where there is a c7 plus value and no c6 plus value
                if c7plusvalue != ():
                    c7plusvalue = c7plusvalue[0][0]
                    #finds the c6 value for the sample in appropriate units
                    conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'C6' and sample_id = " + str(sampleid))
                    c6value = conn.fetchall()
                    #if c6 value exists
                    if c6value != ():
                        
                        c6value = c6value[0][0]
                        #add c6 to c7+ to arrive at c6 plus value
                        c6plusvalue = float(c6value) + float(c7plusvalue)
                        #insert this c6 plus value into the db
                        conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 11, %s, %s)", (sampleid, specUnitId, c6plusvalue))
                        #tell the user it did this
                        f.write("\n\t\tC6+ value [" + str(specUnitname) + "] was calculated for this sample\n")
                        
                #This handles the case where there is no c7 plus value, but there may be a c6 plus value
                if phase == 'Liquid' and c7plusvalue == ():
                    #check to see if there is a c6+ value
                    conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+' and sample_id = " + str(sampleid))
                    
                    c6plusNoc7plus = conn.fetchall()
                    #if there is a c6 plus value
                    if c6plusNoc7plus != ():
                        c6plusNoc7plus = c6plusNoc7plus[0][0]
                        #find c6 value if c6 plus value exists
                        conn.execute("select value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'C6' and sample_id = " + str(sampleid))
                        
                        c6valueNoc7 = conn.fetchall()
                        
                        #if c6 value exists
                        if c6valueNoc7 != ():
                            c6valueNoc7 = c6valueNoc7[0][0]
                            #subtract c6 from c6 plus to arrive at c7 plus
                            c7plusvalueLiq = float(c6plusNoc7plus) - float(c6valueNoc7)
                            #add c7 plus to database
                            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values (%s, 15, %s, %s)", (sampleid, specUnitId, c7plusvalueLiq))
                        #if there is no c6 value in addition to a c6+ value, it cannot calculate c7 plus easily, so it won't and will tell the user this.
                        else:
                            f.write("\n\t\tThis sample has a c6+ value but has neither a c7+ value nor a c6 value. C7+ could not be calculated.")
                    #if the sample doesn't have a c6 plus value or a c7 plus value, the program does nothing     
                    else:
                        f.write("\n\t\tThis sample has neither a c6 plus nor a c7 plus value. c7 plus has not been calculated.")
                #This handles the case where it is a gas sample and it does not have a c7 plus value but may have a c6 plus value    
                if phase == 'Gas' and c7plusvalue == ():
                    
                    #check to see if there is already a c6 plus value
                    conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name = 'c6+' and sample_id = " + str(sampleid))
                    
                    c6plusgasCheck = conn.fetchall()
                    
                    #if there's no c6 plus value in the db already
                    if c6plusgasCheck == ():
                        #Find the values which make up c6 plus if no c6 plus or c7 plus values exist
                        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15+') and sample_id = " + str(sampleid))
                    
                        c6plusValuesGas = conn.fetchall()
                        c6plus = 0
                        
                        #make sure the db has all the values asked for
                        if len(c6plusValuesGas) == 10:
                            #sum the values
                            for value in c6plusValuesGas:
                                c6plus += float(value[0])
                                
                            #add the c6 plus value to the db
                            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 11, %s, %s)", (sampleid, specUnitId, c6plus))
                            f.write("\n\t\tC6 plus [" + str(specUnitname) + "] was calculated for this sample.")
                            
                        else:
                            #if the db doesn't have all the values asked for, don't calculate the c6 plus value
                            f.write("\n\t\tNo C6 plus value calculated for sample " + str(sampleid) + " because could not find c6 through c15+ in database.")
                        
                        
                        
                        #Now do c7 plus  
                        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15+') and sample_id = " + str(sampleid))
                        
                        c7plusValuesGas = conn.fetchall()
                        
                        c7plus = 0
                        #make sure it has all the values asked for
                        if len(c7plusValuesGas) == 9:
                            
                            for value in c7plusValuesGas:
                                c7plus += float(value[0])
                                
                            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values (%s, 15, %s, %s)", (sampleid, specUnitId, c7plus))
                            f.write("\n\t\tC7 plus [" + str(specUnitname) + "] was calculated for this sample.")
                            
                        else:
                            f.write("\n\t\tNo C7 plus value calculated for sample " + str(sampleid) + " because could not find c7 through c15+ in database.")
                            
                            
                #Xylenes calculation
                            
                conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and sample_id = " + str(sampleid) + " and result_name in ('m+p-xylenes', 'o-xylene') and sample_id not in (SELECT DISTINCT sample_id from sampledb.sample_results where unit_name = '" + str(specUnitname)  + "' and result_name = 'Xylenes')")
                
                xyleneValues = conn.fetchall()
                totalXylenes = 0
                if len(xyleneValues) == 2:
                    for xylene in xyleneValues:
                        totalXylenes += xylene[0]
                    
                    conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 46, %s, %s)", (sampleid, specUnitId, totalXylenes))
                    f.write("\n\t\tXylenes [" + str(specUnitname) + "] was calculated for this sample.")
                    
                    
                #c12 plus calculation
                
                if phase == 'Liquid':
                    conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and sample_id = " + str(sampleid) + " and result_name = 'c12+'")
                    
                    if conn.fetchall() == ():
                        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(specUnitname) + "' and result_name in ('c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22', 'c23', 'c24', 'c25', 'c26', 'c27', 'c28', 'c29', 'c30+') and sample_id = " + str(sampleid))
                        
                        c12plusValues = conn.fetchall()
                        c12plus = 0
                        
                        if len(c12plusValues) == 19:
                            for value in c12plusValues:
                                c12plus += float(value[0])
                                
                            conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) VALUES (%s, 73, %s, %s)", (sampleid, specUnitId, c12plus))
                            f.write("\n\t\tC12 plus [" + str(specUnitname) + "] was calculated for this sample.")
                
                    
                    
                    
                    
        #sum checks
        
        f.write("\n\n\t\tSum Checks")
        conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (sampleid))
        
        units = conn.fetchall()

        for unit in units:
            if unit[0] in ['mol %', 'wt %', 'lv %']:
                conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(unit[0]) + "' and result_name in ('he', 'h2', 'co2', 'n2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+') and sample_id = " + str(sampleid))
    
                sumvalues = conn.fetchall()
                sum = 0
                for value in sumvalues:
                    sum += value[0]
        
    
                f.write("\n\t\t" + str(unit[0]) + ": " + str(sum) + "\n")
        
        #convert VPCR4 to psi from pka
        
        if phase == 'Liquid':
            conn.execute("SELECT value from sampledb.results where result_id = 72 and unit_id = 7 and sample_id = %s and sample_id not in (select distinct sample_id from sampledb.results where result_id = 72 and unit_id = 6)", (sampleid))
            
            siVaporPressure = conn.fetchall()
            
            if siVaporPressure != ():
                siVaporPressure = siVaporPressure[0][0]
                psiVaporPressure = float(siVaporPressure) * .14503
                
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 72, 6, %s)", (sampleid, psiVaporPressure))
                
                f.write("\n\t\tThe VPCR4 value for sample " + str(sampleid) + " was converted to psi from kpa\n.")
                
                
                
        #calculated TVP with no N2 and CO2 and TVP with N2 and CO2
        
        if phase == 'Liquid':
            conn.execute("SELECT result_name, value from sampledb.sample_results where sample_id = " + str(sampleid) + " and unit_name = 'mol %' and result_name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+')")
            
            tvpmolpercents = conn.fetchall()
            
            if len(tvpmolpercents) == 8:
                conn.execute("SELECT name, vp_psia_gpa2177 from sampledb.result_names where name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+')")
                
                partialPressureFactorDict = {}
                partialPressureFactorResults = conn.fetchall()
                
                for partialPressureFactor in partialPressureFactorResults:
                    partialPressureFactorDict[partialPressureFactor[0]] = partialPressureFactor[1]
            
                TVPnoN2CO2 = 0    
                for molpercent in tvpmolpercents:
                    name = molpercent[0]
                    molfract = (float(molpercent[1]) / 100)
                    factor = partialPressureFactorDict[name]
                    TVPnoN2CO2 += (factor * molfract)
                    
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 93, 11, %s)", (sampleid, TVPnoN2CO2))
                f.write("\n\t\tThe TVPnoN2CO2 value for sample " + str(sampleid) + " has been calculated\n.")
                
            elif len(tvpmolpercents) != 8:
                f.write("\n\t\tThe TVPnoN2CO2 value for sample " + str(sampleid) + " could not be calculated\n.")
                
                
            conn.execute("SELECT result_name, value from sampledb.sample_results where sample_id = " + str(sampleid) + " and unit_name = 'mol %' and result_name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'n2', 'co2')")
                            
            tvpmolpercentsf = conn.fetchall()
            
            if len(tvpmolpercentsf) == 10:
                conn.execute("SELECT name, vp_psia_gpa2177 from sampledb.result_names where name in ('c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+', 'n2', 'co2')")
                
                partialPressureFactorDict = {}
                partialPressureFactorResults = conn.fetchall()
                
                for partialPressureFactor in partialPressureFactorResults:
                    partialPressureFactorDict[partialPressureFactor[0]] = partialPressureFactor[1]
            
                TVPwN2CO2 = 0    
                for molpercent in tvpmolpercentsf:
                    name = molpercent[0]
                    molfract = (float(molpercent[1]) / 100)
                    factor = partialPressureFactorDict[name]
                    TVPwN2CO2 += (factor * molfract)
                    
                conn.execute("INSERT INTO sampledb.results(sample_id, result_id, unit_id, value) values(%s, 94, 11, %s)", (sampleid, TVPwN2CO2))
                
                f.write("\n\t\tThe TVP_N2CO2_JP3 value for sample " + str(sampleid) + " has been calculated\n.")
                
            elif len(tvpmolpercents) != 10:
                f.write("\n\t\tThe TVP_N2CO2_JP3 value for sample " + str(sampleid) + " could not be calculated\n.")            
                
                
                
    
        #find spectra
        
        conn.execute("SELECT count(id) from sampledb.spectra where sample_id = %s", (sampleid,))
        
        numberofSpectra = conn.fetchall()[0][0]
        
        if numberofSpectra == 0:
        
            conn.execute("SELECT localdb from sampledb.sites where name = %s", (sitename,))
            
            localdb = conn.fetchall()[0][0]
            db.commit()
            db.close()
            
            db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", localdb )
            conn = db.cursor()
            
            conn.execute("SELECT cdatestamp, ctemp, cpres, ctran, cdata from tcal where cdatestamp between %s and %s and canalysis = %s", (starttime, endtime, analysisnumber))
            
            spectra = conn.fetchall()
            
            if spectra == ():
                f.write("\n\t\tNo records found for this sample.\n")

            db.close()
            db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
            conn = db.cursor()
            if spectra != ():
                for spectrum in spectra:
                    conn.execute("INSERT INTO sampledb.spectra(sample_id, time, temp, pres, tran, spectrum) VALUES(%s, %s, %s, %s, %s, %s)", (sampleid, spectrum[0], spectrum[1], spectrum[2], spectrum[3], spectrum[4]))
                    
                conn.execute("SELECT count(id) from sampledb.spectra where sample_id = %s", (sampleid,))
                
                spectraCount = conn.fetchall()
                if spectraCount != ():
                    spectraCount = spectraCount[0][0]
                    f.write("\n\t\tFound " + str(spectraCount) + " spectra for sample " + str(sampleid) + "\n")
                      
        else:
            f.write("\n\t\tThere were already spectra in the database with sample_id " + str(sampleid) + "\n")
        
    f.close()                 
    db.commit()
    db.close()
    
    os.system("notepad.exe " + "sampleProcessingSummary.txt")    
                