
import MySQLdb

f = open('sampleProcessingSummary', 'w')

sampleid = 5087

db = MySQLdb.connect("JP3M-DB01.jp3m.local","WHoward","Austin01", "sampledb" )
conn = db.cursor()

conn.execute("SELECT distinct unit_name from sampledb.sample_results where sample_id = %s", (sampleid))
        
units = conn.fetchall()

for unit in units:
    if unit[0] in ['mol %', 'wt %', 'lv %']:
        conn.execute("SELECT value from sampledb.sample_results where unit_name = '" + str(unit[0]) + "' and result_name in ('he', 'h2', 'co2', 'n2', 'c1', 'c2', 'c3', 'ic4', 'nc4', 'ic5', 'nc5', 'c6+') and sample_id = " + str(sampleid))
    
        sumvalues = conn.fetchall()
        sum = 0
        for value in sumvalues:
            sum += value[0]
        
    
        f.write("\t" + str(unit[0]) + ": " + str(sum) + "\n")
        
f.close()
db.close()