from Tkinter import *
import tkFileDialog
from modelValidationReport import *


def browsebutton(entryobject,t):
    file = tkFileDialog.askopenfilename(parent=t,title='Choose a file')
    entryobject.insert(INSERT, file )
    
def go(predictionpath, unitsmodeled, t):
    t.destroy()
    modelValidationReport(predictionpath, unitsmodeled)
    

def ValidationSummaryGenerator():
    top = Tk()
    top.resizable(width=FALSE, height=FALSE)
    top.title('Model Validation Generator')
    labelframe = LabelFrame(top, text="Model Prediction File Entry")
    labelframe.pack(fill="both", expand="yes", padx = 5, pady = 5)
    
    excelFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    excelFrame.pack(fill=X, padx=5, pady=5)        
    excellabel = Label(excelFrame, text = 'Model Prediction File: ', justify = LEFT, width = 20)
    excellabel.pack(side = LEFT)
    q = Entry(excelFrame, width = 70)
    q.pack(side = LEFT)
    excelbutton = Button(excelFrame, width = 10, text = 'Browse',command= lambda: browsebutton(q, top))
    excelbutton.pack(side = LEFT)
    
    
    settingsFrame = Frame(labelframe, height=2, bd=1, relief=SUNKEN)
    settingsFrame.pack(fill=X, padx=5, pady=5)
    unitFrame = Frame(settingsFrame, height=2, bd=1)
    unitFrame.pack(padx=5, pady=5, side = TOP)
    unitlabel = Label(unitFrame, text = 'Units Modeled: ', justify = RIGHT, width = 13)
    unitlabel.pack(side = LEFT)
    unitVar = StringVar(unitFrame)
    unitVar.set("mol %")
    unitoptions = OptionMenu(unitFrame, unitVar, "mol %", "wt %", "lv %")
    unitoptions.pack(side = LEFT)       
    
    
    
    
    
    gobutton = Button(labelframe, width = 10, text = 'Go', command= lambda: go(q.get(), unitVar.get(), top))
    gobutton.pack(side = BOTTOM, padx = 5, pady = 5)    
    
    top.mainloop()
    
    
ValidationSummaryGenerator()
    